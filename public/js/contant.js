function CustomAlert(icon, type, title, message) {
    let alert = document.createElement('div')
    alert.classList.add('alert')
    alert.classList.add('alert-' + type)
    alert.style.position = 'fixed'
    alert.style.left = '0'
    alert.style.right = '0'
    alert.style.top = '0'
    alert.style.zIndex = '9999'
    let alertIcon = document.createElement('i')
    alertIcon.classList.add('fal')
    alertIcon.classList.add(icon)
    let alertIconD = document.createElement('div')
    alertIconD.classList.add('alert-icon')
    let closeButton = document.createElement('button')
    closeButton.type = 'button'
    closeButton.classList.add('close')
    closeButton.setAttribute('aria-label', 'Close')
    $(closeButton).click(function () {
        $(alert).slideUp(() => {
            alert.remove()
        })
    })
    let CBSpan = document.createElement('span')
    CBSpan.setAttribute('aria-hidden', 'true')
    let CBIcon = document.createElement('i')
    CBIcon.classList.add('fal')
    CBIcon.classList.add('fa-times')
    let titleMessage = document.createElement('b')
    titleMessage.textContent = title
    let container = document.createElement('div')
    container.classList.add('container')
    alertIconD.append(alertIcon)
    CBSpan.append(CBIcon)
    closeButton.append(CBSpan)
    container.append(alertIconD)
    container.append(closeButton)
    container.append(titleMessage)
    container.append(' ' + message)
    alert.append(container)
    $(alert).slideUp()
    document.getElementsByTagName('body')[0].append(alert)
    $(alert).slideDown()
    return alert
}
jQuery(document).ready(function () {
    jQuery('#myForm').on('submit',function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }

        });
        $.ajax({
            url: document.getElementById('myForm').getAttribute('action'),
            method: 'post',
            data: JSON.stringify( {
                name: jQuery('#name').val(),
                email: jQuery('#email').val(),
                phone: jQuery('#tel').val(),
                message: jQuery('#mes').val()
            }),
            statusCode:{
                422:function (res){
                    let messages=[]
                    let errors=res.responseJSON.errors
                    for(message in errors){
                    messages.push(errors[message][0])
                    }

                   let Alert=CustomAlert('danger','danger','Errors:',messages.toString())
                    $('body').prepend(Alert)
                }


            }
        });
    });
});

