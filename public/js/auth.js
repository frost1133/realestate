function CustomAlert(icon, type, title, message) {
    let alert = document.createElement('div')
    alert.classList.add('alert')
    alert.classList.add('alert-' + type)
    alert.style.position = 'fixed'
    alert.style.left = '0'
    alert.style.right = '0'
    alert.style.top = '0'
    alert.style.zIndex = '9999'
    let alertIcon = document.createElement('i')
    alertIcon.classList.add('fal')
    alertIcon.classList.add(icon)
    let alertIconD = document.createElement('div')
    alertIconD.classList.add('alert-icon')
    let closeButton = document.createElement('button')
    closeButton.type = 'button'
    closeButton.classList.add('close')
    closeButton.setAttribute('aria-label', 'Close')
    $(closeButton).click(function () {
        $(alert).slideUp(() => {
            alert.remove()
        })
    })
    let CBSpan = document.createElement('span')
    CBSpan.setAttribute('aria-hidden', 'true')
    let CBIcon = document.createElement('i')
    CBIcon.classList.add('fal')
    CBIcon.classList.add('fa-times')
    let titleMessage = document.createElement('b')
    titleMessage.textContent = title
    let container = document.createElement('div')
    container.classList.add('container')
    alertIconD.append(alertIcon)
    CBSpan.append(CBIcon)
    closeButton.append(CBSpan)
    container.append(alertIconD)
    container.append(closeButton)
    container.append(titleMessage)
    container.append(' ' + message)
    alert.append(container)
    $(alert).slideUp()
    document.getElementsByTagName('body')[0].append(alert)
    $(alert).slideDown()
    return alert
}

function CheckAuth(e = null) {
    if (!Auth) {
        e ? e.preventDefault() : null;
        document.querySelector('figure[data-target="#loginform"]').click()
        CustomAlert('fa-check', 'warning', 'WARNING:', 'To Be Continue Need Login')
    }
}

function logout(e,url) {
    e.preventDefault();
    $.ajax(url, {
        method: 'post',
        data:null   ,
        statusCode: {
            422: function (res) {
                let message = []
                for (error in res.responseJSON.errors) {
                    message.push(res.responseJSON.errors[error]['0'])
                }
                CustomAlert('fa-danger', 'danger', 'ERROR:', message.join(' and '))
            },
        }
    }).done(function (res) {
        Auth = res.auth
        $('meta[name="X-CSRF-TOKEN"]').attr('content' , res['csrf-token'])
        window.location.href = res.intended
    })

}

let responseLogin = null
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': document.querySelector("meta[name='X-CSRF-TOKEN']").getAttribute('content')
        }
    })

    $('#loginForm').on('submit', function (e) {
        e.preventDefault()
        let data = new FormData(this)
        $.ajax(this.getAttribute('action'), {
            method: 'post',
            data: JSON.stringify({
                email: data.get('email'),
                password: data.get('password'),
                remember: !!data.get('remember')
            }),
            dataType: 'json',
            contentType: 'json',
            statusCode: {
                422: function (res) {
                    let message = []
                    for (error in res.responseJSON.errors) {
                        message.push(res.responseJSON.errors[error]['0'])
                    }
                    CustomAlert('fa-danger', 'danger', 'ERROR:', message.join(' and '))
                },
            }
        }).done(function (res) {
            Auth = res.auth
            if (res.auth) {
                CustomAlert('fa-check', 'success', 'SUCCESSFUL:', 'Authentication completed successfully')
                $('figure[data-target="#loginform"]').hide();
                let logout = ` 
                    <a href="${res.route.logout}" onclick="logout(event,'${res.route.logout}')">
                        <i class="fal fa-door-open clr-ef te-sh fs-23"></i>
                        <!-- Button trigger modal -->
                        <span class="clr-ff hd-font fs-16">Logout</span>
                    </a>`;
                let profile = `
                    <a href="${res.route.profile}">
                        <i class="fal fa-user-check clr-ef te-sh fs-23"></i>
                        <!-- Button trigger modal -->
                        <span class="clr-ff hd-font fs-16">Profile</span>
                   </a>`;
                logout = $(logout)[0]
                profile = $(profile)[0]
                $('meta[name="X-CSRF-TOKEN"]').attr('content' , res['csrf-token'])
                document.querySelector('.account-btn.d-inline-block.ml-4.mt-3').append(profile)
                document.querySelector('.account-btn.d-inline-block.ml-4.mt-3').append(logout)
                $.ajaxSetup({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector("meta[name='X-CSRF-TOKEN']").getAttribute('content')
                    }
                })
            }
        })
    })
    $('#registerForm').on('submit', function (e) {
        e.preventDefault()
        let data = new FormData(this)
        $.ajax(this.getAttribute('action'), {
            method: 'post',
            data: JSON.stringify({
                'name': data.get('name'),
                'email': data.get('email'),
                'phone': data.get('phone'),
                'password': data.get('password'),
                'password_confirmation': data.get('password_confirmation')
            })
            ,
            processData: false,
            contentType: false,
            statusCode: {
                422: function (res) {
                    let message = []
                    for (error in res.responseJSON.errors) {
                        message.push(res.responseJSON.errors[error]['0'])
                    }
                    CustomAlert('fa-danger', 'danger', 'ERROR:', message.join(' and '))
                },
            }
        }).done(function (res) {
            Auth = res.auth
            if (res.auth) {
                CustomAlert('fa-check', 'success', 'SUCCESSFUL:', 'Authentication completed successfully')
                $('figure[data-target="#loginform"]').hide();
                let logout = ` 
                    <<a href="${res.route.logout}" onclick="logout(event,'${res.route.logout}')">
                        <i class="fal fa-door-open clr-ef te-sh fs-23"></i>
                        <!-- Button trigger modal -->
                        <span class="clr-ff hd-font fs-16">Logout</span>
                    </a>`;
                let profile = `
                    <a href="${res.route.profile}">
                        <i class="fal fa-user-check clr-ef te-sh fs-23"></i>
                        <!-- Button trigger modal -->
                        <span class="clr-ff hd-font fs-16">Profile</span>
                   </a>`;
                logout = $(logout)[0]
                profile = $(profile)[0]
                $('meta[name="X-CSRF-TOKEN"]').attr('content' , res['csrf-token'])
                document.querySelector('.account-btn.d-inline-block.ml-4.mt-3').append(profile)
                document.querySelector('.account-btn.d-inline-block.ml-4.mt-3').append(logout)
                $.ajaxSetup({
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector("meta[name='X-CSRF-TOKEN']").getAttribute('content')
                    }
                })
            }
        })
    })


})