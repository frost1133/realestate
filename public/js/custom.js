$(document).ready(function () {
    $(".slider-area .owl-carousel").owlCarousel({
        'items': 1,
        'dots': false,
        'autoHeight': false,
        'autoplay': true,
        'autoplayTimeout': 7000,
        'autoplayHoverPause': true,
        'animateOut': 'fadeOut',
        'loop': true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $(".testimonials-slider .owl-carousel").owlCarousel({
        'items': 2,
        'dots': false,
        'nav': true,
        'margin': 30,
        'autoHeight': false,
        'autoplay': true,
        'autoplayTimeout': 7000,
        'autoplayHoverPause': true,
        'animateOut': 'fadeOut',
        'loop': true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            993: {
                items: 2
            }
        }
    });
    $(".blog-slider .owl-carousel").owlCarousel({
        'items': 4,
        'dots': false,
        'nav': true,
        'margin': 30,
        'autoHeight': false,
        'autoplay': true,
        'autoplayTimeout': 7000,
        'autoplayHoverPause': true,
        'animateOut': 'fadeOut',
        'loop': true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });
    $(".home-gallery .owl-carousel").owlCarousel({
        'items': 1,
        'dots': false,
        'nav': true,
        'lazyLoad': true,
        'autoHeight': true,
        'margin': 30,
        'autoplay': true,
        'autoplayTimeout': 7000,
        'autoplayHoverPause': true,
        'animateOut': 'fadeOut',
        'loop': true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
});
// menu
$(document).ready(function () {
    $("#m-menu-btn").click(function () {
        $("#m-menu").removeClass("hid");
        $("#m-menu-box").removeClass("hid");
        $("#m-menu").addClass("show");
        $("#m-menu-box").addClass("show");
    });
});
$(document).ready(function () {
    $("#m-overly").click(function () {
        $("#m-menu").removeClass("show");
        $("#m-menu-box").removeClass("show");
        $("#m-menu").addClass("hid");
        $("#m-menu-box").addClass("hid");
    });
});
$(document).ready(function () {
    $("#m-menu-close").click(function () {
        $("#m-menu").removeClass("show");
        $("#m-menu-box").removeClass("show");
        $("#m-menu").addClass("hid");
        $("#m-menu-box").addClass("hid");
    });
});
