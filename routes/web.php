<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( [ 'namespace' => 'App' , 'as' => 'app-auth.' ] , function() {
	\Illuminate\Support\Facades\Auth::routes();
} );
Route::group( [ 'as' => 'admin-auth.' , 'prefix' => 'v-admin' , 'namespace' => 'Admin' ] , function() {
	Route::redirect( '' , 'v-admin/login' );
	\Illuminate\Support\Facades\Auth::routes( [ 'register' => FALSE , 'emailVerify' => FALSE ] );

} );
Route::group( [ 'namespace' => 'Admin' , 'prefix' => 'v-admin' , 'as' => 'admin.' , 'middleware' => 'auth:admin' ] , function() {
	Route::get( '' , 'AdminController@dashboard' )->name( 'dashboard' );
	Route::group(['prefix'=>'profile','as'=>'profile.'],function(){
		Route::get('','AdminController@profile')->name('show');
		Route::post('edit/profile','AdminController@change')->name('change');
		Route::post('edit/password','AdminController@password')->name('password');
	});
	Route::group( [ 'prefix' => 'users' , 'as' => 'user.' ] , function() {
		Route::get( '' , 'UserController@index' )->name( 'index' );
		Route::delete( 'delete/{user}' , 'UserController@delete' )->name( 'delete' );
		Route::get( '/show/{user}' , 'UserController@show' )->name( 'show' );
	} );
	Route::group( [ 'prefix' => 'contants' , 'as' => 'contant.' ] , function() {
		Route::get( 'delete/{contant}' , 'ContantController@delete' )->name( 'delete' );
		Route::get( '' , 'ContantController@index' )->name( 'index' );
		Route::get('create','ContantController@create')->name('create');
		Route::post('store','ContantController@store')->name('store');
	} );
	Route::group( [ 'prefix' => 'account' , 'as' => 'account.' ] , function() {
		Route::get( '' , 'AdminController@index' )->name( 'index' );
		Route::get( 'create' , 'AdminController@create' )->name( 'create' );
		Route::post( 'create' , 'AdminController@store' )->name( 'store' );
		Route::delete( 'delete/{admin}' , 'AdminController@delete' )->name( 'delete' );
		Route::get( 'edit/{admin}' , 'AdminController@edit' )->name( 'edit' );
		Route::patch( 'update/{admin}' , 'AdminController@update' )->name( 'update' );
		Route::get('show/{admin}' , 'AdminController@show')->name('show');
	} );
	Route::group( [ 'prefix' => 'houses' , 'namespace' => 'House' , 'as' => 'houses.' ] , function() {
		Route::get( '' , 'HousesController@index' )->name( 'index' );
		Route::get( '/create' , 'HousesController@create' )->name( 'create' );
		Route::post( '/create' , 'HousesController@store' )->name( 'store' );
		Route::get( '/edit/{house}' , 'HousesController@edit' )->name( 'edit' );
		Route::post( '/edit/{house}' , 'HousesController@update' )->name( 'update' );
		Route::get( '/delete/{house}' , 'HousesController@delete' )->name( 'delete' );
    } );
	Route::group( [ 'prefix' => 'tickets' , 'as' => 'ticket.' ] , function() {
		Route::get( '' , 'TicketController@index' )->name( 'index' );
		Route::post( 'edit/{id}' , 'TicketController@update' )->name( 'update' );
		Route::get( 'edit/{ticket}' , 'TicketController@edit' )->name( 'edit' );
	} );
	Route::group( [ 'prefix' => 'agents' , 'as' => 'agent.' ] , function() {
		Route::get( '' , 'AgentController@index' )->name( 'index' );
		Route::get( 'edit/{agent}' , 'AgentController@edit' )->name( 'edit' );
		Route::post( 'edit/{id}' , 'AgentController@update' )->name( 'update' );
	} );
	Route::group( [ 'prefix' => 'blogs' , 'as' => 'blog.' , 'namespace' => 'Blog' ] , function() {
		Route::group( [ 'prefix' => 'categories' , 'as' => 'categories.' ] , function() {
            Route::get( '' , 'CategoryController@index' )->name( 'index' );
            Route::get( 'create' , 'CategoryController@create' )->name( 'create' );
			Route::post( 'create' , 'CategoryController@store' )->name( 'store' );
			Route::get( 'edit/{category}' , 'CategoryController@edit' )->name( 'edit' );
			Route::patch( 'update/{category}' , 'CategoryController@update' )->name( 'update' );
			Route::delete( 'delete/{category}' , 'CategoryController@delete' )->name( 'delete' );
		} );
		Route::group( [ 'prefix' => 'comments' , 'as' => 'comments.' ] , function() {
			Route::get( '' , 'CommentController@index' )->name( 'index' );
			Route::get( 'delete/{comment}' , 'CommentController@delete' )->name( 'delete' );
			Route::post( 'confirm/{comment}' , 'CommentController@confirm' )->name( 'confirm' );
			Route::post( 'update' , 'CommentController@update' )->name( 'update' );
		} );
		Route::group( [ 'prefix' => 'articles' , 'as' => 'articles.' ] , function() {
			Route::get( 'create' , 'ArticleController@create' )->name( 'create' );
			Route::get( '' , 'ArticleController@index' )->name( 'index' );
			Route::post( 'store' , 'ArticleController@store' )->name( 'store' );
			Route::get( 'delete/{article}' , 'ArticleController@delete' )->name( 'delete' );
			Route::get( 'edit/{article}' , 'ArticleController@edit' )->name( 'edit' );
			Route::post( 'update/{article}' , 'ArticleController@update' )->name( 'update' );
			Route::post('uploadImage' , 'ArticleController@uploadImage')->name('uploadImage');
	} );
		} );
	Route::group( [ 'prefix' => 'options' , 'as' => 'options.' ] , function() {
		Route::post('up-image' , 'OptionController@UploadImage') ->name('upImage');
		Route::group(['prefix'=>'about' , 'as'=>'about.'],function(){
			Route::get('','OptionController@about')->name('view');
			Route::post('','OptionController@saveAbout')->name('store');
		});
		Route::group(['prefix'=>'form/buyer' , 'as'=>'form.buyer.'],function(){
			Route::get('','OptionController@buyer')->name('view');
			Route::post('','OptionController@saveBuyer')->name('store');
		});
		Route::group(['prefix'=>'form/seller' , 'as'=>'form.seller.'],function(){
			Route::get('','OptionController@seller')->name('view');
			Route::post('','OptionController@saveSeller')->name('store');
		});
		Route::group( [ 'prefix' => 'home' , 'as' => 'home' ] , function() {
            Route::get( '' , 'OptionController@Home' );
            Route::post( '' , 'OptionController@UpdateHome' );
			Route::post( 'upload/ImageSlide' , 'OptionController@imageSlider' )->name( '.imageSlider' );
			Route::post( 'upload/ImageQSearch' , 'OptionController@ImageQSearch' )->name( '.ImageQSearch' );
		} );
		Route::group( [ 'prefix' => 'general' , 'as' => 'general' ] , function() {
			Route::get( '' , 'OptionController@General' );
			Route::post( '' , 'OptionController@UpdateGeneral' );
			Route::post('add/media','OptionController@addMedia')->name('addMedia');
		} );
	} );
} );
Route::group( [ 'as' => 'App.' , 'namespace' => 'App' ] , function() {
	Route::get( '' , "HomeController@Home" )->name( 'Home' );
	Route::group( [ 'as' => 'profile' , 'prefix' => 'profile' , 'middleware' => 'auth' ] , function() {
        Route::get( '' , 'UserController@profile' )->name( '' );
        Route::post( 'change-avatar' , 'UserController@avatar' )->name( '.change-avatar' );
		Route::post( 'changePassword' , 'UserController@changePassword' )->name( '.changePassword' );
		Route::post( 'updateProfile' , 'userController@update' )->name( '.update' );
	} );
	Route::get('about','PageController@about')->name('about');
	Route::get( 'contact' , 'ContantController@contact' )->name( 'contact' );
	Route::group( [ 'prefix' => 'blogs' , 'as' => 'blog.' , 'namespace' => 'Blog' ] , function() {
		Route::get( 'article/{article}' , 'ArticleController@show' )->name( 'show' );
		Route::get( 'archive' , 'ArticleController@archive' )->name( 'archive' );
        Route::post('store/{article}', 'ArticleController@store')->name('store');
	} );
	Route::group( [ 'prefix' => 'houses' , 'as' => 'house.' , 'namespace' => 'House' ] , function() {
		Route::get( '/{MLSNumber}/{Address}' , 'HouseController@show' )->name( 'show' );
		Route::get( '' , 'HouseController@archive' )->name( 'index' );
	} );
	Route::group( [ 'prefix' => 'forms' , 'as' => 'form.' ] , function() {
		Route::post( 'buy' , 'TicketController@Buy' )->name( 'buy' );
		Route::get( 'sell' , 'TicketController@showSellingForm' )->name( 'showSellingForm' );
		Route::get( 'buy' , 'TicketController@showBuyingForm' )->name( 'showBuyingForm' );
		Route::post( 'sell' , 'TicketController@Sell' )->name( 'sell' );
		Route::post( 'ticket' , 'TicketController@contact' )->name( 'TicketContact' );
        Route::post( 'store' , 'TicketController@store' )->name( 'store' );
    } );
} );
