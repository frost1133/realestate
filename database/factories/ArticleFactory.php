<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Factory;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
		'body'          => $faker->paragraph( rand(22, 50) ) . "<img style='width: 100%;' src={$faker->imageUrl()} >" ,
        'featuredImage' => $faker->imageUrl(),
        'slug' => $faker->word,
        'tag' => json_encode(Factory::create()->words(5)),
        'admin_id' => \App\Admin::all()->random()->id,
    ];
});
