<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define( Category::class, function (Faker $faker) {
	$data = [
		'name' => $faker->word(),
	];
	$data ['slug'] = $data['name'];
	$data ['parentCategory'] = $faker->shuffle([null , Category::all()->isNotEmpty()?Category::all()->random()->id:NULL])[0];
    return  $data;
});
