<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'title' => $faker->title(),
        'body' => $faker->text(100),
        'user_id' => \App\User::all()->random()->id,
        'parentComment' => $faker->shuffle([Null, Comment::all()->isNotEmpty()?Comment::all()->random()->id:Null])[0],
        'article_id'=>\App\Article::all()->random()->id
        ];
});
