<?php

use Illuminate\Database\Seeder;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            \Illuminate\Support\Facades\DB::table('admins')->insert([
            	'name' => 'Admin',
            	'email' => 'Admin@state.com',
            	'password' => \Illuminate\Support\Facades\Hash::make('Admin'),
            ]);
    }
}
