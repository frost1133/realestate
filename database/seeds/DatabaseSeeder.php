<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run ()
	{
		
		$this->call( [ Admin::class , Permission::class , User::class , Category::class , Article::class , Comment::class , OptionSeeder::class ] );
	}
}
