<?php

use Illuminate\Database\Seeder;

class House extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        for ($i=0;$i<10;$i++){
            \App\House::create([

                'address'=>$faker->address,
                'Price'=>$faker->buildingNumber,
                'MonthlyRent'=>$faker->buildingNumber,
                'MLSNumber'=>$faker->buildingNumber,
                'RentalID'=>$faker->buildingNumber,
                'postalcode'=>$faker->buildingNumber,
                'TaxAmount'=>$faker->buildingNumber,
                'LotSize'=>$faker->buildingNumber,
                'maint_fee'=>$faker->buildingNumber,
                'Zoning'=>$faker->word(),
                'Features'=>$faker->text,
                'yearBuilt'=>$faker->year(),
                'TaxBuilt'=>$faker->year(),
                'term'=>$faker->text(10),
                'Security_Deposit'=>$faker->text(10),
                'virtualtour_url'=>$faker->text(),
                'furnishing'=>$faker->text(10),
                'utilities'=>$faker->text(10),
                'TypeofDwelling'=>$faker->text(20),
                'StyleofHome'=>$faker->text(20),
                'parking'=>$faker->text(10),
                'manager'=>$faker->text(10),
                'status'=>$faker->text(10),
                'RentalStatus'=>$faker->text(10),
                'FloorArea'=>$faker->text(20),
                'availability'=>$faker->date('Y-m-d'),
                'BasementArea'=>$faker->text(20),
                'dynamic_embed'=>$faker->text(),
                'Bathrooms'=>$faker->numberBetween(1,20),
                'Bedrooms'=>$faker->numberBetween(1,20),
                'pets'=>$faker->numberBetween(1,10),
                'smoking'=>$faker->boolean,
                'Area'=>$faker->state,
                'SubArea'=>$faker->state,
                'longDescription'=>$faker->text,
                'SiteInfluences'=>$faker->name,
                'Amenities'=>$faker->name,
                'location'=>$faker->text,
                'street'=>$faker->text,
                'video'=>$faker->url,
                'img'=>$faker->url,
                'type'=>$faker->boolean,
                'admin_id'=>\Illuminate\Support\Facades\DB::table('admins')->first(['id'])->id,

            ]);
        }
    }
}
