<?php

use Illuminate\Database\Seeder;

class Request extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {

     for($i=0;$i<5;$i++){
         App\Ticket::create([
             'name'=>$faker->name,
             'email'=>$faker->email,
             'phone'=>$faker->phoneNumber,
             'area'=>$faker->name,
             'propertyType'=>$faker->name,
             'bedrooms'=>$faker->numberBetween(1,20),
             'bathrooms'=>$faker->numberBetween(1,20),
             'sqFt'=>$faker->buildingNumber,
             'when'=>$faker->time('H:i:s'),
             'message'=>$faker->text,
             'tracingCode'=>$faker->numberBetween(1,65488),
             'requestType'=>$faker->numberBetween(1,3),
             'state'=>$faker->state,
             'admin_id'=>\Illuminate\Support\Facades\DB::table('admins')->first('id')->id,
             'agent_id'=>\Illuminate\Support\Facades\DB::table('admins')->first('id')->id,
             'user_id'=>\Illuminate\Support\Facades\DB::table('users')->first('id')->id,

         ]);
     }
            }



}
