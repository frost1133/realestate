<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run ()
	{
		\App\Option::insert( [
			[
				'name'  => 'Contacts' ,
				'value' => '[{"Icon":"mail","Name":"Email","Value":"Test@gmail.com"}]'
			] ,
			[
				'name'  => 'HomeSlider' ,
				'value' => '{"Title":"RealState","Slides":[{"Image":"\/storage\/home\/slider\/7NiwG4LaJENItg2cJ4RWw0PNX9Yw6guToJ44OnHa.jpeg","Alt":"Image 1"}]}'
			] ,
			[
				'name'  => 'QSearch' ,
				'value' => '{"QSearchRow1":[{"Image":"\/storage\/home\/qsearch\/SzUoRUvQhRsKKmKcE4p5ven73YXHRKPnQaVHIZp4.jpeg","Alt":"Image 1","Link":"#"},{"Image":"\/storage\/home\/qsearch\/Ixkg2jEy0j15hpT1mEbpsWz6stnDsNiCoHBxm52G.jpeg","Alt":"Image 2","Link":"#"},{"Image":"\/storage\/home\/qsearch\/DwmsYljkn9nNFbET32vr5gjjT0QOODVokiLq44Az.jpeg","Alt":"Image 3","Link":"#"}],"QSearchRow2":[{"Image":"\/storage\/home\/qsearch\/Aujc40M1OJENPESEHoRR02FtxEzMzDnYhyug19hy.jpeg","Alt":"Image 4","Link":"#"},{"Image":"\/storage\/home\/qsearch\/lXQ85FWIGVUVls0hZgJRgJva39j4pEFpzdUJcuh7.jpeg","Alt":"Image 5","Link":"#"}],"Title":"Houses Categories"}'
			] ,
			[
				'name'  => 'HomeAbout' ,
				'value' => '{"Title":"About Us","Body":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eu lobortis elementum nibh tellus. Euismod nisi porta lorem mollis aliquam ut. Fusce ut placerat orci nulla pellentesque dignissim enim sit amet. Elementum curabitur vitae nunc sed velit dignissim sodales ut eu.","Image":"\/storage\/home\/about\/WCCEkDCs4AMMEWgHGQJGfbs3RbDaF7Lok4ftWawP.png"}'
			] ,
			[
				'name'  => 'HomeBuyerSeller' ,
				'value' => '{"Title":"Buyer And Seller","Body":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Laoreet suspendisse interdum consectetur libero id faucibus. Dui nunc mattis enim ut tellus.","Image":"\/storage\/home\/buyerseller\/p5Czeobz3VouQX3uQI0KkAILVHECXUw5dwvs8fDT.jpeg"}'
			] ,
			[
				'name'  => 'HomeTestimonials' ,
				'value' => '[{"Name":"kadkhoda","Title":"mattis pellentesque id nibh","Body":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit at imperdiet dui accumsan sit amet.","Image":"\/storage\/home\/testimonials\/WpigTIcIdTC95NACvXneblwqQvUNNJp6Gsh0suA4.jpeg"},{"Name":"talebi","Title":"eget duis at tellus at","Body":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc sed. Enim facilisis gravida neque convallis a.","Image":"\/storage\/home\/testimonials\/TAe8rvQzxQugY0ku3qbFscYxwO80CzkwhdLbCh7N.jpeg"},{"Name":"Hasani","Title":"consectetur adtipiscing elit","Body":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc sed. Enim facilisis gravida neque convallis a.","Image":"\/storage\/home\/testimonials\/qaJHLBT5iycnTcF50DhbNZUQx8dktQww6C4hiFUV.jpeg"}]'
			] ,
			[
				'name'  => 'SocialMedia' ,
				'value' => '[{"Icon":"facebook","Name":"Facebook","Link":"http:\/\/facebook.com"},{"Icon":"twitter","Name":"Twitter","Link":"http:\/\/twitter.com"}]'
			] ,
		] );
	}
}
