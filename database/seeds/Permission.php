<?php

use Illuminate\Database\Seeder;

class Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('permissions')->insert([
        	[
        		'admin_id' => \App\Admin::first()->id,
		        'permission' => '1'
	        ],
        	[
                'admin_id' => \App\Admin::first()->id,
		        'permission' => '2'
	        ],
        	[
                'admin_id' => \App\Admin::first()->id,
		        'permission' => '3'
	        ],
        ]);
    }
}
