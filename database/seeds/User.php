<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
    	   \App\User::create([
    	   	'name' =>   'UserTest',
	        'email' => 'user@state.com',
	        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'
	       ]);
           factory(\App\User::class , 20)->create();
    }
}
