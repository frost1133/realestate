<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create( 'comments' , function( Blueprint $table ) {
			$table->id();
			$table->string( 'title' );
			$table->mediumText( 'body' );
			$table->foreignId('article_id');
			$table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
			$table->foreignId( 'user_id' )->nullable();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
			$table->foreignId( 'admin_id' )->nullable();
			$table->foreign( 'admin_id' )->references( 'id' )->on( 'admins' )->onDelete( 'cascade' );
			$table->foreignId( 'parentComment' )->nullable();
			$table->foreign( 'parentComment' )->references( 'id' )->on( 'comments' );
			$table->timestamp( 'confirmComment' )->nullable();
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::dropIfExists( 'comments' );
	}
}
