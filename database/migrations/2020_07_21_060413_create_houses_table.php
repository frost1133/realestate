<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up ()
	{
		Schema::create( 'houses' , function( Blueprint $table ) {
			$table->id();
			$table->foreignId( 'admin_id' );
			$table->foreign( 'admin_id' )->references( 'id' )->on( 'admins' )->cascadeOnDelete()->cascadeOnUpdate();
			$table->string( 'postalCode' , '20' )->index();
			$table->string( 'MLSNumber' , '20' )->nullable();
			$table->string( 'rentalID' , '20' )->nullable();
			$table->string( 'address' )->nullable();
			$table->string( 'type' )->nullable();
			$table->bigInteger( 'price' )->nullable();
			$table->integer( 'monthlyRent' )->nullable();
			$table->integer( 'maintFee' )->nullable();
			$table->string( 'pets'  )->nullable();
			$table->string( 'smoking' )->nullable();
			$table->string( 'securityDeposit' )->nullable();
			$table->string( 'term' )->nullable();
			$table->string( 'manager' )->nullable();
			$table->string( 'availability' )->nullable();
			$table->string( 'furnishing' )->nullable();
			$table->string( 'utilities' )->nullable();
			$table->string( 'parking' )->nullable();
			$table->integer( 'taxAmount' )->nullable();
			$table->integer( 'lotSize' )->nullable();
			$table->string( 'zoning' )->nullable();
			$table->string('propertyType')->nullable();
			$table->string( 'typeofDwelling' )->nullable();
			$table->string( 'styleOfHome' )->nullable();
			$table->string( 'virtualTour' )->nullable();
			$table->text( 'features' )->nullable();
			$table->string( 'yearBuilt' )->nullable();
			$table->string( 'taxBuilt' )->nullable();
			$table->string( 'basementArea' )->nullable();
			$table->tinyInteger( 'bathrooms' )->nullable();
			$table->tinyInteger( 'bedrooms' )->nullable();
			$table->string( 'area' )->nullable();
			$table->string( 'subArea' )->nullable();
			$table->string( 'floorArea' )->nullable();
			$table->longText( 'description' )->nullable();
			$table->text( 'siteInfluences' )->nullable();
			$table->text( 'amenities' )->nullable();
			$table->longText( 'img' )->nullable();
			$table->longText( 'video' )->nullable();
			$table->longText( 'dynamicEmbed' )->nullable();
			$table->string( 'status' )->nullable();
			$table->string( 'rentalStatus' )->nullable();
			$table->text( 'location' )->nullable();
			$table->timestamps();
			$table->string( 'metaTitle' )->nullable();
			$table->string( 'metaKeywords' )->nullable();
			$table->mediumText( 'metaDescription' )->nullable();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down ()
	{
		Schema::dropIfExists( 'houses' );
	}
}
