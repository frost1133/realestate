<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('area')->nullable();
            $table->string('streetAddress')->nullable();
            $table->string('city')->nullable();
            $table->string('propertyType')->nullable();
            $table->string('priceRange')->nullable();
            $table->tinyInteger('bedrooms')->nullable();
            $table->tinyInteger('bathrooms')->nullable();
            $table->integer('sqFt')->nullable();
            $table->integer('lotSize')->nullable();
            $table->string('when')->nullable();
            $table->text('message')->nullable();
            $table->string('tracingCode')->nullable();
            $table->smallInteger('ticketType')->nullable();
            $table->smallInteger('state')->nullable();
            $table->foreignId('admin_id')->nullable();
            $table->foreignId('agent_id')->nullable();
            $table->foreign('agent_id')->references('id')->on('admins')->onDelete('SET NULL');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
