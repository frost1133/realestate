<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('body');
            $table->string('slug');
            $table->string('tag')->nullable();
            $table->string('featuredImage');
            $table->smallInteger('status')->default(1);
            $table->foreignId('admin_id')->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->timestamps();
            $table->timestamp('releaseTime')->default(DB::raw('CURRENT_TIMESTAMP'));
	        $table->string('metaTitle')->nullable();
            $table->string('metaKeywords')->nullable();
            $table->mediumText('metaDescription')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
