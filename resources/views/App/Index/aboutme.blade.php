<section class="container-fluid about-me py-3">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 m-auto  order-2 order-lg-1">
				<div class="title-aboutme mb-4">
					<h3 class="clr-ff fs-35">{{$About['Title']}}</h3>
				</div>
				<div class="des-aboutme">
					<p class="clr-ef fs-19">
						{!! $About['Body'] !!}
					</p>
				</div>
				<a href="{{route('App.about')}}" class="btn btn-secondary hd-font fs-20 mt-3 px-5 py-2">more</a>
			</div>
			<div class="col-lg-6  order-1 order-lg-2">
				<img class="p-0 p-lg-5" src="{{$About['Image']??'images/person.png'}}" alt="Amir Hamzehali">
			</div>
		</div>
	</div>
</section>
