<section class="container-fluid testimonials py-5 ">
	<div class="container">
		<div class="row title-section py-4">
			<div class="col-12 ">
				<div class="title-area text-center">
					<h3 class="clr-ff fs-40">
						Recent Blog
					</h3>
				</div>
			</div>
		</div>
		<div class="row mt-4 ">
			<div class="col-12 blog-slider">
				<div class="owl-carousel owl-theme">
					@foreach($resentArticle as $article)
						<div class="item">
							<div class="blog-box bx-sh">
								<a href="{{route('App.blog.show' , $article)}}">
									<figure>
										<div class="blog-img">
											<img src="{{$article->featuredImage}}" alt="">
										</div>
										<div class="blog-title ">
											<h3>{{$article->title}}</h3>
										</div>
										<div class="blog-des clr-ef">
											<p>{!! Str::words($article->body,30,'...') !!}</p>
										</div>
									</figure>
								</a>
							</div>
						</div>
					@endforeach
				
				</div>
			</div>
		
		</div>
	</div>

</section>
