<section class="container-fluid bg-overly calltoactione py-5"  style="background: url({!! $BuyerSeller['Image'] !!})">
    <div class="container ">
        <div class="row title-section py-4">
            <div class="col-12 ">
                <div class="title-area text-center">
                    <h3 class="clr-ff fs-40">
                        {{$BuyerSeller['Title']}}
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 m-auto text-center clr-ff ">
                <div class="des-cta">
                    <p class="fs-20 p-lg-5">
                        {{$BuyerSeller['Body']}}
                    </p>
                </div>
                <a href="{{route('App.form.showSellingForm')}}">
                    <button type="submit" class="btn btn-primary submit-search hd-font m-1 fs-22">Seller</button>
                </a>
                <a href="{{route('App.form.showBuyingForm')}}">
                    <button type="submit" class="btn btn-primary submit-search hd-font m-1 fs-22">Buyer</button>
                </a>
            </div>
        </div>
    </div>
    </div>
</section>
