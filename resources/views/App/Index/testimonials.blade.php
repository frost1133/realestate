<section class="container-fluid testimonials py-5 ">
	<div class="container">
		<div class="row title-section py-4">
			<div class="col-12 ">
				<div class="title-area text-center">
					<h3 class="clr-ff fs-40">
						Testimonials
					</h3>
				</div>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-lg-12 testimonials-slider">
				<div class="owl-carousel owl-theme">
					@foreach($Testimonials as $Testimonial)
						<div class="item">
							<div class="box-testimonials bg-2c">
								<div class="row">
									<div class="col-lg-3 m-auto bg-testi py-4">
										<div class="box-img m-auto">
											<img src="{{$Testimonial['Image']}}" alt="Avatar {{$Testimonial['Name']}}">
										</div>
									</div>
									<div class="col-lg-9 pt-3">
										<div class="box-name text-center text-lg-left hd-font clr-ff fs-23">
											{{$Testimonial['Name']}}
										</div>
										<div class="box-subname text-center text-lg-left clr-ef fs-14 mt-3">
											<span>{{$Testimonial['Title']}}</span>
										</div>
										<hr>
										<div class="box-des text-center text-lg-left clr-ef mt-3 pr-lg-4">
											<p class=" fs-17">{{$Testimonial['Body']}}<p>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
