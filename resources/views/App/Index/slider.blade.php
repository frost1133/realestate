<section class="container-fluid landing p-0">
	<div class="container">
		<div class="row">
			<div class="col-12 slider-area">

				</div>
				<!-- slider -->
				<!-- <div class="owl-carousel owl-theme bx-sh">
					@foreach($Slider['Slides'] as $Slide)
						<div class="item">
							<a href=""><img src="{{$Slide['Image']}}" alt="{{$Slide['Alt']}}"></a>
						</div>
					@endforeach
				</div> -->
			</div>
		</div>
		<div class="row m-0 p-0">
			<div class="fullscreen-bg position-absolute col-12 p-0">
		    <video loop muted autoplay poster="img/videoframe.jpg" class="d-none d-lg-block fullscreen-bg__video">
		      <source src="../images/Luxury.mp4">
		    </video>
				<div class="search-area m-auto  ">
					<div class="title-search clr-ff m-auto text-center">
						<div class="title-area bx-sh px-5 py-2 my-4 d-inline-block">
							<h3 class="m-0 fs-40 ">{{$Slider['Title']}}</h3>
						</div>
					</div>
					<div class="search-form text-center">
						<form>
							<div class="form-row">
								<div class="col-md-2">
								</div>
								<div class="form-group col-md-4">
									<select id="inputState" class="form-control">
										<option selected>Type or Choose City / Neighborhood</option>
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
										<option>6</option>
										<option>7</option>
										<option>8</option>
										<option>9</option>
									</select>
								</div>
								<div class="form-group col-md-2">
									<select id="inputState" class="form-control">
										<option selected>All Property Types</option>
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
										<option>6</option>
										<option>7</option>
										<option>8</option>
										<option>9</option>
									</select>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary btn-block submit-search hd-font m-0 fs-20">
										Find
									</button>
								</div>
							</div>
						</form>
					</div>
			</div>
		</div>
	</div>
</section>
