<section class="container-fluid feature-Property pt-5 pb-2 ">
	<div class="container">
		<div class="row title-section py-4 mb-5">
			<div class="col-12 ">
				<div class="title-area text-center">
					<h3 class="clr-ff fs-40">
						Featured Properties
					</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 mb-3">
				<a href="#">
					<div class="box-feature bx-sh">
						<div class="feature-img">
							<img src="images/acc/feature01.jpg" alt="">
						</div>
						<div class="feature-content bx-sh">
							<div class="feature-title clr-ff fs-22 ">
								<h3>FAKE LISTING EXAMPLE</h3>
							</div>
							<div class="feature-address clr-ff fs-20 bd-font mb-4">
								<span>House/Single Family in Sub-Area</span>
							</div>
							<div class="more mb-2">
								<a href="#">Brokerage Here</a>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-6  mb-3">
				<a href="#">
					<div class="box-feature">
						<div class="feature-img">
							<img src="images/acc/feature02.jpg" alt="">
						</div>
						<div class="feature-content bx-sh">
							<div class="feature-title clr-ff fs-22">
								<h3>FAKE LISTING EXAMPLE</h3>
							</div>
							<div class="feature-address clr-ff fs-20 bd-font mb-4">
								<span>House/Single Family in Sub-Area</span>
							</div>
							<div class="more mb-2">
								<a href="#">Brokerage Here</a>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	</div>
</section>
