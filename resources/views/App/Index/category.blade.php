<section class="container-fluid category-home">
	<div class="container">
		<div class="row title-section py-4">
			<div class="col-12 ">
				<div class="title-area text-center">
					<h3 class="clr-ff fs-40">
						{{$QSearch['Title']}}
					</h3>
				</div>
			</div>
		</div>
		<div class="row">
			@foreach($QSearch['QSearchRow1'] as $QS1)
				<div class="col-lg-4 culom-4 mt-4">
					<div class="banner-area ">
						<a href="{{$QS1['Link']}}">
							<img src="{{$QS1['Image']}}" alt="{{$QS1['Alt']}}">
						</a>
					</div>
				</div>
			@endforeach
			@foreach($QSearch['QSearchRow2'] as $QS2)
				<div class="col-lg-6 culom-4  mt-4">
					<div class="banner-area ">
						<a href="{{$QS2['Link']}}">
							<img src="{{$QS2['Image']}}" alt="{{$QS2['Alt']}}">
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>