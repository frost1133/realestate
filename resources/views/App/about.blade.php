@extends('App.Template.layout')
@section('content')
	<section class="container-fluid archive-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 bg-single" style="background-image: url({{$page['headerImage']}});">
					<div class="info-archive ">
						<div class="title-single position-relative">
							<h1 class="clr-ff p-2 fs-40">About</h1>
						</div>
						<div class="bread-crumb position-relative text-center">
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb clr-ff">
									<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
									<li class="breadcrumb-item"><a href="javascript:;">About</a></li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <section class="container-fluid about-me py-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 m-auto">
                    <div class="title-aboutme mb-4">
                        <h3 class="clr-ff fs-35">{{$page['title']}}</h3>
                    </div>
                    <div class="des-aboutme">
                        <p class="clr-ef fs-19">
                           {!! $page['body'] !!}
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <img class="p-5" src="{{$page['featureImage']}}" alt="Amir Hamzehali">
                </div>
            </div>
        </div>
    </section>
@endsection
@section('PageStyles')
    <style>
    p{
        color: white;
    }
    </style>

@endsection
