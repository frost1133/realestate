@extends('App.Template.layout')
@section('content')
	<section class="container-fluid Single-feature">
		<div class="container">
			<div class="row">
				<!--				--><?php $house = $house ?? new \App\House() ?>
				<div class="col-lg-12">
					<div class=" bg-single" style="background: url({{$house->img[0]}}) ;">
						<div class="title-home position-relative">
							<h1 class="clr-ff p-2 d-inline-block bx-sh ">{{$house->address}}</h1>
						</div>
						<div class="address-home d-inline-block position-relative">
							<h4 class="clr-ff bd-font">{{$house->propertyType??"Apartment/Condo"}} For Sale
								In {{$house->subArea}},
								{{$house->area}}</h4>
						</div>
						<div class="price-home float-right position-relative">
							<span>${{number_format($house->price , 0 , '.' , ',')}}</span>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid Single-info mt-3">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bg-infosingle p-1">
						<figure class="icon-bed d-inline-block">
							<img src="/images/bed.svg" alt="">
							<span>{{$house->bedrooms}} BEDS</span>
						</figure>
						<figure class="icon-bath d-inline-block">
							<img src="/images/bathtub.svg" alt="">
							<span>{{$house->bathrooms}} BATHS</span>
						</figure>
						<figure class="icon-feet d-inline-block">
							<img src="/images/squares.svg" alt="">
							<span>{{$house->lotSize}} SQ. FT.</span>
						</figure>
						<figure class="icon-built d-inline-block">
							<img src="/images/calendar.svg" alt="">
							<span>BUILT {{$house->yearBuilt}}</span>
						</figure>
						<div class="btn-rq d-inline-block float-right">
							<a href="" class="btn btn-primary btn-round hd-font fs-16 rq-info">Request More Info</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid Single-content mt-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 order-2 order-md-1">
					<div class="home-gallery">
						<div class="owl-carousel owl-theme ">
							@foreach($house->img as $img)
								<div class="item bx-sh">
									<img src="{{$img}}" alt="">
								</div>
							@endforeach
						</div>
					</div>
					<div class="des-box clr-ef mt-3">
						<p>{!! $house->longDescription !!}
						</p>
					</div>
					<div class="row custom-field mt-3">
						<div class="col-lg-6">
							<div class="row odd Address">
								<div class="col-4 header-fied">
									<span>Address</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->address}}</span>
								</div>
							</div>
							<div class="row even List-Price">
								<div class="col-4 header-fied">
									<span>List Price</span>
								</div>
								<div class="col-8 value-field">
									<span>${{number_format($house->price , 0 , '.' , ',')}}</span>
								</div>
							</div>
							<div class="row odd Property-Type">
								<div class="col-4 header-fied">
									<span>Property Type</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->propertyType??"Apartment/Condo"}}</span>
								</div>
							</div>
							<div class="row even Dwelling">
								<div class="col-4 header-fied">
									<span>Type of Dwelling</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->typeofDwelling??'None'}}</span>
								</div>
							</div>
							<div class="row odd Style-Home">
								<div class="col-4 header-fied">
									<span>Style of Home</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->styleOfHome}}</span>
								</div>
							</div>
							<div class="row even Area">
								<div class="col-4 header-fied">
									<span>Area</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->area}}</span>
								</div>
							</div>
							<div class="row odd Sub-Area">
								<div class="col-4 header-fied">
									<span>Sub Area</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->subArea}}</span>
								</div>
							</div>
							<div class="row even Bedrooms">
								<div class="col-4 header-fied">
									<span>Bedrooms</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->bedrooms}}</span>
								</div>
							</div>
							<div class="row odd Bethrooms">
								<div class="col-4 header-fied">
									<span>Bethrooms</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->bathrooms}}</span>
								</div>
							</div>
							<div class="row even Floor-Area">
								<div class="col-4 header-fied">
									<span>Floor Area</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->lotSize}} Sq. Ft.</span>
								</div>
							</div>
							<div class="row odd Year-Built">
								<div class="col-4 header-fied">
									<span>Year Built</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->yearBuilt}}</span>
								</div>
							</div>
							<div class="row even Maint-Fee">
								<div class="col-4 header-fied">
									<span>Maint. Fee</span>
								</div>
								<div class="col-8 value-field">
									<span>${{number_format($house->maintFee , 2 , '.' , ',')}}</span>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="row odd MLS">
								<div class="col-4 header-fied">
									<span>MLS® Number</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->MLSNumber}}</span>
								</div>
							</div>
							<div class="row even Brokerage">
								<div class="col-4 header-fied">
									<span>Listing Brokerage</span>
								</div>
								<div class="col-8 value-field">
									<span>Sutton Group-West Coast Realty (Abbotsford)</span>
								</div>
							</div>
							<div class="row odd Basement">
								<div class="col-4 header-fied">
									<span>Basement Area</span>
								</div>
								<div class="col-8 value-field">
									<span>None</span>
								</div>
							</div>
							<div class="row even Postal">
								<div class="col-4 header-fied">
									<span>Postal Code</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->postalCode}}</span>
								</div>
							</div>
							<div class="row odd Zoning">
								<div class="col-4 header-fied">
									<span>Zoning</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->zoning}}</span>
								</div>
							</div>
							<div class="row even Tax-Amount">
								<div class="col-4 header-fied">
									<span>Tax Amount</span>
								</div>
								<div class="col-8 value-field">
									<span>${{number_format($house->taxAmount , 2 , '.' , ',')}}</span>
								</div>
							</div>
							<div class="row odd Tax-Year">
								<div class="col-4 header-fied">
									<span>Tax Year</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->taxBuilt}}</span>
								</div>
							</div>
							<div class="row even Pets">
								<div class="col-4 header-fied">
									<span>Pets</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->pets}}</span>
								</div>
							</div>
							<div class="row odd Site">
								<div class="col-4 header-fied">
									<span>Site Influences</span>
								</div>
								<div class="col-8 value-field">
									<span>Central Location, Shopping Nearby</span>
								</div>
							</div>
							<div class="row even Features">
								<div class="col-4 header-fied">
									<span>Features</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->features}}</span>
								</div>
							</div>
							<div class="row odd Amenities">
								<div class="col-4 header-fied">
									<span>Amenities</span>
								</div>
								<div class="col-8 value-field">
									<span>{{$house->amenities??'None'}}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 order-1 order-md-2">
					<div class="row">
						<div class="col-12">
							<div class="btn-content">
								<a href="" class="btn btn-primary btn-round btn-block hd-font fs-14 float-right view-tour">
									<i class="fal fa-video clr-ef te-sh fs-23"></i>View Virtual Tour</a>
								<a href="" class="btn btn-primary btn-round hd-font btn-block fs-14 float-right calculator-btn" data-toggle="modal" data-target="#calculator"><i class="fal fa-calculator clr-ef te-sh fs-23"></i>
									Calculate Mortgage</a>
								<a href="" class="btn btn-primary btn-round hd-font btn-block fs-14 float-right Share-property" data-toggle="modal" data-target="#shareButtons"><i class="fal fa-share-alt clr-ef te-sh fs-23"></i>
									Share Property</a>
							</div>
						</div>
						<div class="col-12">
							<div class="tab-map">
								<ul class="nav nav-pills nav-pills">
									<li class="nav-item">
										<a class="hd-font fs-20 nav-link active" href="#pill1" data-toggle="tab">Map</a>
									</li>
									<li class="nav-item">
										<a class="hd-font fs-20 nav-link" href="#pill2" data-toggle="tab">Street</a>
									</li>
								</ul>
								<div class="tab-content tab-space">
									<div class="tab-pane active" id="pill1">
										<img src="/images/acc/map.jpg" alt="">
										<br><br>
										<a class="seegooglemap hd-font text-center d-block" href="#">View on Google
											Maps</a>
									</div>
									<div class="tab-pane" id="pill2">
										<p class="clr-ff">Efficiently unleash cross-media information without
											cross-media value. Quickly maximize timely deliverables for real-time
											schemas.</p>
										<br><br>
										<p class="clr-ff">Dramatically maintain clicks-and-mortar solutions without
											functional solutions.</p>
									</div>
								</div>
							</div>
							<div class="tab-map">
								<ul class="nav nav-pills nav-pills">
									<li class="nav-item">
										<a class="hd-font fs-20 nav-link active" href="#pill1" data-toggle="tab">Map</a>
									</li>
								</ul>
								<div class="tab-content tab-space">
									<div class="tab-pane active" id="pill1">
										<script type='text/javascript'>
                                            var ws_wsid = 'g131252092ba64f7c8cfc5a3ec96b892b';
                                            var ws_address = 'lat=49.306293423261400/lng=-122.971010800000000';
                                            var ws_format = 'tall';
                                            var ws_width = '300';
                                            var ws_height = '350';
										</script>
										<style type='text/css'>#ws-walkscore-tile {
												position: relative;
												text-align: left
											}
											
											#ws-walkscore-tile * {
												float: none;
											}</style>
										<div id='ws-walkscore-tile'></div>
										<script type='text/javascript' src='http://www.walkscore.com/tile/show-walkscore-tile.php'></script>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- calculator  -->
					<div class="modal fade" id="calculator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- shareButtons  -->
					<div class="modal fade" id="shareButtons" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid Single-form bg-single-form my-5" style="  background: url(../images/bg-seller.jpg);">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class=" area-form ">
						<div class="title-form">
							<h3 class="clr-ff py-5 px-3 position-relative">Request More Info Or A Showing</h3>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="info-box">
									<div class="address-info">
										<figure class="text-left mb-0">
											<i class="fal fa-map-pin clr-ef te-sh fs-23"></i>
											<p class="clr-ef fs-17 mb-2">2397 Marine Dr.West Vancouver BC, V7V 1K9</p>
										</figure>
									</div>
									<div class="phone-info ">
										<figure class="text-left mb-0">
											<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
											<span class="clr-ef fs-17"> 604.500.1010</span>
										</figure>
									</div>
									<div class="phone-info ">
										<figure class="text-left mb-0">
											<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
											<span class="clr-ef fs-17">Office: 604.929.7969</span>
										</figure>
									</div>
									<div class="phone-info ">
										<figure class="text-left mb-0">
											<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
											<span class="clr-ef fs-17">Toll Free: 1.888.767.7421</span>
										</figure>
									</div>
									<div class="email-info ">
										<figure class="text-left m-0">
											<i class="fal fa-envelope clr-ef te-sh fs-23"></i>
											<span class="clr-ef fs-17">amir@hamzehali.com</span>
										</figure>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-contact">
									<form>
										<div class="row">
											<div class="col-12">
												<input type="text" class="form-control" placeholder="Your Name *">
												<input type="email" class="form-control" placeholder="Your Email *">
												<input type="tel" class="form-control" placeholder="Your phone *">
												<textarea class="form-control" name="name" rows="5" cols="40" placeholder="Your message" *></textarea>
												<div class="form-check pl-3 mt-2">
													<label class="form-check-label">
														<input class="form-check-input" type="checkbox" value="">
														I'd like to request a showing of this property
														<span class="form-check-sign">
																	<span class="check"></span>
															</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-12 text-center">
											<button type="button" class="btn btn-primary btn-round hd-font fs-12 mt-3">
												Send Message
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
@endsection
