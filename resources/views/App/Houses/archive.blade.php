@extends('App.Template.layout')
@section('content')
	<section class="container-fluid my-slider p-0">
		<div class="container p-0">
			<div class="row">
				<div class="col-12 slider-area">
					<div class="search-area position-relative">
						<div class="title-search clr-ff  w-100 position-relative m-auto text-center">
							<div class="title-area bx-sh px-5 py-2 d-inline-block">
								<h3 class="m-0 fs-40 ">QUICK MLS® SEARCH RESULTS</h3>
							</div>
						</div>
						<div class="search-form w-100 position-absolute text-center">
							<form>
								<div class="form-row">
									<div class="form-group col-md-5">
										<select id="inputState" class="form-control" onchange="document.querySelector('input[name=area]').setAttribute('value' , event.target.value)">
											<option value="" {{request('area')==NULL ? ' selected':''}}>Type or Choose City / Neighborhood</option>
											@foreach($filter['area'] as $area)
												<option value="{{$area}}"{{request('area')==$area ? ' selected':''}}>{{$area}}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group col-md-3">
										<select id="inputState" class="form-control" onchange="document.querySelector('input[name=propertyType]').setAttribute('value' , event.target.value)">
											<option value="" {{request('propertyType')==NULL ? ' selected':''}}>All Property Types</option>
											@foreach($filter['propertyType'] as $PType)
												<option value="{{$PType}}"{{request('propertyType')==$PType ? ' selected':''}}>{{$PType}}</option>
											@endforeach
										</select>
									</div>
									<button type="button" class="btn btn-primary submit-search btn-round hd-font m-0 fs-20" onclick="document.querySelector('button[type=submit].btn.btn-rose').click()">
										Find
									</button>
								</div>
							
							</form>
						</div>
					</div>
					<!-- slider -->
					<div class="owl-carousel owl-theme bx-sh d-none d-lg-block">
						<div class="item">
							<a href="#"><img src="/images/slider/slider01.jpg" alt=""></a>
						</div>
						<div class="item">
							<a href="#"><img src="/images/slider/slider02.jpg" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid archive-content py-3">
		<div class="container">
			<div class="row mt-3">
				<div class="col-lg-3">
					<div class="side-box p-3">
						<form action="" method="get">
							<div class="filter-price">
								<div class="row title-side mt-0">
									<div class="col-12 ">
										<div class="title-area text-center">
											<h3 class="clr-ff fs-23 mt-3 mb-3">
												Price
											</h3>
											<hr>
										</div>
									</div>
								</div>
								<div id="priceFilter" class="slider slider-primary mt-0"></div>
								<div class="controler-num text-center clr-ef">
									<span class="down-num" id="startPrice">300,000</span>
									<span>-</span>
									<span class="up-num" id="stopPrice">1,200,000</span>
								</div>
							</div>
							<div class="filter-feet">
								<div class="row title-side mt-4">
									<div class="col-12 ">
										<div class="title-area text-center">
											<h3 class="clr-ff fs-23 mt-3 mb-3">
												Square Feet
											</h3>
											<hr>
										</div>
									</div>
								</div>
								<div id="featFilter" class="slider slider-primary mt-0"></div>
								<div class="controler-num text-center clr-ef">
									<span class="down-num" id="startFeat">Any</span>
									<span>-</span>
									<span class="up-num" id="stopFeat">+200,000</span>
								</div>
							</div>
							<div class="filter-year">
								<div class="row title-side mt-4">
									<div class="col-12 ">
										<div class="title-area text-center">
											<h3 class="clr-ff fs-23 mt-3 mb-3">
												Year Built
											</h3>
											<hr>
										</div>
									</div>
								</div>
								<div id="yearBuiltFilter" class="slider slider-primary mt-0"></div>
								<div class="controler-num text-center clr-ef">
									<span class="down-num" id="startYearBuilt">Any</span>
									<span>-</span>
									<span class="up-num" id="stopYearBuilt">+2020</span>
								</div>
							</div>
							<div class="filter-bedroom">
								<div class="row title-side mt-3">
									<div class="col-12 ">
										<div class="title-area text-center">
											<h3 class="clr-ff fs-23 mt-3 mb-3">
												Bedrooms
											</h3>
											<hr>
										</div>
									</div>
								</div>
								<div id="bedroomsFilter" class="slider slider-primary mt-0"></div>
								<div class="controler-num text-center clr-ef">
									<span class="down-num" id="BedroomsNum">+2</span>
									<span>Bedrooms</span>
								</div>
							</div>
							<div class="filter-bathroom">
								<div class="row title-side mt-3">
									<div class="col-12 ">
										<div class="title-area text-center">
											<h3 class="clr-ff fs-23 mt-3 mb-3">
												Bathrooms
											</h3>
											<hr>
										</div>
									</div>
								</div>
								<div id="bathroomsFilter" class="slider slider-primary mt-0"></div>
								<div class="controler-num text-center clr-ef">
									<span class="down-num" id="BathroomsNum">+3</span>
									<span>Bathrooms</span>
								</div>
							</div>
							<input type="hidden" name="price[start]" value="{{request('price')['start']??''}}">
							<input type="hidden" name="price[stop]" value="{{request('price')['stop']??''}}">
							<input type="hidden" name="yearBuilt[start]" value="{{request('yearBuilt')['start']??''}}">
							<input type="hidden" name="yearBuilt[stop]" value="{{request('yearBuilt')['stop']??''}}">
							<input type="hidden" name="feat[start]" value="{{request('feat')['start']??''}}">
							<input type="hidden" name="feat[stop]" value="{{request('feat')['stop']??''}}">
							<input type="hidden" name="bedrooms" value="{{request('bedrooms')??''}}">
							<input type="hidden" name="bathrooms" value="{{request('bathrooms')??''}}">
							<input type="hidden" name="area" value="{{request('area')??''}}">
							<input type="hidden" name="propertyType" value="{{request('propertyType')??''}}">
							<button class="btn btn-rose" type="submit">SET</button>
						</form>
					</div>
				</div>
				<div class="col-lg-9">
					<div class="row">
						<!-- pagination -->
						<div class="col-lg-8 m-auto">
							{{$houses->links('App.Template.paginate')}}
						</div>
						<!-- order -->
						<div class="col-lg-4">
							<i class="fal fa-chevron-down order-icon clr-ef te-sh fs-23"></i>
							<select id="order" class="form-control">
								<option selected>Date Added: New</option>
								<option>Date Added: Newest to Oldest</option>
								<option>Date Added: Oldest to Newest</option>
								<option>Price: Highest to Lowest</option>
								<option>Price: Lowest to Highest</option>
								<option>Sq. Ft.: Largest to Smallest</option>
								<option>Sq. Ft.: Smallest to Largest</option>
								<option>Beds: Most to Least</option>
								<option>Beds: Least to Most</option>
								<option>Baths: Most to Least</option>
								<option>Baths: Least to Most</option>
								<option>Year Built: Newest to Oldest</option>
								<option>Year Built: Oldest to Newest</option>
							</select>
						</div>
					</div>
					<div class="row list-archive my-4">
						@foreach($houses as $house)
						<div class="col-md-6 col-lg-4 mt-4">
							<div class="box-list position-relative">
								<div class="img-list position-relative">
									<a href="{{route('App.house.show' , ['MLSNumber' => $house->MLSNumber , 'Address' => $house->address])}}">
										<img src="{{$house->img[0]}}" alt="">
									</a>
								</div>
{{--								<div class="tag-sold">--}}
{{--									<div class="area-sold position-relative;">--}}
{{--										<span class="clr-ff hd-font fs-22">sold</span>--}}
{{--									</div>--}}
{{--								</div>--}}
								<div class="title-list px-2">
									<a href="{{route('App.house.show' , ['MLSNumber' => $house->MLSNumber , 'Address' => $house->address])}}">
										<h3 class="fs-18 clr-ff m-0 pt-4 pb-2">{{$house->address}}</h3>
									</a>
								</div>
								<div class="price-list px-2">
									<span class="num-price clr-yellow">${{number_format($house->price ,0, "." , ",")}}</span>
									<span class="tag-house clr-ef"> Townhouse</span>
								</div>
								<div class="address-list px-2 clr-ef mt-3">
									<p>    
										{{$house->subArea}}, {{$house->area}}  </br>
										{{$house->bedrooms}} Beds, {{$house->bathrooms}} Baths, {{$house->lotSize}} Sq. Ft., Built
										{{$house->yearBuilt}}
									</p>
								</div>
								<div class="icon-list px-2 mt-3">
									<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-html="true" title="{{$house->propertyType??"Apartment/Condo"}}">
										<i class="fal fa-home clr-ef te-sh fs-23"></i>
									</button>
									<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-html="true" title="">
										<i class="fal fa-users clr-ef te-sh fs-23"></i>
									</button>
									<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-html="true" title="">
										<i class="fal fa-star clr-ef te-sh fs-23"></i>
									</button>
									<button type="button" class="btn btn-secondary float-right mx-2" data-toggle="tooltip" data-html="true" title="">
										<i class="fal fa-heart clr-ef te-sh fs-23"></i>
									</button>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<div class="row">
						<!-- pagination -->
						<div class="col-lg-12 m-auto">
							{{$houses->links('App.Template.paginate')}}
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</section>
@endsection
@section('PageScripts')
	<script src="/js/wNumb.min.js"></script>
	<script>
        $(document).ready(function () {
            // filter of archive page
            let priceFilter = document.getElementById('priceFilter');
            noUiSlider.create(priceFilter, {
                start: [{{request('price')['start']??$filter['price']['min']-10000}}, {{request('price')['stop']??$filter['price']['max']}}],
                connect: true,
                format: wNumb({
                    decimals: 0
                }),
                range: {
                    min: [{{$filter['price']['min']-10000}}, 1000],
                    max: {{$filter['price']['max']}}
                }
            });
            priceFilter.noUiSlider.on('update', function (values) {
                document.querySelector('input[name="price[start]"]').setAttribute('value', values[0])
                document.querySelector('input[name="price[stop]"]').setAttribute('value', values[1])
                document.getElementById('startPrice').innerText = values[0]
                document.getElementById('stopPrice').innerText = values[1]
            })
            let featFilter = document.getElementById('featFilter');
            noUiSlider.create(featFilter, {
                start: [{{request('feat')['start']??$filter['feat']['min']-500}}, {{request('feat')['stop']??$filter['feat']['max']}}],
                connect: true,
                format: wNumb({
                    decimals: 0
                }),
                range: {
                    min: [{{$filter['feat']['min']-500}}, 10],
                    max: {{$filter['feat']['max']}}
                }
            });
            featFilter.noUiSlider.on('update', function (values) {
                document.querySelector('input[name="feat[start]"]').setAttribute('value', values[0])
                document.querySelector('input[name="feat[stop]"]').setAttribute('value', values[1])
                document.getElementById('startFeat').innerText = values[0]
                document.getElementById('stopFeat').innerText = values[1]
            })
            let yearBuiltFilter = document.getElementById('yearBuiltFilter');
            noUiSlider.create(yearBuiltFilter, {
                start: [{{request('yearBuilt')['start']??$filter['yearBuilt']['min']-50}}, {{request('yearBuilt')['stop']??$filter['yearBuilt']['max']}}],
                connect: true,
                format: wNumb({
                    decimals: 0
                }),
                range: {
                    min: [{{$filter['yearBuilt']['min']-50}}, 1],
                    max: {{$filter['yearBuilt']['max']}}
                }
            });
            yearBuiltFilter.noUiSlider.on('update', function (values) {
                document.querySelector('input[name="yearBuilt[start]"]').setAttribute('value', values[0])
                document.querySelector('input[name="yearBuilt[stop]"]').setAttribute('value', values[1])
                document.getElementById('startYearBuilt').innerText = values[0]
                document.getElementById('stopYearBuilt').innerText = values[1]
            })
            let bedroomsFilter = document.getElementById('bedroomsFilter');
            noUiSlider.create(bedroomsFilter, {
                start: {{request('bedrooms')??0}},
                format: wNumb({
                    decimals: 0
                }),
                range: {
                    min: [0, 1],
                    max: 10
                }
            });
            bedroomsFilter.noUiSlider.on('update', function (values) {
                document.querySelector('input[name="bedrooms"').setAttribute('value', values[0])
                document.getElementById('BedroomsNum').innerText = ((values[0] == 0) ? "Any" : "+" + values[0])
            })
            let bathroomsFilter = document.getElementById('bathroomsFilter');
            noUiSlider.create(bathroomsFilter, {
                start: {{request('bathrooms')??0}},
	            format: wNumb({
                    decimals: 0
                }),
                range: {
                    min: [0, 1],
                    max: 10
                }
            });
            bathroomsFilter.noUiSlider.on('update', function (values) {
                document.querySelector('input[name="bathrooms"]').setAttribute('value', values[0])
                document.getElementById('BathroomsNum').innerText = ((values[0] == 0) ? "Any" : "+" + values[0])
            })
        });
	</script>
@endsection
