@extends('App.Template.layout')
@section('PageStyles')
	<style>
		form input, textarea {
			color: white !important;
		}
	</style>
@endsection
@section('content')
	<section class="container-fluid archive-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 bg-single" style="background-image: url({{$data['headerImage']}});">
					<div class="info-archive ">
						<div class="title-single position-relative">
							<h1 class="clr-ff p-2 fs-40">Buy</h1>
						</div>
						<div class="bread-crumb position-relative text-center">
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb clr-ff">
									<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
									<li class="breadcrumb-item"><a href="javascript:;">Buy</a></li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid about-me py-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 m-auto">
					<div class="des-aboutme">
						<p class="clr-ef fs-19">
							{!! $data['body'] !!}
						</p>
					</div>
				</div>
			
			</div>
		</div>
	</section>
	<section class="container-fluid Single-form my-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 bg-single-form">
					<div class="title-form">
						<h3 class="clr-ff py-5 px-3 position-relative">Request Form Buyer</h3>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="info-box">
								<div class="address-info">
									<figure class="text-left mb-0">
										<i class="fal fa-map-pin clr-ef te-sh fs-23"></i>
										<p class="clr-ef fs-17 mb-2">2397 Marine Dr.West Vancouver BC, V7V 1K9</p>
									</figure>
								</div>
								<div class="phone-info ">
									<figure class="text-left mb-0">
										<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
										<span class="clr-ef fs-17"> 604.500.1010</span>
									</figure>
								</div>
								<div class="phone-info ">
									<figure class="text-left mb-0">
										<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
										<span class="clr-ef fs-17">Office: 604.929.7969</span>
									</figure>
								</div>
								<div class="phone-info ">
									<figure class="text-left mb-0">
										<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
										<span class="clr-ef fs-17">Toll Free: 1.888.767.7421</span>
									</figure>
								</div>
								<div class="email-info ">
									<figure class="text-left m-0">
										<i class="fal fa-envelope clr-ef te-sh fs-23"></i>
										<span class="clr-ef fs-17">amir@hamzehali.com</span>
									</figure>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-contact">
								<form action="{{route('App.form.buy')}}" method="post" onsubmit="CheckAuth(event)">
									@csrf
									<div class="row">
										<div class="col-12">
											<input type="text" name="name" class="form-control" placeholder="Your Name *" required value="{{auth()->user()->name??NULL}}">
											<input type="email" name="email" class="form-control" placeholder="Your Email *" required value="{{auth()->user()->email??NULL}}">
											<input type="tel" name="phone" class="form-control" placeholder="Your phone *" required value="{{auth()->user()->phone??NULL}}">
											<input type="text" name="propertyType" class="form-control" placeholder="propertyType *" required>
											<input type="text" name="area" class="form-control" placeholder="Ideal Location *" required>
											<input type="number" min="0" name="bedrooms" class="form-control" placeholder="bedrooms *" required>
											<input type="number" min="0" name="bathrooms" class="form-control" placeholder="bathrooms *" required>
											<input type="text" name="priceRange" class="form-control" placeholder="priceRange *" required>
											<input type="number" min="0" name="sqFt" class="form-control" placeholder="Desired Size (approx. square feet) *" required>
											<input type="text" name="when" class="form-control" placeholder="When are you planning to buy? *" required>
											<textarea class="form-control" name="message" rows="5" cols="40" placeholder="Additional Comments *" required></textarea>
											<div class="form-check pl-3 mt-2">
												<label class="form-check-label">
													<input class="form-check-input" type="checkbox" value="">
													I'd like to request a showing of this property
													<span class="form-check-sign">
									              <span class="check"></span>
									          </span>
												</label>
											</div>
										</div>
									</div>
									<div class="col-12 text-center">
										<button type="submit" class="btn btn-primary btn-round hd-font fs-12 mt-3">Send
											Message
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
		</div>
	</section>

@endsection
@section('PageScripts')
	<script>
        $(document).ready(function () {
            CheckAuth()
			@if(session('requestBuy'))
            CustomAlert('fa-check', 'success', 'SUCCESSFULLY', '{{session('requestBuy')}}')
			@endif
        })
	</script>
@endsection
