@extends('App.Template.layout')
@section('content')
	<section class="container-fluid profile-page mt-5">
		<div class="container">
			<div class="row row-profile">
				<div class="col-lg-9 col-md-8">
					<div class="profile-box position-related">
						<div class="title-section-modal py-4" style="background-position: 10px center;">
							<div class="col-12">
								<div class="title-area text-left">
									<h5 class="clr-ef fs-30 m-3">
										Requests
									</h5>
								</div>
							</div>
						</div>
						<div class="rq-box row pt-0 px-3 pb-3 ">
							<div class="col-lg-6 mt-4">
								<div class="content-RQbox bx-sh clr-ef">
									<div class="box-name px-2">
										<h3 class="m-0 fs-20">Ross Galler</h3>
									</div>
									<div class="box-mail fs-12 mb-3 px-2">
										<span>mailExamplle@gmail.com</span>
									</div>
									<div class="box-des-RQ px-2 fs-14">
										<p class=" fs-14 clr-8d">Lorem ipsum dolor sit amet, consectetuer adipiscing
											elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
											aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
											tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
											consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit
											esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero
											eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril
											delenit augue duis dolore te feugait nulla facilisi.</p>
									</div>
									<div class="content_btn_RQ px-2 mt-2">
										<a href="" class="btn btn-warning btn-round hd-font fs-14" data-toggle="modal" data-target="#edit-request-form"><i class="fal fa-edit clr-ef te-sh fs-14 "></i>
											Edit Form</a>
										<a href="" class="btn btn-secondary btn-round hd-font fs-14"><i class="fal fa-trash clr-ef te-sh fs-14 "></i>
											Delete</a>
										<!-- modal edit-request-form -->
										<div class="modal fade" id="edit-request-form" tabindex="-1" role="dialog" aria-labelledby="editRQlabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content bg-2c">
													<div class="modal-header row">
														<div class="col-lg-12">
															<div class="title-section-modal py-4">
																<div class="col-12 ">
																	<div class="title-area text-left">
																		<h5 class="clr-ef fs-20" id="loginlabel">
																			Edit My Request
																		</h5>
																	</div>
																</div>
															</div>
														</div>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<div class="form-contact">
															<form>
																<div class="row">
																	<div class="col-12">
																		<input type="text" class="form-control" placeholder="Your Name *">
																		<input type="email" class="form-control" placeholder="Your Email *">
																		<input type="tel" class="form-control" placeholder="Your phone *">
																		<textarea class="form-control" name="name" rows="5" cols="40" placeholder="Your message" *></textarea>
																		<div class="form-check pl-3 mt-2">
																			<label class="form-check-label">
																				<input class="form-check-input" type="checkbox" value="">
																				I'd like to request a showing of this
																				property
																				<span class="form-check-sign">
																						<span class="check"></span>
																				</span>
																			</label>
																		</div>
																	</div>
																</div>
																<div class="col-12 text-center">
																	<button type="button" class="btn btn-primary btn-round hd-font fs-12 mt-3">
																		Edit Request
																	</button>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="status-box position-absolute">
										<span class="badge badge-info fs-12 te-sh">info</span>
										<span class="badge badge-success fs-12 te-sh">success</span>
										<span class="badge badge-danger fs-12 te-sh">Danger</span>
									</div>
									<hr>
									<div class="date_send text-center">
										<span>12 may 2020</span>
									</div>
								</div>
							</div>
						</div>
						<!--paginate-->
					</div>
				</div>
				<div class="col-lg-3 col-md-4">
					<div class="profile-box p-info-box bg-ff bx-sh">
						<div class="profile-image px-3 pt-5 pb-2">
							<div class="p-img">
								<img src="{{$user->avatar??"images/avatar.jpg"}}" alt="" id="userAvatar" style="width: 100px;height: 100px; background-size: cover">
								<a onclick="ChangeAvatar()">
									<button class="bg-ff p-1">
										<i class="fal fa-edit"></i>
									</button>
								</a>
							</div>
							<p class="fs-18 text-center hd-font clr-ff">
								{{$user->name}}
							</p>
						</div>
						<div class="profile-pe py-3">
							<div class="pass-change pe-box">
								<a data-toggle="modal" data-target="#ChangePass">
									<i class="fal fa-lock"></i>
									<p class="">
										Change Pass
									</p>
								</a>
								<!-- ChangePass modal -->
								<div class="modal fade" id="ChangePass" tabindex="-1" role="dialog" aria-labelledby="editprofile_label" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content bg-2c">
											<div class="modal-header row">
												<div class="col-lg-12">
													<div class="title-section-modal py-4">
														<div class="col-12 ">
															<div class="title-area text-left">
																<h5 class="clr-ef fs-20" id="editprofile_label">
																	Change Password
																</h5>
															</div>
														</div>
													</div>
												</div>
												<button type="submit" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="form-contact">
													<form onsubmit="ajaxFrom(event , '{{route('App.profile.changePassword')}}')">
														<div class="row">
															<div class="col-12">
																<input type="password" name="current-password" class="form-control" placeholder="current password *">
																<input type="password" name="password" class="form-control" placeholder="New password *">
																<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm New password *">
															</div>
														</div>
														<div class="col-12 text-center">
															<button type="submit" class="btn btn-primary btn-round hd-font fs-12 mt-3">
																Change Password
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="exit-profile pe-box">
								<a href="{{route('app-auth.logout')}}" onclick="logout(event , '{{route('app-auth.logout')}}')">
									<i class="fal fa-sign-out-alt"></i>
									<p>
										Sign Out
									</p>
								</a>
							</div>
						</div>
					</div>
					<div class="btn-profile">
						<a href="" class="btn btn-info btn-round hd-font fs-14 d-block mt-4 " data-toggle="modal" data-target="#RQ_form_New"><i class="fal fa-file clr-ef te-sh fs-20 "></i>
							Request Form</a>
						<!-- RQ_form_New modal -->
						<div class="modal fade" id="RQ_form_New" tabindex="-1" role="dialog" aria-labelledby="RQ_form_label" aria-hidden="true">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content bg-2c">
									<div class="modal-header row">
										<div class="col-lg-12">
											<div class="title-section-modal py-4">
												<div class="col-12 ">
													<div class="title-area text-left">
														<h5 class="clr-ef fs-20" id="RQ_form_label">
															Add New Request
														</h5>
													</div>
												</div>
											</div>
										</div>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="form-contact">
											<form>
												<div class="row">
													<div class="col-12">
														<textarea class="form-control" name="name" rows="5" cols="50" placeholder="Your message" *></textarea>
														<div class="form-check pl-3 mt-2">
															<label class="form-check-label">
																<input class="form-check-input" type="checkbox" value="">
																I'd l
																ike to request a showing of this property
																<span class="form-check-sign">
																		<span class="check"></span>
																</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-12 text-center">
													<button type="button" class="btn btn-primary btn-round hd-font fs-12 mt-3">
														Send Request
													</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="" class="btn btn-primary btn-round hd-font fs-14 d-block mt-3 " data-toggle="modal" data-target="#editprofile"><i class="fal fa-user-edit clr-ef te-sh fs-20 "></i>
							Edit Profile</a>
						<!-- editprofile modal -->
						<div class="modal fade" id="editprofile" tabindex="-1" role="dialog" aria-labelledby="editprofile_label" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content bg-2c">
									<div class="modal-header row">
										<div class="col-lg-12">
											<div class="title-section-modal py-4">
												<div class="col-12 ">
													<div class="title-area text-left">
														<h5 class="clr-ef fs-20" id="editprofile_label">
															Add New Request
														</h5>
													</div>
												</div>
											</div>
										</div>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="form-contact">
											<form onsubmit="ajaxFrom(event , '{{route('App.profile.update')}}')">
												<div class="row">
													<div class="col-12">
														<input type="text" name="name" class="form-control" placeholder="Your Name *" value="{{$user->name}}">
														<input type="email" name="email" class="form-control" placeholder="Your Email *" value="{{$user->email}}">
														<input type="tel" name="phone" class="form-control" placeholder="Your phone *" value="{{$user->phone}}">
														<div class="form-check pl-3 mt-2">
															<label class="form-check-label">
																<input class="form-check-input" type="checkbox" value="">
																I'd like to request a showing of this property
																<span class="form-check-sign">
																		<span class="check"></span>
																</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-12 text-center">
													<button type="submit" class="btn btn-primary btn-round hd-font fs-12 mt-3">
														Edit Request
													</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('PageScripts')
	<script>
        function ChangeAvatar() {
            let inputfile = document.createElement('input')
            inputfile.type = 'file'
            inputfile.name = 'avatar'
            inputfile.accept = 'image/jpeg,image/png'
            inputfile.onchange = function () {
                let data = new FormData();
                data.append('avatar', inputfile.files[0])
                let xhr = new XMLHttpRequest()
                xhr.open('POST', '{{route('App.profile.change-avatar')}}')
                xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken.get())
                xhr.responseType = 'json'
                xhr.onload = function (res) {
                    res = res.target.response
                    if (xhr.status == 200) {
                        if (res.success) {
                            CustomAlert('fa-check', 'success', 'SUCCESSFULLY:', res.message)
                            document.getElementById('userAvatar').setAttribute('src', res.avatar.link)
                            csrfToken.set(res['csrf-token'])
                        } else {
                            CustomAlert('fa-danger', 'warning', 'WARNING:', res.message)
                            csrfToken.set(res['csrf-token'])
                        }
                    }
                }
                xhr.send(data)
            }
            inputfile.click()
        }

        function ajaxFrom(e, url) {
            e.preventDefault()
            let data = new FormData(e.target)
            let xhr = new XMLHttpRequest()
	        xhr.open('post', url)
            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken.get())
            xhr.responseType = 'json'
            xhr.onload = function (res) {
                res = res.target.response
                if (res.success) {
                    CustomAlert('fa-check', 'success', 'SUCCESSFULLY:', res.message)
                } else {
                    CustomAlert('fa-danger', 'warning', 'WARNING:', res.message)
                }
            }
            xhr.send(data)
        }
	</script>
@endsection