<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	@include('App.template.styles')
	<title>Amir Hamzehali</title>
</head>
<body>
<!-- Start Header -->
<section class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-6 m-auto">
				<img src="images/404.svg" alt="">
				<a class="btn btn-secondary d-block btn-round hd-font fs-20 my-3 px-5 py-2 mx-auto">Go to Home</a>
			</div>
		</div>
	</div>
</section>
<!-- End Body-home -->

<!-- Start JS -->
@include('App.template.scripts')
<!-- End JS -->
</body>
</html>
