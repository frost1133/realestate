@extends('App.Template.layout')
@section('PageStyles')
    <style>
        .media-body p,h5{
            color: white;
        }
        .reply_answer{
            margin-top: 20px;
        }
    </style>

@endsection
@section('content')
	<section class="container-fluid Single-feature mt-5 mt-md-2 ">
		<div class="container">
			<div class="row">

				<div class="col-lg-12 bg-single" style="background-image: url({{$article->featuredImage}});">
					<div class="contentbox-info d-inline-block">
						<div class="title-single position-relative">
							<h1 class="clr-ff fs-40 p-2 ">{{$article->title}}</h1>
						</div>
						<div class="bread-crumb position-relative m-0">
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb clr-ff">
									<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
									<li class="breadcrumb-item"><a href="javascript:;">blog</a></li>
                                    @foreach($categories as $category)
									<li class="breadcrumb-item active" aria-current="page">{{$category->name}}</li>
                                    @endforeach
								</ol>
							</nav>
						</div>
						<div class="publish-row clr-ff px-2 position-relative">
							<span class="date-publish">{{$article->created_at}}</span>
							<span>Posted By</span>
							<span class="author-box">{{$article->admin->name}}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid Single-content mt-3">
		<div class="container p-md-0">
			<div class="row">
				<div class="col-lg-9 order-2 order-md-1">
					<div class="des-box clr-ef mt-3">
							{!! $article->body!!}
					</div>
                    @include('App.Blog.index',['comments'=>$comments,'user'=>$user,'article'=>$article])
				</div>

				<div class="col-lg-3 order-1 order-md-2">
					<div class="row">
						<div class="col-12">
							<div class="side-box p-3 mt-4">
								<div class="side-category">
									<div class="row title-side mt-0">
										<div class="col-12 ">
											<div class="title-area text-center">
												<h3 class="clr-ff fs-23 mt-3 mb-3">
													Latest Blog
												</h3>
												<hr>
											</div>
										</div>
									</div>
									<div class="row Latest-Blog">
										<div class="col-12">

                                            @foreach($articles as $article)

                                                <a href="{{route('App.blog.show' , $article)}}">
                                                    <figure>
                                                        <img class="w-100" src="{{$article->featuredImage}}" alt="">
                                                        <h3 class="mt-1 pl-2 clr-ef fs-14 m-0 bd-font">{{$article->title}}</h3>
                                                    </figure>
                                                </a>

                                                <hr>
                                            @endforeach


										</div>

									</div>

								</div>

							</div>
						</div>

					</div>
					<!-- calculator  -->
					<div class="modal fade" id="calculator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- shareButtons  -->
					<div class="modal fade" id="shareButtons" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('PageScripts')
    @if(session()->has('success'))
    <script>
        $(document).ready(function(){
            CustomAlert('a','success','Success','{{ session('success') }}')
        })
    </script>
@endif
    <script>
        function replyComment(e){
            let target=e.target;
            let replyForm=$(target.nextElementSibling);
            if( replyForm.hasClass('reply-form')){
                replyForm.slideUp();
                replyForm.removeClass('reply-form');
                $('.reply-form').slideUp();
                $('.reply-form').removeClass('reply-form');
            }
            else{
                replyForm.slideDown();
                $('.reply-form').slideUp();
                $('.reply-form').removeClass('reply-form');
                replyForm.addClass('reply-form');
            }

        }
    </script>
@endsection
