@extends('App.Template.layout')
@section('content')
	<section class="container-fluid archive-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 bg-single" style="background-image: url(/images/acc/blog01.jpg);">
					<div class="info-archive ">
						<div class="title-single position-relative">
							<h1 class="clr-ff p-2 fs-40">Title Archive </h1>
						</div>
						<div class="bread-crumb position-relative text-center">
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb clr-ff">
									<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
									<li class="breadcrumb-item"><a href="javascript:;">blog</a></li>
									<li class="breadcrumb-item active" aria-current="page">Here is Your Title</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid Single-content mt-3">
		<div class="container p-md-0">
			<div class="row">
				<div class="col-lg-9 order-2 order-md-1">
					<div class="row mt-3">
						@foreach($articles as $article)
							<div class="col-lg-4 mt-5">
								<div class="blog-box bx-sh">
									<a href="{{route('App.blog.show' , $article)}}">
										<figure>
											<div class="blog-img">
												<img src="{{$article->featuredImage}}" alt="">
											</div>
											<div class="blog-title ">
												<h3 class="fs-20">{{$article->title}}</h3>
											</div>
											<div class="blog-des clr-ef">
												<p class="fs-12">{!! Str::words($article->body,30,'...') !!}</p>
											</div>
										</figure>
									</a>
								</div>
							</div>
						@endforeach
					</div>
					<!-- pagination -->
					
					{{$articles->links('App.Template.paginate')}}
				
				
				</div>
				<div class="col-lg-3 order-1 order-md-2">
					<div class="row">
						<div class="col-12">
							<div class="side-box p-3 mt-4">
								<div class="side-category">
									<div class="row title-side mt-0">
										<div class="col-12 ">
											<div class="title-area text-center">
												<h3 class="clr-ff fs-23 mt-3 mb-3">
													Latest Blog
												</h3>
												<hr>
											</div>
										</div>
									</div>
									<div class="row Latest-Blog">
										@foreach($latestArticles as $article)
											
											<div class="col-12">
												<a href="{{route('App.blog.show' , $article)}}">
													<figure>
														<img class="w-100" src="{{$article->featuredImage}}" alt="">
														<h3 class="mt-1 pl-2 clr-ef fs-14 m-0 bd-font">{{$article->title}}</h3>
													</figure>
												</a>
												<hr>
											
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- calculator  -->
					<div class="modal fade" id="calculator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- shareButtons  -->
					<div class="modal fade" id="shareButtons" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									...
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
