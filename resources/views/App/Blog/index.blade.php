<div class="col-lg-12 order-1 order-md-2">
    <div class="row">
        <div class="col-12 p-0">
            <div class="side-box p-3 mt-4">
                <div class="side-category">
                    <div class="row title-side mt-0 " style="background-position: center top">
                        <div class="col-12">
                            <div class="title-area text-center">
                                <h3 class="clr-ff fs-23 mt-3 mb-4 ">
                                    Comments
                                </h3>
                                <hr>
                            </div>
                            @if ($user)
                                <div class="modal-body">
                                    <form action="{{route('App.blog.store',$article)}}" method="post" id="registerForm">
                                        @csrf
                                        <input type="text" name="title" class="form-control"
                                               placeholder="Enter title *">
                                        <input type="text" name="body" class="form-control"
                                               placeholder="Enter comment *">
                                        <button type="submit" class="btn btn-warning btn-round hd-font fs-14 mt-3">
                                            Send
                                        </button>
                                    </form>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="row Latest-Blog">
                        <div class="col-12">

                          @include('App.Blog.comments',['comments'=>$comments,'user'=>$user,'article'=>$article,'test'=>null])
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
