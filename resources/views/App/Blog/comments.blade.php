
@foreach ($comments as $comment)
    <div class="media">

        <img src="https://lorempixel.com/640/480/?26173" class="align-self-start mr-3"
             alt="..." style="width: 50px">

        <div class="media-body">
            <h5 class="mt-0">{{$comment->title}}</h5>
            <p>{{$comment->body}}</p>
            <button type="button" class="btn-sm btn btn-primary" onclick="replyComment(event)">
                <i class="fa-reply fal"></i>
            </button>
            @if ($user)
                <div class="modal-body" style="display: none">
                    <form method="post" action="{{route('App.blog.store',$article)}}">
                        @csrf
                        <input type="text" class="col-3 form-control" name="title" placeholder="Enter title">
                        <input type="text" class="col-6 form-control" name="body" placeholder="answer this comment">
                        <input type="hidden" name="parentComment" value="{{$comment->id}}">
                        <button type="submit" class="btn btn-primary" style="text-align: center">
                            send
                        </button>
                    </form>
                </div>
            @endif
            @if ($comment->childes->isNotEmpty())
                <div class="reply_answer"  >
                    @include('App.Blog.comments',['comments'=>$comment->childes()->whereNotNull('confirmComment')->get(),'user'=>$user,'article'=>$article,'test'=>$comment->id])
                </div>
            @endif

        </div>

    </div>
@endforeach
