@extends('App.Template.layout')

@section('content')
    <section class="container-fluid archive-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 bg-single" style="background-image: url({{$data['headerImage']}});">
                    <div class="info-archive ">
                        <div class="title-single position-relative">
                            <h1 class="clr-ff p-2 fs-40">Contact</h1>
                        </div>
                        <div class="bread-crumb position-relative text-center">
                            <nav aria-label="breadcrumb" role="navigation">
                                <ol class="breadcrumb clr-ff">
                                    <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:;">Contact</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid about-me py-3">
        <div class="container">
           
            
                    <div class="row">
                        
                        <div class="col-lg-6">
                            <div class="title-single position-relative">
                                <h1 class="clr-ff p-2 fs-40">Contact us</h1>
                            </div>
                        <p class="clr-ef fs-19">
                            {!! $data['body'] !!}
                        </p>
            
                        </div>
                </div>
            
            </div>
    </section>

@endsection
