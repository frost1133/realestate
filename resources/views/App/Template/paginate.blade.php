@if($paginator->hasPages())
	<div class="row mt-5">
		<div class="col-12">
			<nav aria-label="archive-pagination">
				<ul class="pagination justify-content-center">
					@if($paginator->onFirstPage())
						<li class="page-item disabled">
							<span class="page-link">Previous</span>
						</li>
					@else
						<li class="page-item">
							<a class="page-link" href="{{$paginator->previousPageUrl()}}">Previous</a>
						</li>
					@endif
					@foreach($elements as $element)
						@if(is_string($element))
							<li class="page-item"><a class="page-link" href="javascript:;">{{$element}}</a></li>
						@endif
						@if(is_array($element))
							@foreach($element as $page => $url)
								@if($page == $paginator->currentPage())
									<li class="page-item active">
										<span class="page-link">{{$page}}<span class="sr-only">(current)</span></span>
									</li>
								@else
									<li class="page-item"><a class="page-link" href="{{$url}}">{{$page}}</a></li>
								@endif
							@endforeach
						@endif
					@endforeach
					@if ($paginator->hasMorePages())
						<li class="page-item">
							<a class="page-link" href="{{ $paginator->nextPageUrl() }}">Next</a>
						</li>
					@else
						<li class="page-item disabled">
							<span class="page-link">Next</span>
						</li>
					@endif
				</ul>
			</nav>
		</div>
	</div>
@endif