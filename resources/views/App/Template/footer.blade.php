<footer class="container-fluid footer mt-3 position-relative">
	<div class="container">
		<div class="row">
			<div class="box-plan position-absolute w-25">
				<img class="plan" src="images/plan.svg" alt="">
			</div>
			<div class="col-lg-8">
				<div class="row title-section footer-title py-4">
					<div class="col-12 ">
						<div class="title-area text-center">
							<h3 class="clr-ff fs-23">
								Get In Touch
							</h3>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12">
						<form id="myForm" action="{{route('App.form.store')}}" method="post">
							<div class="row">
								<div class="col-6">
									<input type="text" class="form-control" placeholder="Your Name *" id="name">
									<input type="email" class="form-control" placeholder="Your Email *" id="email">
									<input type="tel" class="form-control" placeholder="Your phone *" id="tel">
								</div>
								<div class="col-6">
									<textarea class="form-control" id="mes" name="name" rows="5" cols="40" placeholder="Your message" *></textarea>
								</div>
							</div>
							<button type="submit"  id="ajaxSubmit" class="btn btn-warning btn-round hd-font fs-12 mt-3">Send Message
							</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="row title-section footer-title py-4">
					<div class="col-12 ">
						<div class="title-area text-center">
							<h3 class="clr-ff fs-23">
								OFFICE INFO
							</h3>
						</div>
					</div>
				</div>
				<hr>
				<div class="info-box">
					<div class="address-info">
						<figure class="text-left mb-0">
							<i class="fal fa-map-pin clr-ef te-sh fs-23"></i>
							<p class="clr-ef fs-17">2397 Marine Dr.West Vancouver BC, V7V 1K9</p>
						</figure>
					</div>
					<div class="phone-info mt-4">
						<figure class="text-left mb-0">
							<i class="fal fa-mobile clr-ef te-sh fs-23"></i>
							<span class="clr-ef fs-17"> 604.500.1010</span>
						</figure>
					</div>
					<div class="email-info ">
						<figure class="text-left m-0">
							<i class="fal fa-envelope clr-ef te-sh fs-23"></i>
							<span class="clr-ef fs-17">amir@hamzehali.com</span>
						</figure>
					</div>


				</div>
			</div>
		</div>
	</div>
</footer>
<section class="container-fluid copyright py-3 text-center clr-ff">
	<p class="m-0">Real Estate Website by RealtyNinja Privacy Policy</p>
</section>
