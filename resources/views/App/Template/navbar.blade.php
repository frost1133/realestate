
<!-- desktop menu -->
<div class="menu-area d-none d-md-block">
	<ul class="menu text-center clr-ff my-4">
		<li class="active">
			<a href="{{route('App.Home')}}">Home</a>
		</li>
		<li id="have-submenu" class="have-submenu">
			<a href="{{route('App.form.showBuyingForm')}}">Buy</a>
		</li>
		<li>
			<a href="{{route('App.form.showSellingForm')}}">Sell</a>
		</li>
		<li>
			<a href="{{route('App.house.index')}}">Listings</a>
		</li>
		<li>
			<a href="{{route('App.blog.archive')}}">Blog</a>
		</li>
		<li>
			<a href="{{route('App.about')}}">About</a>
		</li>
		<li>
			<a href="{{route('App.contact')}}">Contact Us</a>
		</li>
	</ul>
</div>
<!-- mobile-menu -->
<div class="off-con text-right d-block d-md-none">
	<div class="mobile-menu">
		<div class="m-menu-btn">
			<a id="m-menu-btn">
				<i class="fal fa-bars"></i>
			</a>
		</div>
		<section class="m-menu" id="m-menu">
			<div class="m-menu-box" id="m-menu-box">
				<a id="m-menu-close" class="m-menu-close">
					<i class="fal fa-times clr-red"></i>
				</a>
				<div class="logo-area text-center">
					<a href="#">
						<img src="images/logo.png" alt="">
					</a>
				</div>
				<ul class="hd-font">
					<li>
						<a href="index.html">
							home
						</a>
					</li>
					<li>
						<a class="have-submenu-mobile" data-toggle="collapse" href="#ul-submenu-mobile" role="button" aria-expanded="false" aria-controls="collapseExample">
							buyer
							<i class="fal fa-chevron-down clr-ef ml-1"></i>
						</a>
						<div class="collapse" id="ul-submenu-mobile">
							<div class=" card-body m-0 p-0">
								<ul class="ul-submenu-mob mt-0">
									<li>
										<a href="#">
											submenu01
										</a>
									</li>
									<li>
										<a href="#">
											submenu01
										</a>
									</li>
									<li>
										<a href="#">
											submenu01
										</a>
									</li>
									<li>
										<a href="#">
											submenu01
										</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#">
							seller
						</a>
					</li>
					<li>
						<a href="#">
							Contact us
						</a>
					</li>
					<li>
						<a href="#">about amir</a>
					</li>
				</ul>
			</div>
			<div class="m-overly" id="m-overly"></div>
		</section>
	</div>
</div>
