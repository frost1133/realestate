<header class="container-fluid header-area">
	<div class="container">
		<div class="row logo-area">
			<div class="d-none d-md-block col-md-3 col-lg-4"></div>
			<div class="col-12 col-md-6 col-lg-4 logo">
				<div class="logo-area text-center">
					<a href="#">
						<img src="/images/logo.png" alt="">
					</a>
				</div>
			</div>
			<div class="d-none d-md-block col-md-3 col-lg-4"></div>
		</div>
		<div class="row nav-menu">
			<div class="col-lg-3 text-center text-lg-left m-auto">
				<div class="phone-num d-inline-block mt-3">
					<figure>
						<i class="fal fa-phone clr-ef te-sh fs-23"></i>
						<span class="clr-ff hd-font fs-16">604.500.1010</span>
					</figure>
				</div>
				<div class="account-btn d-inline-block ml-4 mt-3">
					@guest('web')
						<figure data-toggle="modal" data-target="#loginform">
							<i class="fal fa-user clr-ef te-sh fs-23"></i>
							<!-- Button trigger modal -->
							<span class="clr-ff hd-font fs-16">Account</span>
						</figure>
					@else()
						<a href="{{route('App.profile')}}">
							<i class="fal fa-user-check clr-ef te-sh fs-23"></i>
							<!-- Button trigger modal -->
							<span class="clr-ff hd-font fs-16">Profile</span>
						</a>
						<a href="{{route('app-auth.logout')}}" onclick="logout(event , '{{route('app-auth.logout')}}')">
							<i class="fal fa-door-open clr-ef te-sh fs-23"></i>
							<!-- Button trigger modal -->
							<span class="clr-ff hd-font fs-16">Logout</span>
						</a>
				@endguest()
				<!-- Modal -->
					<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="loginlabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content bg-2c">
								<div class="modal-header row">
									<div class="col-lg-6">
										<div class="title-section-modal py-4">
											<div class="col-12 ">
												<div class="title-area text-left">
													<h5 class="clr-ef fs-20" id="loginlabel">
														Create A Free Account
													</h5>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="title-section-modal py-4">
											<div class="col-12 ">
												<div class="title-area text-left">
													<h5 class="clr-ef fs-20">
														Login To Your Account
													</h5>
												</div>
											</div>
										</div>
									</div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body row">
									<div class="col-lg-6">
										<form action="{{route('app-auth.register')}}" id="registerForm">
											<input type="text" name="name" class="form-control" placeholder="Your Name *">
											<input type="email" name="email" class="form-control" placeholder="Your Email *">
											<input type="tel" name="phone" class="form-control" placeholder="Your phone *">
											<input type="password" name="password" class="form-control" placeholder="Create Your Password *">
											<input type="password" name="password_confirmation" class="form-control" placeholder="Repeat Your Password *">
											<div class="form-check mt-4">
												<label class="form-check-label">
													<input class="form-check-input" type="checkbox" name="accept" value="">
													I agree to the privacy policy and consent to receiving real estate
													related emails from this website (you can unsubscribe at any time).
													<span class="form-check-sign">
															<span class="check"></span>
													</span>
												</label>
											</div>
											<button type="submit" class="btn btn-warning btn-round hd-font fs-14 mt-3">
												Register
											</button>
										</form>
									</div>
									<div class="col-lg-6 brd-left">
										<form action="{{route('app-auth.login')}}" id="loginForm" enctype="multipart/form-data">
											<input type="email" class="form-control" name="email" placeholder="Your Email *">
											<input type="password" class="form-control" name="password" placeholder="Your Password *">
											<div class="form-check">
												<label class="form-check-label">
													<input class="form-check-input" type="checkbox" name="remember">
													<span class="form-check-sign"><span class="check"></span></span>Remember
													me
												</label>
											</div>
											<button type="submit" class="btn btn-warning btn-round hd-font fs-14 mt-3">
												Login
											</button>
											<span class="clr-ef">Forgot your password? <a href="#" class="reset_link clr-blue">Reset it</a>.</span>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 text-center">
				<div class="nav-menu hd-font">
					@include('App.Template.navBar')
				</div>
			</div>
			@php($Social = \App\Option::SocialMedia()->value)
			<div class="col-lg-3 text-center text-lg-right m-auto">
				<div class="social-icon">
					@foreach($Social??[] as $s)
						<a href="{{$s['Link']}}" class="btn btn-secondary btn-fab btn-round">
							<i class="fab fa-{{$s['Icon']}}"></i>
						</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</header>
<!--Start-->
