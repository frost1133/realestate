<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="_token" content="{{csrf_token()}}">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
	@include('App.Template.styles')
	@yield('PageStyles')
	@yield('PageMeta')
	<title>@yield('PageTitle')</title>
</head>
<body class="home-page">
@include('App.Template.header')

@yield('content')

@include('App.Template.footer')
<!-- Start JS -->
@include('App.Template.scripts')
@yield('PageScripts')
<!-- End JS -->
</body>
</html>
