<script type="text/javascript" src="/js/buttons.js"></script>
<script type="text/javascript" src="/js/jquery-min.js"></script>
<script type="text/javascript" src="/js/popper.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-material-design.min.js"></script>
<script type="text/javascript" src="/js/material-kit.min.js"></script>
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/nouislider.min.js"></script>
<script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/contant.js"></script>
<script type="text/javascript" src="/js/auth.js"></script>


<script>
    let Auth = {{ (auth()->check()?'true':'false') }} ;
    csrfToken = {
        tag: document.querySelector('meta[name="X-CSRF-TOKEN"]'),
        set: function (newCSRF) {
            this.tag.setAttribute('content', newCSRF)
        },
        get: function () {
            return this.tag.getAttribute('content')
        }
    }
</script>