@extends('App.Template.layout')
@section('content')
	@include('App.Index.slider' , ['Slider'=> $Slider])
	@include('App.Index.aboutMe' , ['About' => $About])
	@include('App.Index.category' , ['QSearch' => $QSearch])
	@include('App.Index.callTo' , ['BuyerSeller' => $BuyerSeller])
	@include('App.Index.feature')
	@include('App.Index.testimonials' , ['Testimonials'=>$Testimonials])
	@include('App.Index.recentBlog',['resentArticle'=>$resentArticle])
@endsection
@section('PageScripts')
	@if(session('NeedAuth'))
		<script>
            $(document).ready(function () {
                let Auth = false
                CheckAuth()
            })
		</script>
	@endif
@endsection
