@extends('Admin.Template.layout')

@section('content')
	@include('Admin.Template.notification')
	<form action="{{route('admin.options.general')}}" method="post">
		@csrf
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="card shadow mb-4">
					<div class="card-header py-6">
						<h6 class="m-0 font-weight-bold text-primary" style="display: contents">Contacts</h6>
						<button type="button" class="btn btn-success " onclick="addContacts()" style="float: right">AddContacts</button>
					
					</div>
					<div class="card-body" id="addContacts">
					
					@foreach($oldData['Contacts']??$Contacts??[] as $index=>$a)
							<div class="row Contacts" draggable="true" ondragover="Contact.DragTag.AllowDrag(event)" ondrop="Contact.DragTag.Drop(event)" ondragstart="Contact.DragTag.Drag(event)" ondragend="Contant.DragTag.EndDrag(event)">								<div class="col-md-2">
									<div class="form-group">
									<label for="icon">Icon</label>
									<input type="text" class="form-control" name="ContactsIcon[]" id="Icon" value="{{$a['Icon']??null}}" required >
								</div>
							</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="Name">Name</label>
								<input type="text" class="form-control" name="ContactsName[]" id="Name" value="{{$a['Name']??null}}" required >
								</div>
							</div>
							
								<div class="col-md-5">
									<div class="form-group">
										<label for="Address">Value</label>
								<input type="text" class="form-control" name="ContactsValue[]" id="Value" value="{{$a['Value']??null}}" required >
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="card shadow mb-4">
					<div class="card-header py-6">
						<h6 class="m-0 font-weight-bold text-primary" style="display: contents">Home Page Options</h6>
						<button type="button" class="btn btn-success " onclick="addMedia()" style="float: right">AddSocialMedia
						</button>
					
					</div>
					<div class="card-body" id="socialMedia">
						@foreach($oldData['SocialMedia']??$SocialMedia as $i=>$s)
							<div class="row sMedia" draggable="true" ondrop="SocialMedia.DragTag.Drop(event)" ondragover="SocialMedia.DragTag.AllowDrag(event) " ondragstart="SocialMedia.DragTag.Drag(event)" ondragend="SocialMedia.DragTag.EndDrag(event)">
								<div class="col-md-2">
									<div class="form-group">
										<label for="icon">Icon</label>
										<input type="text" name="SocialMediaIcon[]" class="form-control" value="{{$s['Icon']}}">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="title">Title</label>
										<input type="text" name="SocialMediaName[]" class="form-control" value="{{$s['Name']}}">
									</div>
								</div>
								
								<div class="col-md-7">
									<div class="form-group">
										<label for="title">Link</label>
										<input type="url" name="SocialMediaLink[]" class="form-control" value="{{$s['Link']}}">
										@error('SocialMediaLink.'.$i)
										<span>the field value must be a link</span>
										@enderror
									</div>
								</div>
							</div>
						
						@endforeach
					</div>
					<button type="submit" class="btn btn-success ">send</button>
				
				</div>
			</div>
		</div>
	</form>
@endsection
@section('css')
@endsection
@section('js')
	

        <script>
        function addMedia() {
            let element = `
                       <div class="row sMedia" draggable="true" ondrop="SocialMedia.DragTag.Drop(event)" ondragover="SocialMedia.DragTag.AllowDrag(event) " ondragstart="SocialMedia.DragTag.Drag(event)" ondragend="SocialMedia.DragTag.EndDrag(event)">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="icon">Icon</label>
                                        <input type="text" name="SocialMediaIcon[]" class="form-control" value="{{$s['Icon']}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" name="SocialMediaName[]" class="form-control" value="{{$s['Name']}}">
                                    </div>
                                </div>
                                
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="title">Link</label>
                                        <input type="url" name="SocialMediaLink[]" class="form-control" value="{{$s['Link']}}">
                                        @error('SocialMediaLink.'.$i)
                                                    <span>the field value must be a link</span>
			                                        @enderror
                                                </div>
			                                </div>
			                            </div>
`
            $('#socialMedia').append(element)

        }

        function addContacts() {
            let es = `
		<div class="row Contacts" draggable="true" ondragover="Contact.DragTag.AllowDrag(event)" ondrop="Contact.DragTag.Drop(event)" ondragstart="Contact.DragTag.Drag(event)" ondragend="Contant.DragTag.EndDrag(event)">                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="icon">Icon</label>
                                    <input type="text" class="form-control" name="ContactsIcon[]" id="Icon" value="{{$a['Icon']??null}}" required >
                                </div>
                            </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="Name">Name</label>
                                <input type="text" class="form-control" name="ContactsName[]" id="Name" value="{{$a['Name']??null}}" required >
                                </div>
                            </div>
                            
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="Address">Value</label>
                                <input type="text" class="form-control" name="ContactsValue[]" id="Value" value="{{$a['Value']??null}}" required >
                                </div>
                            </div>
                        </div>
		                      
`
            $('#addContacts').append(es)

        }

        let SocialMedia = {
            DragTag: {
                AllowDrag(c) {
                    if (this.tag && this.tag !== null) {
                        c.preventDefault()
                    }
                },
                Drag(c) {
                    if (c.target) {
                        this.tag = c.path.find(t => t.className == 'row sMedia')
                    }
                },
                Drop(c) {
                    if (this.tag && this.tag !== null) {
                        let swap = c.path.find(t => t.className == 'row sMedia')

                        let swapClone = swap.cloneNode(true)
                        let tagClone = this.tag.cloneNode(true)
                        this.tag.after(swapClone)
                        swap.after(tagClone)
                        swap.remove()
                        this.tag.remove()
                    }
                },
                EndDrag(c) {
                    this.tag = null

                }

            }
        }
        let Contact = {
            DragTag: {
                AllowDrag(c) {
                    if (this.contact && this.contact !== null) {
                        c.preventDefault()
                    }
                },
                Drag(c) {
                    if (c.target) {
                        this.contact = c.path.find(t => t.className == 'row Contacts')

                    }
                },
                Drop(c) {
                    if (this.contact && this.contact !== null) {

                        let swap = c.path.find(t => t.className == 'row Contacts')
                        let swapClone = swap.cloneNode(true)
                        let contactClone = this.contact.cloneNode(true)
                        this.contact.after(swapClone)
                        swap.after(contactClone)
                        swap.remove()
                        this.contact.remove()
                    }
                },
                EndDrag(c) {
                    this.content = null
                }
            }

        }
	
	
	</script>
@endsection