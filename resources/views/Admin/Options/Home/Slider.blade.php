@if($type  == 'HTML')
	<div class="card shadow mb-4">
		<div class="card-header py-6">
			<h6 class="m-0 font-weight-bold text-primary">Slider</h6>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-xl-6 col-md-6 col-sm-8">
					<div class="form-group">
						<input type="text" name="SliderTitle" id="SliderTitle" class="form-control" value="{{$slider['Title']??NULL}}" placeholder="Enter Title Slider">
					</div>
				</div>
				<div class="col-xl-6 col-md-6 col-sm-4">
					<button type="button" class="btn btn-success" id="AddSlide" onclick="slider.AddSlide()">Add
						Slide
					</button>
				</div>
			</div>
			<div class="row" id="slider">
				@foreach($slider['Slides'] as $slideNum => $slide)
					<div class="col-xl-2 col-md-3 col-sm-4 mb-1 slide" ondrop="slider.DragSlide.Drop(event)" ondragover="slider.DragSlide.AllowDrag(event)">
						<h5 class="text-center mt-1">Slide {{$slideNum+1}}</h5>
						<hr class="mt-0">
						<div class="slide-image" draggable="true" ondragstart="slider.DragSlide.Drag(event)">
							<img draggable="false" src="{{$slide['Image']}}" alt="">
							<input type="text" name="SlideImages[]" value="{{$slide['Image']}}" hidden>
							<input type="text" name="SlideImagesAlt[]" value="{{$slide['Alt']}}" class="form-control" placeholder="Enter Alt Image" required>
						</div>
						<button type="button" class="btn btn-danger btn-sm btn-circle" onclick="slider.RemoveSlide(event)">
							<i class="fas fa-trash"></i>
						</button>
					</div>
				@endforeach
			</div>
			<hr>
			<button type="submit" class="btn btn-success">Send</button>
		</div>
	</div>
@elseif($type == 'Styles')
	<style>
		.slide-image img {
			width: 100%;
		}
		
		.slide {
			position: relative;
		}
		
		.slide > button {
			position: absolute;
			top: -10px;
			right: 10px;
			opacity: 0;
			transition: 0.3s;
		}
		
		.slide:hover > button {
			opacity: 1;
			top: 0;
			transition: 0.3s;
		}
	</style>
@elseif($type == 'Scripts')
	<script>
        let buttona = null

        let slider = {
            SortSlides() {
                let slides = $('#slider').children()
                slides.each(function (index, item) {
                    let num = index + 1
                    item.children.item(0).textContent = 'Slide ' + num
                    // item.children.item(2).children.item(1).setAttribute('name', 'SlideImage-' + num)
                    // item.children.item(2).children.item(2).setAttribute('name', 'SlideImageAlt-' + num)
                })
            },
            AddSlide() {
                let inputfile = document.createElement('input')
                inputfile.type = 'file'
                inputfile.accept = 'image/jpeg,image/png'
                inputfile.onchange = function () {
                    let data = new FormData();
                    data.append('slide', inputfile.files[0])
                    let xhr = new XMLHttpRequest()
                    xhr.open('POST', '{{route('admin.options.home.imageSlider')}}')
                    xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken.get())
                    xhr.responseType = 'json'

                    xhr.onload = function (res) {
                        res = res.target.response
                        if (xhr.status == 200) {
                            if (res.success) {
                                $('#slider').append(slider.slideElement(res.slide))
                                slider.SortSlides()
                                csrfToken.set(res['csrf-token'])
                            } else {
                                CustomAlert('fa-danger', 'warning', 'WARNING:', res.message)
                                csrfToken.set(res['csrf-token'])
                            }
                        }
                    }
                    xhr.send(data)
                }
                inputfile.click()
            },
            RemoveSlide(e) {
                let tar = e.target
                let sli
                if (tar.nodeName != "BUTTON") {
                    sli = $(tar.parentElement.parentElement)
                } else {
                    sli = $(tar.parentElement)
                }
                sli.fadeOut(function () {
                    sli.remove()
                })
            },
            slideElement(url) {
                let element = `
	                <div class="col-xl-2 col-md-3 col-sm-4 mb-1 slide" ondrop="slider.DragSlide.Drop(event)" ondragover="slider.DragSlide.AllowDrag(event)" >
                        <h5 class="text-center mt-1">Slide</h5>
                        <hr class="mt-0">
                        <div class="slide-image" draggable="true" ondragstart="slider.DragSlide.Drag(event)">
                            <img draggable="false" src="${url}" alt="">
                            <input type="text" name="SlideImages[]" value="${url}" hidden>
                            <input type="text" name="SlideImagesAlt[]" class="form-control" placeholder="Enter Alt Image" required>
                        </div>
						<button type="button" class="btn btn-danger btn-sm btn-circle" onclick="slider.RemoveSlide(event)">
							<i class="fas fa-trash"></i>
						</button>
                    </div>
	           `
                return element
            },
            DragSlide: {
                AllowDrag(e) {
                    if (this.Slide !== null) {
                        e.preventDefault()
                    }
                },
                Drag(e) {
                    if (e.target) {
                        this.Slide = e.target
                    }
                },
                Drop(e) {
                    let DropArea = e.path.find(x => x.classList.contains('slide'))
                    let Swap = $(DropArea).children('.slide-image')[0]
                    let SwapAfter = Swap.previousElementSibling
                    let DragAfter = this.Slide.previousElementSibling
                    DragAfter.after(Swap)
                    SwapAfter.after(this.Slide)
                    slider.SortSlides()
                    this.Slide = null
                },
                Slide: null
            }
        }
	</script>
@endif