@if($type  == 'HTML')
	<div class="card shadow mb-4">
		<div class="card-header py-6">
			<h6 class="m-0 font-weight-bold text-primary">BuyerSeller</h6>
		</div>
		<div class="card-body">
			<div class="row">
				
				<div class="col-md-8">
					<div class="form-group">
						<label for="BuyerSellerTitle">BuyerSeller Title</label>
						<input type="text" name="BuyerSellerTitle" id="BuyerSellerTitle" class="form-control" value="{{$BuyerSeller['Title']??NULL}}">
					</div>
					<div class="form-group">
						<label for="BuyerSellerBody">BuyerSeller Body</label>
						<textarea name="BuyerSellerBody" id="BuyerSellerBody" class="form-control">{{$BuyerSeller['Body']??NULL}}</textarea>
					</div>
				</div>
				<div class="col-md-4">
					<img src="{{$BuyerSeller['Image']??NULL}}" style="width: 100%" onclick="BuyerSellerImageUpload(event)">
					<input type="text" hidden name="BuyerSellerImage" id="BuyerSellerImage" value="{{$BuyerSeller['Image']??NULL}}">
				</div>
			</div>
		</div>
	</div>
@elseif($type == 'Styles')
@elseif($type == 'Scripts')
	<script>
		function BuyerSellerImageUpload(e) {
            let target = e.target
            let imgBuyerSeller = target.nextElementSibling
            UPImage('HomeBuyerSeller', function (res) {
                console.log(res)
                if (res.success) {
                    target.setAttribute('src' , res.image)
                    imgBuyerSeller.setAttribute('value' , res.image)
                }
            })
        }
	</script>
@endif