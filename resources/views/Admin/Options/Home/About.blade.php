@if($type  == 'HTML')
	<div class="card shadow mb-4">
		<div class="card-header py-6">
			<h6 class="m-0 font-weight-bold text-primary">About Us</h6>
		</div>
		<div class="card-body">
			<div class="row">
				
				<div class="col-md-8">
					<div class="form-group">
						<label for="AboutTitle">About Title</label>
						<input type="text" name="AboutTitle" id="AboutTitle" class="form-control" value="{{$About['Title']??NULL}}">
					</div>
					<div class="form-group">
						<label for="AboutBody">About Body</label>
						<textarea name="AboutBody" id="AboutBody" class="form-control">{{$About['Body']??NULL}}</textarea>
					</div>
				</div>
				<div class="col-md-4">
					<img src="{{$About['Image']??NULL}}" style="width: 100%" onclick="AboutImageUpload(event)">
					<input type="text" hidden name="AboutImage" id="AboutImage" value="{{$About['Image']??NULL}}">
				</div>
			</div>
		</div>
	</div>
@elseif($type == 'Styles')
@elseif($type == 'Scripts')
	<script>
        function AboutImageUpload(e) {
            let target = e.target
            let imgAbout = target.nextElementSibling
            UPImage('HomeAbout', function (res) {
                console.log(res)
                if (res.success) {
                    target.setAttribute('src' , res.image)
                    imgAbout.setAttribute('value' , res.image)
                }
            })
        }
	</script>
@endif