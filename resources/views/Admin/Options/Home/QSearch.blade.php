@if($type  == 'HTML')
	<div class="card shadow mb-4">
		<div class="card-header py-6">
			<h6 class="m-0 font-weight-bold text-primary">Home Page Options</h6>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-xl-6 col-md-6 col-sm-8">
					<div class="form-group">
						<input type="text" name="QSearchTitle" id="QSearchTitle" class="form-control" value="{{$QSearch['Title']??NULL}}" placeholder="Enter QSearch Title">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 culom-4 mt-4" ondragover="QSearch.DragQS.AllowDrag(event)" ondrop="QSearch.DragQS.Drop(event)">
					<div class="banner-area" draggable="true" ondragstart="QSearch.DragQS.Drag(event)">
						<img src="{{$QSearch['QSearchRow1'][0]['Image']}}" alt="" draggable="false" onclick="QSearch.UploadImage(event)">
						<input type="text" hidden name="QSearchRow1Image[]" class="form-control" value="{{$QSearch['QSearchRow1'][0]['Image']}}">
						<input type="text" name="QSearchRow1Alt[]" class="form-control" placeholder="Enter Alt Image " value="{{$QSearch['QSearchRow1'][0]['Alt']}}">
						<input type="text" name="QSearchRow1Link[]" class="form-control" placeholder="Enter Link" value="{{$QSearch['QSearchRow1'][0]['Link']}}">
					</div>
				</div>
				<div class="col-lg-4 culom-4 mt-4" ondragover="QSearch.DragQS.AllowDrag(event)" ondrop="QSearch.DragQS.Drop(event)">
					<div class="banner-area " draggable="true" ondragstart="QSearch.DragQS.Drag(event)">
						<img src="{{$QSearch['QSearchRow1'][1]['Image']}}" alt="" draggable="false" onclick="QSearch.UploadImage(event)">
						<input type="text" hidden name="QSearchRow1Image[]" class="form-control" value="{{$QSearch['QSearchRow1'][1]['Image']}}">
						<input type="text" name="QSearchRow1Alt[]" class="form-control" placeholder="Enter Alt Image " value="{{$QSearch['QSearchRow1'][1]['Alt']}}">
						<input type="text" name="QSearchRow1Link[]" class="form-control" placeholder="Enter Link" value="{{$QSearch['QSearchRow1'][1]['Link']}}">
					</div>
				</div>
				<div class="col-lg-4 culom-4 mt-4" ondragover="QSearch.DragQS.AllowDrag(event)" ondrop="QSearch.DragQS.Drop(event)">
					<div class="banner-area " draggable="true" ondragstart="QSearch.DragQS.Drag(event)">
						<img src="{{$QSearch['QSearchRow1'][2]['Image']}}" alt="" draggable="false" onclick="QSearch.UploadImage(event)">
						<input type="text" hidden name="QSearchRow1Image[]" class="form-control" value="{{$QSearch['QSearchRow1'][2]['Image']}}">
						<input type="text" name="QSearchRow1Alt[]" class="form-control" placeholder="Enter Alt Image " value="{{$QSearch['QSearchRow1'][2]['Alt']}}">
						<input type="text" name="QSearchRow1Link[]" class="form-control" placeholder="Enter Link" value="{{$QSearch['QSearchRow1'][2]['Link']}}">
					</div>
				</div>
				<div class="col-lg-6 culom-4  mt-4" ondragover="QSearch.DragQS.AllowDrag(event)" ondrop="QSearch.DragQS.Drop(event)">
					<div class="banner-area" draggable="true" ondragstart="QSearch.DragQS.Drag(event)">
						<img src="{{$QSearch['QSearchRow2'][0]['Image']}}" alt="" draggable="false" onclick="QSearch.UploadImage(event)">
						<input type="text" hidden name="QSearchRow2Image[]" class="form-control" value="{{$QSearch['QSearchRow2'][0]['Image']}}">
						<input type="text" name="QSearchRow2Alt[]" class="form-control" placeholder="Enter Alt Image " value="{{$QSearch['QSearchRow2'][0]['Alt']}}">
						<input type="text" name="QSearchRow2Link[]" class="form-control" placeholder="Enter Link" value="{{$QSearch['QSearchRow2'][0]['Link']}}">
					</div>
				</div>
				<div class="col-lg-6 culom-4 mt-4" ondragover="QSearch.DragQS.AllowDrag(event)" ondrop="QSearch.DragQS.Drop(event)">
					<div class="banner-area" draggable="true" ondragstart="QSearch.DragQS.Drag(event)">
						<img src="{{$QSearch['QSearchRow2'][1]['Image']}}" alt="" draggable="false" onclick="QSearch.UploadImage(event)">
						<input type="text" hidden name="QSearchRow2Image[]" class="form-control" value="{{$QSearch['QSearchRow2'][1]['Image']}}">
						<input type="text" name="QSearchRow2Alt[]" class="form-control" placeholder="Enter Alt Image " value="{{$QSearch['QSearchRow2'][1]['Alt']}}">
						<input type="text" name="QSearchRow2Link[]" class="form-control" placeholder="Enter Link" value="{{$QSearch['QSearchRow2'][1]['Link']}}">
					</div>
				</div>
			</div>
		</div>
	</div>
@elseif($type == 'Styles')
@elseif($type == 'Scripts')
	<script>
        let QSearch = {
            UploadImage(e) {
                let target = e.target
                let inputfile = document.createElement('input')
                inputfile.type = 'file'
                inputfile.accept = 'image/jpeg,image/png'
                inputfile.onchange = function () {
                    let data = new FormData();
                    data.append('QSearch', inputfile.files[0])
                    let xhr = new XMLHttpRequest()
                    xhr.open('POST', '{{route('admin.options.home.ImageQSearch')}}')
                    xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken.get())
                    xhr.responseType = 'json'

                    xhr.onload = function (res) {
                        res = res.target.response
                        if (xhr.status == 200) {
                            if (res.success) {
                                target.setAttribute('src', res.QSearch)
                                console.log(target.nextElementSibling.value, target.nextElementSibling, target);
                                target.nextElementSibling.value = res.QSearch
                                target.nextElementSibling.setAttribute('value', res.QSearch)
                                csrfToken.set(res['csrf-token'])
                            } else {
                                CustomAlert('fa-danger', 'warning', 'WARNING:', res.message)
                                csrfToken.set(res['csrf-token'])
                            }
                        }
                    }
                    xhr.send(data)
                }
                inputfile.click()
            },
            DragQS: {
                AllowDrag(e) {
                    if (this.Item.SwapArea) {
                        e.preventDefault()
                    }
                },
                Drag(e) {
                    let tar = e.target.children
                    this.Item.SwapArea = tar
                    this.Item.Image = tar.item(1).value
                    this.Item.Alt = tar.item(2).value
                    this.Item.Link = tar.item(3).value

                },
                Drop(e) {
                    let over = e.path.find(x => x.getAttribute('ondragover') === "QSearch.DragQS.AllowDrag(event)")
                    let DragArea = over.children.item(0).children
                    let SwapArea = this.Item.SwapArea
                    let Swap = {
                        Image: DragArea.item(1).value,
                        Alt: DragArea.item(2).value,
                        Link: DragArea.item(3).value,
                    }
                    let Drag = this.Item
                    DragArea.item(0).setAttribute('src', Drag.Image)
                    DragArea.item(1).setAttribute('value' , Drag.Image)
                    DragArea.item(2).value = Drag.Alt
                    DragArea.item(3).value = Drag.Link

                    SwapArea.item(0).setAttribute('src', Swap.Image)
                    SwapArea.item(1).setAttribute('value' , Swap.Image)
                    SwapArea.item(2).value = Swap.Alt
                    SwapArea.item(3).value = Swap.Link

                    this.Item = {}
                },
                Item: {}
            }
        }
	</script>
@endif