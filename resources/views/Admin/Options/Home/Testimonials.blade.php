@if($type  == 'HTML')
	<div class="card shadow mb-4">
		<div class="card-header py-6">
			<h6 class="m-0 font-weight-bold text-primary" style="display: contents">Testimonials</h6>
			<button type="button" class="btn btn-success" id="AddTestimonial" style="float: right" onclick="Testimonial.add()">
				Add Testimonial
			</button>
		</div>
		<div class="card-body">
			<div class="row" id="TestimonialsArea">
				@foreach($Testimonials as $Testimonial)
				<div class="card border-left-info shadow h-100 py-2 col-sm-12 mb-2 Testimonial">
					<div class="card-body">
						<div class="row">
							<div class="col-md-9">
								<div class="form-group">
									<label for="TestimonialName">Testimonial Name</label>
									<input type="text" name="TestimonialName[]" id="TestimonialName" class="form-control" value="{{$Testimonial['Name']??NULL}}">
								</div>
								<div class="form-group">
									<label for="TestimonialTitle">Testimonial Title</label>
									<input type="text" name="TestimonialTitle[]" id="TestimonialTitle" class="form-control" value="{{$Testimonial['Title']??NULL}}">
								</div>
								<div class="form-group">
									<label for="TestimonialBody">Testimonial Body</label>
									<textarea name="TestimonialBody[]" id="TestimonialBody" class="form-control">{{$Testimonial['Body']??NULL}}</textarea>
								</div>
							</div>
							<div class="col-md-3">
								<img draggable="false" src="{{$Testimonial['Image']??NULL}}" onclick="Testimonial.ImageUpload(event)">
								<input type="text" hidden name="TestimonialImage[]" id="TestimonialImage" value="{{$Testimonial['Image']??NULL}}">
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@elseif($type == 'Styles')
	<style>
	    .Testimonial img{
		    width: 100%;
		    object-fit: contain;
		    max-height: 250px;
	    }
	</style>
@elseif($type == 'Scripts')
	<script>
        let Testimonial = {
            ImageUpload(e) {
                let image = e.target
                let TMImage = image.nextElementSibling
                UPImage('HomeTestimonial', function (res) {
                    if (res.success) {
                        image.setAttribute('src', res.image)
                        TMImage.setAttribute('value', res.image)
                    }
                })
            },
            add() {
                UPImage('HomeTestimonial', function (res) {
                    if (res.success) {
                        let element = Testimonial.element(res.image)
                        $('#TestimonialsArea').append(element)
                    }
                })
            },
            remove(e) {
                e.path.find(e => e.className = 'card border-left-info shadow h-100 py-2 col-sm-12 Testimonial').remove()
            },
            element(ImageUrl) {
                let element = `
				<div class="card border-left-info shadow h-100 py-2 col-sm-12 mb-2 Testimonial">
					<div class="card-body">
						<div class="row">
							<div class="col-md-9">
								<div class="form-group">
									<label for="TestimonialName">Testimonial Name</label>
									<input type="text" name="TestimonialName[]" id="TestimonialName" class="form-control" value="">
								</div>
								<div class="form-group">
									<label for="TestimonialTitle">Testimonial Title</label>
									<input type="text" name="TestimonialTitle[]" id="TestimonialTitle" class="form-control" value="">
								</div>
								<div class="form-group">
									<label for="TestimonialBody">Testimonial Body</label>
									<textarea name="TestimonialBody[]" id="TestimonialBody" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-3">
								<img draggable="false" src="${ImageUrl}" style="width: 100%" onclick="Testimonial.ImageUpload(event)">
								<input type="text" hidden name="TestimonialImage[]" id="TestimonialImage" value="${ImageUrl}">
							</div>
						</div>
					</div>
				</div>`
                return element
            }

        }
	</script>
@endif