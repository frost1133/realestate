@extends('Admin.Template.layout')
@if($errors->any())
	@dd($errors->all())
@endif
@section('content')
	<form action="{{route('admin.options.home')}}" method="post">
		@csrf
		<div class="card shadow mb-4">
			<div class="card-header py-6">
				<h6 class="m-0 font-weight-bold text-primary">Home Page Options</h6>
			</div>
			<div class="card-body">
			
			</div>
		</div>
		@include('Admin.Options.Home.Slider' , ['type' => 'HTML' , 'slider' => $slider])
		@include('Admin.Options.Home.QSearch' , ['type' => 'HTML' , 'QSearch' => $QSearch])
		@include('Admin.Options.Home.About' , ['type' => 'HTML' , 'About' => $About])
		@include('Admin.Options.Home.BuyerSeller' , ['type' => 'HTML' , 'BuyerSeller' => $BuyerSeller])
		@include('Admin.Options.Home.Testimonials' , ['type' => 'HTML' , 'Testimonials' => $Testimonials])
	</form>
@endsection
@section('css')
	@include('Admin.Options.Home.Slider' , ['type' => 'Styles'])
	@include('Admin.Options.Home.Testimonials' , ['type' => 'Styles'])
	<style>
		.banner-area img {
			object-fit: cover;
			height: 260px;
			width: 100%;
		}
	</style>
@endsection
@section('js')
	<script>
        function UPImage(directory, callBackFunction) {
            let inputfile = document.createElement('input')
            inputfile.type = 'file'
            inputfile.accept = 'image/jpeg,image/png'
            inputfile.onchange = function () {
                let data = new FormData();
                data.append('image', inputfile.files[0])
                data.append('directory', directory)
                let xhr = new XMLHttpRequest()
                xhr.open('POST', '{{route('admin.options.upImage')}}')
                xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken.get())
                xhr.responseType = 'json'

                xhr.onload = function (res) {
                    res = res.target.response
                    if (xhr.status == 200) {
                        callBackFunction(res)
                    }
                }
                xhr.send(data)
            }
            inputfile.click()
        }
	
	</script>
	@include('Admin.Options.Home.Slider' , ['type' => 'Scripts'])
	@include('Admin.Options.Home.QSearch' , ['type' => 'Scripts'])
	@include('Admin.Options.Home.About' , ['type' => 'Scripts'])
	@include('Admin.Options.Home.BuyerSeller' , ['type' => 'Scripts'])
	@include('Admin.Options.Home.Testimonials' , ['type' => 'Scripts'])
@endsection