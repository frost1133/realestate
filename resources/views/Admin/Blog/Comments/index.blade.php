@extends('Admin.Template.layout')
@section('content')
	@include('Admin.Template.notification')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">ListRequest:</h6>
		</div>
		<div class="card-body">
			<form class="" action="{{request()->fullUrl()}}">
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-md-2 col -sm-12-col-xl-1">
						<select name="ticketType" id="ticketType" class="form-control">
							<option value {{request('ticketType')? 'selected' : NULL}}>Select Ticket Type</option>
							<option value="0" {{request('ticketType') === 0 ? 'selected':NULL}}>But</option>
							<option value="1" {{request('ticketType') === 1 ? 'selected':NULL}}>Sell</option>
						</select>
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="hidden" name="type" value="{{old('type')}}">
						<input type="text" class="form-control" placeholder="Name" name="name" value="{{request('name')}}">
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="email" class="form-control" placeholder="Email" name="email" value="{{request('email')}}">
					</div>
					<div class="col-md-4 col-sm-12 col-xl-3">
						<input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" value="{{old('phone')}}">
					</div>
					<div class="col-sm-2 col-sn-12 col-xl-2">
						<button type="submit" class="btn btn-outline-primary">Apply</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover" border="5">
				<thead>
				<tr>
					<th style="text-align: center">Name</th>
					<th style="text-align: center">Email</th>
					<th style="text-align: center">Comment</th>
					<th style="text-align: center">Article</th>
					<th style="text-align: center">Confirm</th>
					<th style="text-align: center">Setting
				</tr>
				</thead>
				<tbody>
				@foreach($comments as $comment)
					<tr commentId="{{$comment->id}}">
						<td>{{$comment->user->name??$comment->admin->id??NULL}}</td>
						<td>{{$comment->user->email??$comment->admin->id??NULL}}</td>
						<td class="CommentBody">
							<p data-CommentId="{{$comment->id}}" style="width:100%;">{{$comment->body}}</p>
							<div class="btn-group-vertical comment-update-btns" style="display: none">
								<button onclick="update({{$comment->id}} , event)" class="btn btn-success">Update
								</button>
								<button onclick="cancel(event)" class="btn btn-success">Cancel</button>
							</div>
							<button onclick="edit(event)" class="btn btn-success comment-edit">Edit</button>
						</td>
						<td>{{$comment->article->title??NULL}}</td>
						<td>{{$comment->confirmComment?'Confirmed' : 'NotConfirmed'}}</td>
						<td>
							<form action="{{route('admin.blog.comments.confirm' , $comment)}}" method="post">
								@csrf
								<div class="btn-group">
									<a href="{{route('admin.blog.comments.delete' , $comment)}}" class="btn btn-danger">Delete</a>
									<button class="btn btn-info" type="submit">{{$comment->confirmComment?'NotConfirm' : 'Confirm'}}</button>
								</div>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('css')
	<style>
		td.CommentBody {
			position: relative;
		}
		
		button.btn.btn-success.comment-edit::-webkit-scrollbar {
			display: none;
		}
		
		button.btn.btn-success.comment-edit {
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			border-radius: 0;
			overflow-x: hidden;
			padding: 0;
			margin: 0;
			width: 0;
			transition: 0.5s;
			border: 0;
		}
		
		button.btn.btn-success.comment-edit {
			display: -webkit-inline-box;
		}
		
		td.CommentBody:hover > .comment-edit {
			padding: inherit;
			margin: inherit;
			width: 51px;
			border: initial;
			transition: 0.5s;
		}
		
		.btn-group-vertical.comment-update-btns {
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
		}
		
		.btn-group-vertical.comment-update-btns > .btn {
			border-radius: 0;
		}
	</style>
@endsection
@section('js')
	<script>
        function edit(e) {
            let target = $(e.target)
            let p = target.prev().prev();
            let comment = p.text();
            let textarea = $("<textarea></textarea>");
            textarea.val(comment);
            textarea.css("width", "calc(100% - 80px)")
            p.after(textarea);
            p.remove()
            target.hide();
            target.prev().show()
        }

        function cancel(e) {
            let target = $(e.target)
            let t = target.parent().prev();
            let comment = t.val();
            let p = $("<p></p>");
            p.text(comment);
            p.css("width", "100%")
            t.after(p);
            t.remove()
            target.parent().hide();
            target.parent().next().show();
        }

        function update(commentId, e) {
            let target = $(e.target)
            let comment = target.parent().prev().val();
            let token = '{{csrf_token()}}' ;
            $.post('{{route('admin.blog.comments.update')}}' , {!!  "{'_token' : token , id: commentId,comment: comment}" !!}).done(function (response) {
                let p = $("<p></p>");
                let t = target.parent().prev();
                p.text(comment);
                p.css("width", "100%")
                t.after(p);
                t.remove()
                target.parent().hide();
                target.parent().next().show();
            });
        }
	</script>
@endsection
