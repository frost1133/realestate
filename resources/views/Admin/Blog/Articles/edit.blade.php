@extends('Admin.Template.layout')
@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/admin/vendor/select2tree/select2totree.css">
	<style>
		.ck-editor__editable {
			min-height: 500px;
		}
	</style>
@endsection
@php
global $articleData;
	$articleData = $article;
	function OldArticle($field){
		global $articleData;
        if (old($field)){
        	return old($field);
        }
        $field = ($field=='publishTime'?'releaseTime':$field);
        return $articleData->getAttribute($field);
	}
@endphp
@section('content')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary" style="display: contents">Create Article</h6>
			<a style="float: right" href="{{route('admin.blog.articles.index')}}">
				<button type="button" class="btn btn-success btn-sm">Index</button>
			</a>
		</div>
		<div class="card-body">
			@include('Admin.Template.notification')
			<form action="{{route('admin.blog.articles.update' , $article)}}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-9">
						<div class="card mb-3">
							<div class="card-body">
								<div class="form-group">
									<input type="text" id="Title" name="title" class="form-control" placeholder="Title" value="{{OldArticle("title")}}">
								</div>
								<textarea name="body" id="body" hidden>{{OldArticle('body')}}</textarea>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="MetaTitle">Meta Title</label>
											<input type="text" name="metaTitle" id="MetaTitle" class="form-control" value="{{OldArticle('metaTitle')}}">
										</div>
									</div>
									<div class="col-md-6">
										<label for="MetaKeywords">Meta Keywords</label>
										<select name="metaKeywords[]" id="MetaKeywords" class="form-control" multiple>
											@foreach(OldArticle('metaKeywords')??[] as $keyword)
												<option value="{{$keyword}}" selected>{{$keyword}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="MetaDescription">Meta Description</label>
									<textarea type="text" name="metaDescription" id="MetaDescription" class="form-control">{{OldArticle('metaDescription')}}</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="card mb-3">
							<div class="card-body">
								<button type="submit" class="btn btn-outline-success" onclick="document.getElementById('Status').value = '{{\App\Article::$Status['Draft']}}'">
									Draft
								</button>
								<button type="submit" class="btn btn-outline-success float-right" onclick="document.getElementById('Status').value = '{{\App\Article::$Status['Publish']}}'">
									Publish
								</button>
								<hr>
								<div class="form-group">
									<label for="Status">Status</label>
									<select name="status" id="Status" class="form-control">
										@foreach(\App\Article::$Status as  $state => $value)
											<option value="{{$value}}"{{OldArticle('status')==$value?' selected':''}}>{{$state}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="PublishTime">Publish Time</label>
									<input type="datetime-local" class="form-control" name="publishTime" id="PublishTime" value="{{(new \Carbon\Carbon(OldArticle('publishTime')))->format('yy-m-d\Th:m')}}">
								</div>
							</div>
						</div>
						<div class="card mb-3">
							<div class="card-body">
								<div class="form-group">
									<label for="Category">Category</label>
									<select name="category" id="Category" class="form-control">
										@include('Admin.Blog.Categories.tree' , ['categories' => \App\Category::whereNull('parentCategory')->get() , 'level' => 1 , 'type' => 'select' , 'select' => old('category')??$article->categories[0]->id??1])
									</select>
								</div>
								<div class="form-group">
									<label for="Tags">Tags</label>
									<select name="tag[]" id="Tags" class="form-control" multiple>
										@foreach(OldArticle('tag')??[] as $tag)
											<option value="{{$tag}}" selected>{{$tag}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="card mb-3">
							<div class="card-body">
								<div class="form-group">
									<img style="width:100%;" src="{{$article->featuredImage??'/storage/articles/Defualt.jpg'}}" alt="Feature Image" onclick="$('#FeaturedImage').click()">
									<input hidden type="file" id="FeaturedImage" name="featuredImage" accept="image/jpeg,image/png">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
@section('js')
	<script src="https://cdn.tiny.cloud/1/njaj317q8zpesyb5je3znfi3pdzsfcyn11g2c7v8id8dm7df/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
        tinymce.init({
            selector: '#body',
            plugins: 'print preview importcss  searchreplace autolink directionality image visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            mobile: {
                plugins: 'print preview importcss tinydrive searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount textpattern noneditable help charmap quickbars emoticons'
            },
            menu: {
                tc: {
                    title: 'TinyComments',
                    items: 'addcomment showcomments deleteallconversations'
                }
            },
            menubar: 'file edit view insert format tools table tc help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | insertfile image media template link anchor codesample |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | a11ycheck ltr rtl | showcomments addcomment',
            image_advtab: true,
            importcss_append: true,
            templates: [
                {
                    title: 'New Table',
                    description: 'creates a new table',
                    content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
                },
                {
                    title: 'Starting my story',
                    description: 'A cure for writers block',
                    content: 'Once upon a time...'
                },
                {
                    title: 'New list with dates',
                    description: 'New List with dates',
                    content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
                }
            ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            spellchecker_whitelist: ['Ephox', 'Moxiecode'],
            content_style: '.mymention{ color: gray; }',
            contextmenu: 'link image imagetools table configurepermanentpen',
            a11y_advanced_options: true,
            images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;

                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '{{route('admin.blog.articles.uploadImage')}}');

                xhr.upload.onprogress = function (e) {
                    progress(e.loaded / e.total * 100);
                };
                xhr.setRequestHeader("X-CSRF-TOKEN" , csrfToken.get());
                xhr.onload = function () {
                    var json;

                    if (xhr.status < 200 || xhr.status >= 300) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }

                    json = JSON.parse(xhr.responseText);
                    csrfToken.set(json.csrfToken)

                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }

                    success(json.location);
                };

                xhr.onerror = function () {
                    failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
                };

                formData = new FormData();
                formData.append('image', blobInfo.blob(), blobInfo.filename());

                xhr.send(formData);
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
	</script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script src="/admin/vendor/select2tree/select2totree.js"></script>
	<script>
        let FeaturedImage = new FileReader();
        $(document).ready(function () {
            $('#Category').select2ToTree();
            $('#Tags').select2({tags: true, tokenSeparators: ',',})
            $('#MetaKeywords').select2({tags: true, tokenSeparators: ',',})
            $('#FeaturedImage').change(function () {
                if (this.files && this.files[0]) {
                    FeaturedImage.readAsDataURL(document.getElementById('FeaturedImage').files[0]);
                    FeaturedImage.onload = e => {
                        $('#FeaturedImage').prev().attr('src', e.target.result);
                    }
                }
            });
        })
	
	</script>
@endsection

