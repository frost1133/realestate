@extends('Admin.Template.layout')
@section('content')
	@include('Admin.Template.notification')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary" style="display: contents">ListArticle</h6>
			<a style="float: right" href="{{route('admin.blog.articles.create')}}">
				<button type="button" class="btn btn-success btn-sm">create</button>
			</a>
		</div>
		<div class="card-body">
			<form class="" action="{{request()->fullUrl()}}">
				<div class="mb-2">
					<a class="badge badge-{{request('status') == 'All'?'warning' : 'dark'}}" href="{{\App\Utility\H::url('status' , 'All' , 'page')}}">
						All
						<span class="badge badge-counter badge-light">
							{{$count['All']}}
						</span>
					</a>
					<span> | </span>
					<a class="badge badge-{{request('status') == 'Published'?'warning' : 'dark'}}" href="{{\App\Utility\H::url('status' , 'Published' , 'page')}}">
						Published
						<span class="badge badge-counter badge-light">
							{{$count['Published']}}
						</span>
					</a>
					<span> | </span>
					<a class="badge badge-{{request('status') == 'PendingPublish'?'warning' : 'dark'}}" href="{{\App\Utility\H::url('status' , 'PendingPublish' , 'page')}}">
						Pending Publish
						<span class="badge badge-counter badge-light">
							{{$count['PendingPublish']}}
						</span>
					</a>
					<span> | </span>
					<a class="badge badge-{{request('status') == 'Draft'?'warning' : 'dark'}}" href="{{\App\Utility\H::url('status' , 'Draft' , 'page')}}">
						Draft
						<span class="badge badge-counter badge-light">
							{{$count['Draft']}}
						</span>
					</a>
					<span> | </span>
					<a class="badge badge-{{request('status') == 'UnPublish'?'warning' : 'dark'}}" href="{{\App\Utility\H::url('status' , 'UnPublish' , 'page')}}">
						UnPublish
						<span class="badge badge-counter badge-light">
							{{$count['UnPublish']}}
						</span>
					</a>
				</div>
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="hidden" title="status" name="status"{{request('status')?' value='.request('status'):''}}>
						<input type="text" class="form-control" placeholder="Title" name="title" value="{{request('title')}}">
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<select name="categories[]" id="Categories" class="form-control" multiple>
							@include('Admin.Blog.Categories.tree' , ['categories' => \App\Category::whereNull('parentCategory')->get() , 'level' => 1 , 'type' => 'select' , 'select' => request('categories')])
						</select>
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<select name="tags[]" id="Tags" class="form-control" multiple>
							@foreach($tags as $tag)
								<option value="{{$tag}}"{{request('tags') && in_array($tag , request('tags')) ? ' selected' : '' }}>{{$tag}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<select name="authors[]" id="Authors" class="form-control" multiple>
							@foreach($authors as $author)
								<option value="{{$author->id}}"{{request('authors') && in_array($author->id , request('authors')) ? ' selected' : '' }}>{{$author->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-2 col-sn-12 col-xl-2">
						<button type="submit" class="btn btn-outline-primary">Apply</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover" border="5">
				<thead>
				<tr>
					<th colspan="3">Title</th>
					<th style="text-align: center">Categories</th>
					<th style="text-align: center">Author</th>
					<th style="text-align: center">Tags</th>
					<th style="text-align: center">Publish Date</th>
					<th style="text-align: center">Settings</th>
				</tr>
				</thead>
				<tbody>
				@foreach($articles  as $article)
					<tr>
						<td colspan="3">{{$article->title}}</td>
						<td style="text-align: center">{{$article->categories->isNotEmpty()?implode(',',$article->categories->pluck('name')->toArray()):"Uncategorized"}}</td>
						<td style="text-align: center">{{$article->admin->name}}</td>
						<td style="text-align: center" colspan="1">{{implode(' | ' , $article->tag)??'Uncategorized'}}</td>
						<td style="text-align: center">{{$article->created_at}}</td>
						<td style="text-align: center;width: 150px">
							<div class="btn btn-info action">Action
								<div class="btn-group-vertical actions">
									<button class="btn btn-danger btn-sm" onclick="window.location.href= '{{route('admin.blog.articles.delete',$article)}}'">
										Delete
									</button>
									<button class="btn btn-warning btn-sm" onclick="window.location.href= '{{route('admin.blog.articles.edit',$article)}}'">
										Edit
									</button>
									<button class="btn btn-primary btn-sm" onclick="window.open('{{route('App.blog.show',$article)}}' , '_blank')">
										Show
									</button>
								</div>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			
			</table>
			{{$articles->links('Admin.Template.paginator')}}
		</div>
	</div>
@endsection
@section('css')
	
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/admin/vendor/select2tree/select2totree.css">
	<style>
		div.action {
			position: relative;
		}
		
		div.action > .actions {
			z-index: 100;
			opacity: 0;
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			overflow: hidden;
			transform: scale(1);
		}
		
		div.action:hover > .actions {
			opacity: 1;
			overflow: unset;
			transition: transform 0.5s;
			transform: scale(1.3);
		}
	</style>
@endsection
@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script src="/admin/vendor/select2tree/select2totree.js"></script>
	<script>
        $(document).ready(function () {
            $('#Categories').select2ToTree();
            $('#Tags').select2();
            $('#Authors').select2();
        })
	</script>
@endsection