@extends('Admin.Template.layout')
@section('content')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Create Admin 1</h6>
		</div>
		<div class="card-body">
			<form action="{{route('admin.blog.categories.update' , $category)}}" method="post">
				@csrf
				@method('patch')
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" id="name" name="name" class="form-control" value="{{$category->name}}">
						</div>
						<div class="form-group">
							<label for="slug">Slug:</label>
							<input type="text" id="slug" name="slug" class="form-control" value="{{$category->slug}}">
						</div>
						<div class="btn-group">
							<button type="submit" class="btn btn-success">Send</button>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="parentCategory">Parent Category</label>
							<select name="parentCategory" id="parentCategory" class="form-control">
								<option value{{$category->paterCategory?'':' selected'}}>Select Parent Category</option>
								<?php
								$data = [
									'categories' => \App\Category::whereNull( 'parentCategory' )->get() ,
									'level'      => 1 ,
									'type'       => 'select' ,
									'select'     => $category->parentCategory ?? FALSE ,
									'disable'    => $category->id
								]
								?>
								@include('Admin.Blog.Categories.tree' , $data)
							</select>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/admin/vendor/select2tree/select2totree.css">
@endsection
@section('js')
	
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script src="/admin/vendor/select2tree/select2totree.js"></script>
	<script>
        $('#parentCategory').select2ToTree();
	</script>
@endsection
