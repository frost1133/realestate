@extends('Admin.Template.layout')
@section('content')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<a style="float: right" href="{{route('admin.blog.categories.create')}}">
				<button type="button" class="btn btn-success btn-sm ">create</button>
			</a>
			<h6 class="m-0 font-weight-bold text-primary">List Admins</h6>
		</div>
		<div class="card-body">
			<table class="table table-bordered table-hover" border="5">
				<thead>
				<tr>
					<th style="text-align: center">Name</th>
					<th style="text-align: center">URL</th>
					<th style="text-align: center">Parent Category</th>
					<th>Settings</th>
				</tr>
				</thead>
				<tbody>
				@include('Admin.Blog.Categories.tree',['categories'=>$categories , 'level' => "" , 'type'=> 'table'])
				</tbody>
			</table>
		</div>
	</div>
@endsection