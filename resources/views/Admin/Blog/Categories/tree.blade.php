@if($type == 'select')
	@php($select = ( isset($select) ? (is_array($select)?$select:[$select]) : FALSE ) )
	@php($disable = $disable ?? FALSE)
	@foreach($categories??[] as $category)
		@if($category->id != $disable)
			<option value="{{$category->id}}" {{$category->parentCategory?' data-pup='.$category->parentCategory.'':NULL}} class="l{{$level}}{{$category->childes->isNotEmpty()?' non-leaf':NULL}}"{{($select && in_array($category->id , $select))?' selected':''}}>{{$category->name}}</option>
			@if($category->childes->isNotEmpty())
				<?php
					$data = [
						'categories' => $category->childes ,
						'level'      => $level+1 ,
						'type'       => 'select' ,
						'select'     => $select ,
						'disable'    => $disable
					];
				?>
				@include('Admin.Blog.Categories.tree', $data)
			@endif
		@endif
	@endforeach
@elseif($type == 'table')
	@foreach($categories??[] as $category)
		<tr>
			<td>{{$level.$category->name}}</td>
			<td>{{$category->slug}}</td>
			<td>{{$category->parent->name??"---"}}</td>
			<td>
				<div class="btn-group">
					<form action="{{route('admin.blog.categories.delete' , $category)}}" method="post">
						@csrf
						@method('delete')
						<a href="{{route('admin.blog.categories.edit',$category)}}" class="btn btn-warning btn-sm">Edit</a>
						<button type="submit" class="btn btn-danger btn-sm">Delete</button>
					</form>
				</div>
			</td>
		</tr>
		@if($category->childes->isNotEmpty())
			@include('Admin.Blog.Categories.tree',['categories'=>$category->childes , 'level' => $level.'_' , 'type'=> 'table'])
		@endif
	@endforeach
@endif