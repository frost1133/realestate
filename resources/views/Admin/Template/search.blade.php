@extends('Admin.Template.layout')
@section('css')
    <style>
        legend.range_lable {
            display: contents;
        }
    </style>

@endsection
@section('js')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {
            $('#slider-price-range').slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
            });
            $('#slider-sqft-range').slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
            });
            $('#slider-yearbuilt-range').slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
            });
            $('#slider-bathrooms').slider({
                range:"min",
                min: 0,
                max: 10,
                value:2,
                slide: function( event, ui ) {
                    $( "#bathroom" ).val( ui.value );
                    $("label[for='bathroom']").text(ui.value);
                }
            });
            $( "#bathroom" ).val( $( "#slider-bathrooms" ).slider( "value" ) );


            $('#slider-bedrooms').slider({
                range:"min",
                min: 0,
                max: 10,
                value:2,
                slide:function (event,ui) {
                    $("#bedroom").val(ui.value);
                    $("label[for='bedroom']").text(ui.value);
                }
            });
            $("#bedroom").val($("#slider-bedrooms").slider("value"));
        })
    </script>
@endsection
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group  ">
                    <fieldset name="price" class="form-group mls-search-form-price-container">
                        <legend class="range_lable">Price: &nbsp; </legend>
                        <label id="min_price_display" for="min_price"></label>
                        <input type="hidden" class="mls-search-form-slider-min-value"  name="c" id="min_price" value="">
                        <label id="max_price_display" for="max_price"></label>
                        <input type="hidden" class="mls-search-form-slider-max-value"  name="d" id="max_price" value="">
                        <div id="slider-price-range"></div>
                    </fieldset>
                </div>
                <div class="form-group  ">
                    <fieldset class="form-group mls-search-form-sqft-container">
                        <legend class="range_lable">Square Feet: &nbsp; </legend>
                        <label id="min_sqft_display" for="min_sqft"></label>
                        <input type="hidden" class="mls-search-form-slider-min-value"  name="s" id="min_sqft" value="">
                        <label id="max_sqft_display" for="max_sqft"></label>
                        <input type="hidden" class="mls-search-form-slider-max-value"  name="m" id="max_sqft" value="">
                        <div id="slider-sqft-range"></div>
                    </fieldset>

                </div>
                <div class="form-group ">
                    <fieldset class="form-group mls-search-form-year-built-container">
                        <legend class="range_lable">Year Built: &nbsp; </legend>
                        <label id="min_built_display" for="min_built"></label>
                        <input type="hidden" class="mls-search-form-slider-min-value"  name="y" id="min_built" value="">
                        <label id="max_built_display" for="max_built"></label>
                        <input type="hidden" class="mls-search-form-slider-max-value"  name="z" id="max_built" value="">
                        <div id="slider-yearbuilt-range"></div>
                    </fieldset>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <fieldset name="propertytype" class="form-group mls-search-form-price-container">
                        <legend class="range_lable">propertytype: &nbsp; </legend>

                        <select name="t" id="propertytype" class="form-control property-select-multiple2">
                            <option value="">All Property Types</option>
                            <option value="APTU">Condos</option>
                            <option value="HOUSE">Houses</option>
                            <option value="TWNHS">Townhouses</option>
                            <option value="LOFTS">Lofts</option>
                            <option value="LND">Land</option>

                        </select>
                    </fieldset>
                </div>
                <div class="form-group">
                    <fieldset name="price" class="form-group mls-search-form-price-container">
                        <legend class="range_lable">bedroom: &nbsp; </legend>
                        <label id="min_price_display" for="bedroom"></label>
                        <input type="hidden" class="mls-search-form-slider-min-value"  name="bedroom" id="bedroom" value="">
                        <div id="slider-bedrooms"></div>
                    </fieldset>
                </div>
                <div class="form-group ">

                    <fieldset name="bathroom" class="form-group mls-search-form-price-container">
                        <legend class="range_lable">bathroom: &nbsp; </legend>
                        <label  for="bathroom"></label>
                        <input type="hidden" class="mls-search-form-slider-value" name="bathroom" id="bathroom" value="" readonly>
                        <div id="slider-bathrooms"></div>
                    </fieldset>
                </div>


            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <legend class="range_lable"> &nbsp; </legend>

                <select name="status" id="mls_search_form_listing_status" class="form-control">
                    <option value="A" selected="selected">Active Listings</option>
                    <option value="S1" >Sold In Last Month</option>
                    <option value="S3" >Sold In Last 3 Months</option>
                    <option value="S6" >Sold In Last 6 Months</option>
                    <option value="S9" >Sold In Last 9 Months</option>
                    <option value="S12" >Sold In Last Year</option>
                    <option value="S24" >Sold In Last 2 Years</option>
                </select>
                </div>
                <div class="form-group ">
                    <fieldset name="bathroom" class="form-group mls-search-form-price-container">
                        <legend class="mls-search-form-label"> &nbsp; </legend>

                        <select class="form-control" id="subareacommunity" name="Area">
                            <option>Choose</option>
                        </select>
                        <p class="links_under_area_search"><a id="select_all_areas" class="mls-search-form-area-list-all hide">Select All Areas</a><span> or </span><a id="select_by_mlsnum" class="search_by_mls_link" href="#">Search by MLS&reg; Number</a></p>
                    </fieldset>


                </div>
            </div>

            </div>


        </div>

    </div>





@endsection

