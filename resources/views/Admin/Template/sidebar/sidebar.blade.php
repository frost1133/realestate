<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
	</a>
	
	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	
	<!-- Nav Item - Dashboard -->
	<li class="nav-item active">
		<a class="nav-link" href="index.html">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider">
	
	<!-- Heading -->
	<div class="sidebar-heading">
		Interface
	</div>
	
	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<i class="topnav-icons fa fa-home w3-left w3-bar-item w3-button"></i>
			<span>Houses</span>
		</a>
		
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header"> Home:</h6>
				<a class="collapse-item" href="{{route('admin.houses.index')}}">List Home</a>
				<a class="collapse-item" href="{{route('admin.houses.create')}}">Create</a>
			
			
			</div>
		</div>
	
	</li>
	
	<!-- Nav Item - Utilities Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
			<i class="fas fa-fw fa-wrench"></i>
			<span>Requests</span>
		</a>
		<div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">request:</h6>
				<a class="collapse-item" href="{{route('admin.ticket.index')}}">List Requests</a>
				<a class="collapse-item" href="{{route('admin.agent.index')}}">Agents</a>
			
			</div>
		</div>
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider">
	
	<!-- Heading -->
	<div class="sidebar-heading">
		Addons
	</div>
	
	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
			<i class="fas fa-fw fa-folder"></i>
			<span>Account</span>
		</a>
		<div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Login Screens:</h6>
				<a class="collapse-item" href="{{route('admin.account.index')}}">Admins</a>
				<a class="collapse-item" href="{{route('admin.user.index')}}">Users</a>
			
			</div>
		</div>
	</li>
	<hr class="sidebar-divider">
	
	<!-- Heading -->
	<div class="sidebar-heading">
		Blog
	</div>
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagess" aria-expanded="true" aria-controls="collapsePagess">
			<i class="fas fa-fw fa-folder"></i>
			<span>Blogs</span>
		</a>
		<div id="collapsePagess" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">index:</h6>
				<a class="collapse-item" href="{{route('admin.blog.articles.index')}}">Articles</a>
				<a class="collapse-item" href="{{route('admin.blog.comments.index')}}">Comments</a>
				<a class="collapse-item" href="{{route('admin.blog.categories.index')}}">Categories</a>
				{{--                <a class="collapse-item" href="{{route('admin.user.index')}}">Users</a>--}}
			</div>
		
		</div>
	</li>
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePage" aria-expanded="true" aria-controls="collapsePagess">
			<i class="fas fa-fw fa-folder"></i>
			<span>contants</span>
		</a>
		<div id="collapsePage" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a href="{{route('admin.contant.index')}}" class="collapse-item">Contact</a>
			</div>
		
		</div>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	
	<!-- Heading -->
	<div class="sidebar-heading">Site</div>
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Setting" aria-expanded="true" aria-controls="Setting">
			<i class="fas fa-fw fa-wrench"></i>
			<span>Setting</span>
		</a>
		<div id="Setting" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href="{{route('admin.options.about.view')}}">About</a>
				<a class="collapse-item" href="{{route('admin.options.form.buyer.view')}}">Buyer</a>
				<a class="collapse-item" href="{{route('admin.options.form.seller.view')}}">Seller</a>
				<a class="collapse-item" href="{{route('admin.contant.create')}}">Cantact</a>
				<a class="collapse-item" href="{{route('admin.options.general')}}">General</a>
				<a class="collapse-item" href="{{route('admin.options.home')}}">Home Page</a>
			</div>
		</div>
	</li>
	
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>
