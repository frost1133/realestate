<script src="/admin/vendor/jquery/jquery.min.js"></script>
<script src="/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/admin/js/sb-admin-2.min.js"></script>

<script>
    csrfToken = {
        tag: document.querySelector('meta[name="X-CSRF-TOKEN"]'),
        set: function (newCSRF) {
            this.tag.setAttribute('content', newCSRF)
        },
        get: function () {
            return this.tag.getAttribute('content')
        }
    }
</script>


