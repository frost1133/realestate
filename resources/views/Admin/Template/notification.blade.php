@if (session('success'))
	
	<div class="alert alert-success">
		<strong>Success!</strong>{{session('success')}}.
	</div>

@endif
@if (session('update'))
	<div class="alert alert-success">
		<strong>Success!</strong>{{session('update')}}.
	</div>

@endif

@if (session('delete'))
	<div class="alert alert-danger">
		<strong>Success!</strong>{{session('delete')}}.
	</div>
@endif

@if (session('error'))
	<div class="alert alert-danger">
		<strong>Error!</strong>{{session('error')}}.
	</div>
@endif

@if ($errors->any())
	<div class="alert alert-danger">
		<ul class="mb-0">
			@foreach($errors->all() as $error)
				<li><strong>Error!</strong>{{$error}}.</li>
			@endforeach
		</ul>
	</div>
@endif

