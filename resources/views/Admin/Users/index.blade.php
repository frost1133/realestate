@extends('Admin.Template.layout')
@section('content')
	@include('Admin.Template.notification')
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<a style="float: right" href="{{route('admin.account.create')}}">
				<button type="button" class="btn btn-success btn-sm ">create</button>
			</a>
			<h6 class="m-0 font-weight-bold text-primary">List Users</h6>
		</div>
		<div class="card-body">
			<form class="" action="{{request()->fullUrl()}}">
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="hidden" name="type" value="{{request('type')}}">
						<input type="text" class="form-control" placeholder="Name" name="name" value="{{request('name')}}">
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="email" class="form-control" placeholder="Email" name="email" value="{{request('email')}}">
					</div>
					<div class="col-sm-2 col-sn-12 col-xl-2">
						<button type="submit" class="btn btn-outline-primary">Apply</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover" border="5">
				<thead>
				<tr>
					<th style="text-align: center">Name</th>
					<th style="text-align: center">Email</th>
					<th style="text-align: center">Number tickets</th>
					<th>Settings</th>
				</tr>
				</thead>
				@foreach($users  as $user)
					<tr>
						<td style="text-align: center">{{$user->name}}</td>
						<td style="text-align: center">{{$user->email}}</td>
						<td style="text-align: center">{{$user->tickets->count()}}</td>
						<td>
							<div class="btn-group">
								<form action="{{route('admin.user.delete' , $user)}}" method="post">
									@csrf
									@method('delete')
									{{--                                <a href="{{route('admin.account.edit',$admin)}}" class="btn btn-warning btn-sm">Edit</a>--}}
									<button type="submit" class="btn btn-danger btn-sm">Delete</button>
									<a href="{{route('admin.user.show',$user)}}" target="_blank">
										<button type="button" class="btn btn-success btn-sm">ShowProfile</button>
									</a>
								</form>
							</div>
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>


@endsection
