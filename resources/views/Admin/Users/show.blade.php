@extends('Admin.Template.layout')
@section('css')
    <style>
        img.avatar {
            width: 100%;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="agent-container">

                <img class="avatar"
                     src="{{$user->avatar??'http://namehere.realtyninja.com/static/images/agents/med/1512073054_agent-placeholder-5.jpeg'}}">

                <div class="user-content">
                    <h4>{{$user->name}}</h4>

                    <p class="agent-phone">
                        <i class="fa fa-phone"></i> <a href="tel:604-123-4567">{{$user->phone}}</a>
                    </p>

                    <p class="agent-email">
                        <i class="fa fa-envelope"></i> <a href="mailto:linda@gmail.com">{{$user->email}}</a>
                    </p>
                    <p>
                        <i class="fa fa-envelope fa-5x fa-border icon-grey badge"></i>Number
                        ticket:{{$user->tickets->count()}}
                    </p>


                    <div class="clearfix"></div>
                </div>

            </div>

        </div>
        <div class="col-lg-9 col-md-8 col-sm-6 ">
            <h2>List tickets:</h2>
            <table class="table table-bordered table-hover" border="5">
                <thead>
                <tr>

                    <th style="text-align: center">ticket type</th>
                    <th style="text-align: center">state</th>
                    <th style="text-align: center">ticket date</th>

                </tr>
                </thead>

                @foreach($user->tickets as $ticket)
                    <tr>

                        <td style="text-align: center">{{$ticket->ticketType}}</td>
                        <td style="text-align: center">{{$ticket->state}}</td>
                        <td style="text-align: center">{{$ticket->created_at}}</td>


                    </tr>
                @endforeach

            </table>
        </div>
    </div>



@endsection
