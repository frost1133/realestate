@extends('Admin.Template.layout')
@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">

            <h6 class="m-0 font-weight-bold text-primary">List Contants</h6>
            <a style="float: right" href="{{route('admin.contant.create')}}">
                <button type="button" class="btn btn-success btn-sm">index</button>
            </a>
        </div>
        <div class="card-body">
            <form class="" action="{{request()->fullUrl()}}">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-3 col-sm-12 col-xl-2">
                        <input type="hidden" name="type" value="{{request('type')}}">
                        <input type="text" class="form-control" placeholder="Name" name="name"
                               value="{{request('name')}}">
                    </div>
                    <div class="col-md-3 col-sm-12 col-xl-2">
                        <input type="email" class="form-control" placeholder="Email" name="email"
                               value="{{request('email')}}">
                    </div>
                    <div class="col-sm-2 col-sn-12 col-xl-2">
                        <button type="submit" class="btn btn-outline-primary">Apply</button>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-hover" border="5">
                <thead>
                <tr>
                    <th style="text-align: center">Name</th>
                    <th style="text-align: center">Phone</th>
                    <th style="text-align: center">Email</th>
                    <th style="text-align: center">Settings</th>
                </tr>
                </thead>
                @foreach($contants  as $contant)
                    <tr>
                        <td style="text-align: center">{{$contant->name}}</td>
                        <td style="text-align: center">{{$contant->phone}}</td>
                        <td style="text-align: center">{{$contant->email}}</td>
                        <td style="text-align: center">
                            <button class="btn btn-success" type="button" onclick="setcontant('contant__{{$contant->id}}')">Show Message</button>
                           <a href="{{route('admin.contant.delete',$contant)}}">
                               <button class="btn btn-danger" type="submit">delete</button>

                           </a>
                        </td>
                    </tr >
                    <tr id="contant__{{$contant->id}}">
                        <td style="display: none" colspan="4">
                        <h4>Message:</h4>
                           <h5>{{$contant->message}}</h5>


                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div





@endsection
@section('js')
    <script>
        function setcontant(id) {
            if ($('#' + id).css('display') == 'none') {

                $('#' + id).show(function () {
                    $($('#' + id).children()[0]).slideDown()
                })
            } else {
                $($('#' + id).children()[0]).slideUp(function () {
                    $('#' + id).hide()
                })
            }

        }
    </script>
@endsection
