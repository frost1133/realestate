@extends('Admin.Template.layout')
@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="/admin/vendor/select2tree/select2totree.css">
	<style>
		.ck-editor__editable {
			min-height: 500px;
		}
	</style>
@endsection

@section('content')
	@include('Admin.Template.notification')
	<form action="{{route('admin.contant.store')}}" method="post" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="col-md-3">
				<div class="card shadow">
					<div class="card-header">About as</div>
					<div class="card-body">
					
						<div class="card">
							<img src="{{$data['headerImage']??'https://images.unsplash.com/source-404?fit=crop&fm=jpg&h=800&q=60&w=1200'}}" class="card-img-top" alt="..."  onclick="$('#HeaderImage').click()">
							<input type="file" name="headerImage" id="HeaderImage" class="form-control" hidden>
						</div>
					
					</div>
					<div class="card-body">
						<div class="card">
							<label for="metatitle">Meta Title:</label>
							<input type="text" name="metatitle" class="form-control">
						</div>
						<div class="card">
							<label for="MetaDescription">Meta Description:</label>
							<textarea name="discription" type="text" class="form-control"></textarea>
						</div>
						<div class="card">
							<label for="MetaKeywords">Meta Keywords</label>
							<select name="metaKeywords[]" id="MetaKeywords" class="form-control" multiple>
								@foreach(old('metaKeywords')??[] as $keyword)
									<option value="{{$keyword}}" selected>{{$keyword}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card shadow">
					<div class="card-body">
					
						<textarea name="body" id="body" hidden >{{$data['body']}}</textarea><br>
						
						<button class="btn btn-primary" type="submit" style="float: right">send</button>
					
					</div>
				
				</div>
			
			</div>
		</div>
	</form>
	


@endsection
@section('js')
	<script src="https://cdn.tiny.cloud/1/njaj317q8zpesyb5je3znfi3pdzsfcyn11g2c7v8id8dm7df/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
        tinymce.init({
            selector: '#body',
            plugins: 'print preview importcss  searchreplace autolink directionality image visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            mobile: {
                plugins: 'print preview importcss tinydrive searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount textpattern noneditable help charmap quickbars emoticons'
            },
            menu: {
                tc: {
                    title: 'TinyComments',
                    items: 'addcomment showcomments deleteallconversations'
                }
            },
            menubar: 'file edit view insert format tools table tc help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | insertfile image media template link anchor codesample |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | a11ycheck ltr rtl | showcomments addcomment',
            image_advtab: true,
            importcss_append: true,
            templates: [
                {
                    title: 'New Table',
                    description: 'creates a new table',
                    content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
                },
                {
                    title: 'Starting my story',
                    description: 'A cure for writers block',
                    content: 'Once upon a time...'
                },
                {
                    title: 'New list with dates',
                    description: 'New List with dates',
                    content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
                }
            ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            spellchecker_whitelist: ['Ephox', 'Moxiecode'],
            content_style: '.mymention{ color: gray; }',
            contextmenu: 'link image imagetools table configurepermanentpen',
            a11y_advanced_options: true,
            images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;

                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '{{route('admin.options.upImage')}}');

                xhr.upload.onprogress = function (e) {
                    progress(e.loaded / e.total * 100);
                };
                xhr.setRequestHeader("X-CSRF-TOKEN" , csrfToken.get());
                xhr.onload = function () {
                    var json;

                    if (xhr.status < 200 || xhr.status >= 300) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }

                    json = JSON.parse(xhr.responseText);
                    csrfToken.set(json.csrfToken)

                    if (!json || typeof json.image != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    csrfToken.set(json["csrf-token"]);
                    success(json.image);
                };

                xhr.onerror = function () {
                    failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
                };

                formData = new FormData();
                formData.append('image', blobInfo.blob(), blobInfo.filename());
                formData.append('directory','PageContact');

                xhr.send(formData);
            },
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
	</script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script src="/admin/vendor/select2tree/select2totree.js"></script>
	<script>

        $(document).ready(function () {
            $('#MetaKeywords').select2({tags: true, tokenSeparators: ',',});
        })
	
	</script>
	
	<script>
        let HeaderImage = new FileReader();
        $(document).ready(function () {

            $('#HeaderImage').change(function () {
                if (this.files && this.files[0]) {
                    HeaderImage.readAsDataURL(document.getElementById('HeaderImage').files[0]);
                    HeaderImage.onload = e => {
                        $('#HeaderImage').prev().attr('src', e.target.result);
                    }
                }
            });
        })
	
	</script>

@endsection