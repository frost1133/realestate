@extends('Admin.Template.layout')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create Admin 1</h6>
        </div>
        <div class="card-body">
            <form action="{{route('admin.account.store')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">title:</label>
                            <input type="text" id="title" name="title" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label for="email">body:</label>
                            <textarea name="body" id="body"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="text" id="password" name="password" class="form-control">
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">Send</button>
                        </div>
                    </div>
                    @php
                        $oldPermissions = old('permissions')== NULL?[]:old('permissions');
                    @endphp
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="permissions">Permissions:</label>
                            <select class="form-control" name="permissions[]" id="permissions" multiple>
                                @foreach($permissions as $keyPermission => $permission)
                                <option value="{{$keyPermission}}" {{in_array($keyPermission,$oldPermissions)? 'selected' : NULL}}>{{$permission}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
