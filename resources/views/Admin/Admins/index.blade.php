@extends('Admin.Template.layout')
@section('content')
	
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			
			<a style="float: right" href="{{route('admin.account.create')}}">
				<button type="button" class="btn btn-success btn-sm ">create</button>
			</a>
			<h6 class="m-0 font-weight-bold text-primary">List Admins</h6>
		</div>
		<div class="card-body">
			<form class="" action="{{request()->fullUrl()}}">
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="hidden" name="type" value="{{request('type')}}">
						<input type="text" class="form-control" placeholder="Name" name="name" value="{{request('name')}}">
					</div>
					<div class="col-md-3 col-sm-12 col-xl-2">
						<input type="email" class="form-control" placeholder="Email" name="email" value="{{request('email')}}">
					</div>
					<div class="col-md-4 col-sm-12 col-xl-3">
						<select class="form-control" name="permissions[]" id="permissions" multiple="">
							<option value="1" {{request('permissions')? (in_array(1, request('permissions'))?'selected' : NULL):NULL }}>
								Admin1
							</option>
							<option value="2" {{request('permissions')? (in_array(2, request('permissions'))?'selected' : NULL):NULL}}>
								Admin2
							</option>
							<option value="3" {{request('permissions')? (in_array(3, request('permissions'))?'selected' : NULL):NULL}}>
								Admin3
							</option>
							<option value="4" {{request('permissions')? (in_array(4, request('permissions'))?'selected' : NULL):NULL}}>
								Admin4
							</option>
							<option value="5" {{request('permissions')? (in_array(5, request('permissions'))?'selected' : NULL):NULL}}>
								Admin5
							</option>
						</select>
					</div>
					<div class="col-sm-2 col-sn-12 col-xl-2">
						<button type="submit" class="btn btn-outline-primary">Apply</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover" border="5">
				<thead>
				<tr>
					<th style="text-align: center">Id</th>
					<th style="text-align: center">Name</th>
					<th style="text-align: center">Email</th>
					<th>Settings</th>
				</tr>
				</thead>
				<tbody>
				@foreach($admins  as $admin)
					<tr>
						<td style="text-align: center">{{$admin->id}}</td>
						<td style="text-align: center">{{$admin->name}}</td>
						<td style="text-align: center">{{$admin->email}}</td>
						<td>
							<div class="btn-group">
								<form action="{{route('admin.account.delete' , $admin)}}" method="post">
									@csrf
									@method('delete')
									<a href="{{route('admin.account.edit',$admin)}}" class="btn btn-warning btn-sm">Edit</a>
									<button type="submit" class="btn btn-danger btn-sm">Delete</button>
									<a href="{{route('admin.account.show',$admin)}}" target="_blank">
										<button type="button" class="btn btn-success btn-sm">ShowProfile</button>
									</a>
								</form>
							</div>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection
@section('css')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('js')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<script>
        $(document).ready(function () {
            $("#permissions").select2({
                placeholder: 'Select Permission',
                allowClear: true
            })
        });
	</script>
@endsection
