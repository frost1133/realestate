@extends('Admin.Template.layout')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Admin</h6>
        </div>
        <div class="card-body">
            <form action="{{route('admin.account.update' , $admin)}}" method="post">
                @csrf
                @method('patch')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{$admin->name}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" id="email" name="email" class="form-control" value="{{$admin->email}}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success">Send</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="permissions">Permissions:</label>
                            <select class="form-control" name="permissions[]" id="permissions" multiple>
                                @foreach($permissions as $keyPermissions => $permission)
                                <option value="{{$keyPermissions}}" {{in_array($keyPermissions ,$admin->PermissionsArray )? 'selected' : "" }} >{{$permission}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

