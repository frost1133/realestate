@extends('Admin.Template.layout')
@section('content')

    <div class="container-fluid">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Creating Dynamic Tabs in Bootstrap 4 via Data Attributes</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            .bs-example {
                margin: 20px;
            }
        </style>


        <div class="col-md-2">

            <select class="form-control" name="type">
                <option value="S">For Sale</option>
                <option value="R">For Rent</option>
            </select>
        </div>
        <div class="bs-example">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#home" class="nav-link active " data-toggle="tab">level-1</a>
                </li>
                <li class="nav-item">
                    <a href="#messages" class="nav-link" data-toggle="tab">level-2</a>
                </li>
                <li class="nav-item">
                    <a href="#level" class="nav-link " data-toggle="tab">level-3</a>
                </li>
                <li class="nav-item">
                    <a href="#asas" class="nav-link" data-toggle="tab">level-4</a>
                </li>
            </ul>

            <form action="{{route('admin.house.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="home">
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="half_width">
                                    <div class="rent_only"
                                         style="width:100%; margin-right:5px; float:left; display: none">
                                        <label>Rental ID</label>
                                        <input type="Text" name="RentalID" class="form-control">
                                    </div>
                                    <div class="sale_only" style="width:100%; margin-right:5px; float:left;">
                                        <label>MLS®Number</label>
                                        <input type="Text" name="MLSNumber" class="form-control">
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="half_width last" style="margin-bottom: 23px;">
                                    <label>Area</label>
                                    <select class="form-control" id="subareacommunity" name="Area">
                                        <option value="">Choose</option>
                                        <optgroup label="British Columbia">
                                            <option value="100_mile_house">100 Mile House</option>
                                            <option value="100_mile_house_zone_10">100 Mile House (zone 10)</option>
                                            <option value="100_mile_house_rural">100 Mile House Rural</option>
                                            <option value="100_mile_house_town">100 Mile House Town</option>
                                            <option value="100_mile_house_west">100 Mile House West</option>
                                            <option value="103_mile_house">103 Mile House</option>
                                            <option value="108_mile_ranch">108 Mile Ranch</option>
                                            <option value="150_mile_house">150 Mile House</option>
                                            <option value="70_mile_house">70 Mile House</option>
                                            <option value="tr">;tr</option>
                                            <option value="abbotsford">Abbotsford</option>
                                            <option value="aberdeen">Aberdeen</option>
                                            <option value="adams_lake">Adams Lake</option>
                                            <option value="agassiz">Agassiz</option>
                                            <option value="ainsworth">Ainsworth</option>
                                            <option value="ainsworth_hot_springs">Ainsworth Hot Springs</option>
                                            <option value="alert_bay">Alert Bay</option>
                                            <option value="alexis_park">Alexis Park</option>
                                            <option value="allison_lake">Allison Lake</option>
                                            <option value="alpine_resort_area">Alpine Resort Area</option>
                                            <option value="amherst">Amherst</option>
                                            <option value="amstrong">Amstrong</option>
                                            <option value="anglemont">Anglemont</option>
                                            <option value="anglemont_bc">Anglemont Bc</option>
                                            <option value="anglemot">Anglemot</option>
                                            <option value="anglmeont">Anglmeont</option>
                                            <option value="anmore">Anmore</option>
                                            <option value="anstey_arm_bc">Anstey Arm Bc</option>
                                            <option value="anstey_bc">Anstey Bc</option>
                                            <option value="apex">Apex</option>
                                            <option value="apex_penticton">Apex (penticton)</option>
                                            <option value="apex_mountain">Apex Mountain</option>
                                            <option value="appledale">Appledale</option>
                                            <option value="ardoise">Ardoise</option>
                                            <option value="argenta">Argenta</option>
                                            <option value="armstong">Armstong</option>
                                            <option value="armstromg">Armstromg</option>
                                            <option value="armstrong">Armstrong</option>
                                            <option value="armstrong_bc">Armstrong Bc</option>
                                            <option value="armstrong_spallumcheen">Armstrong Spallumcheen</option>
                                            <option value="arrow_creek">Arrow Creek</option>
                                            <option value="arrow_creek_to_yahk">Arrow Creek To Yahk</option>
                                            <option value="arrow_park">Arrow Park</option>
                                            <option value="ashcroft">Ashcroft</option>
                                            <option value="ashton_creek">Ashton Creek</option>
                                            <option value="balaklava_island">Balaklava Island</option>
                                            <option value="balfour">Balfour</option>
                                            <option value="balfour_to_kaslo_west">Balfour To Kaslo West</option>
                                            <option value="bamfield">Bamfield</option>
                                            <option value="bankeir">Bankeir</option>
                                            <option value="barriere">Barriere</option>
                                            <option value="baynes_lake">Baynes Lake</option>
                                            <option value="bc_drive">Bc Drive</option>
                                            <option value="bcr_indust">Bcr Indust</option>
                                            <option value="bear_lake">Bear Lake</option>
                                            <option value="beasley">Beasley</option>
                                            <option value="beaver_falls">Beaver Falls</option>
                                            <option value="beaverdell">Beaverdell</option>
                                            <option value="beaverdell_carmi">Beaverdell Carmi</option>
                                            <option value="beaverley">Beaverley</option>
                                            <option value="bednesti">Bednesti</option>
                                            <option value="belcarra">Belcarra</option>
                                            <option value="bella_coola">Bella Coola</option>
                                            <option value="bella_vista">Bella Vista</option>
                                            <option value="beverdell">Beverdell</option>
                                            <option value="big_white">Big White</option>
                                            <option value="big_wnite">Big Wnite</option>
                                            <option value="birchwood">Birchwood</option>
                                            <option value="birken">Birken</option>
                                            <option value="black_creek">Black Creek</option>
                                            <option value="black_mountain">Black Mountain</option>
                                            <option value="blackwater">Blackwater</option>
                                            <option value="blaeberry">Blaeberry</option>
                                            <option value="bligh_island">Bligh Island</option>
                                            <option value="blind_bay">Blind Bay</option>
                                            <option value="blind_bay_bc">Blind Bay Bc</option>
                                            <option value="blish_island">Blish Island</option>
                                            <option value="blue_river">Blue River</option>
                                            <option value="bonnington">Bonnington</option>
                                            <option value="bonnington_falls">Bonnington Falls</option>
                                            <option value="boston_bar">Boston Bar</option>
                                            <option value="boston_bar_lytton">Boston Bar Lytton</option>
                                            <option value="boswell">Boswell</option>
                                            <option value="bouchie_lake">Bouchie Lake</option>
                                            <option value="bow_island">Bow Island</option>
                                            <option value="bowen_island">Bowen Island</option>
                                            <option value="bowser">Bowser</option>
                                            <option value="brackendale">Brackendale</option>
                                            <option value="bridesville">Bridesville</option>
                                            <option value="brideville">Brideville</option>
                                            <option value="bridge_lake">Bridge Lake</option>
                                            <option value="brisco">Brisco</option>
                                            <option value="britannia_beach">Britannia Beach</option>
                                            <option value="brooklyn">Brooklyn</option>
                                            <option value="buckhorn">Buckhorn</option>
                                            <option value="bull_river">Bull River</option>
                                            <option value="burnaby">Burnaby</option>
                                            <option value="burns_lake">Burns Lake</option>
                                            <option value="burns_lake_zone_55">Burns Lake (zone 55)</option>
                                            <option value="burns_lake_rural_west">Burns Lake Rural West</option>
                                            <option value="burnslk_e">Burnslk E</option>
                                            <option value="burton">Burton</option>
                                            <option value="busby_island">Busby Island</option>
                                            <option value="bute_inlet">Bute Inlet</option>
                                            <option value="cache_creek">Cache Creek</option>
                                            <option value="cadreb_other">Cadreb Other</option>
                                            <option value="campbell_river">Campbell River</option>
                                            <option value="campbell_river_p">Campbell River P</option>
                                            <option value="canal_flats">Canal Flats</option>
                                            <option value="canim_lake">Canim Lake</option>
                                            <option value="canoe">Canoe</option>
                                            <option value="canoe_bc">Canoe Bc</option>
                                            <option value="canyon">Canyon</option>
                                            <option value="carmi">Carmi</option>
                                            <option value="cascade">Cascade</option>
                                            <option value="castlegar">Castlegar</option>
                                            <option value="cawston">Cawston</option>
                                            <option value="celista">Celista</option>
                                            <option value="central">Central</option>
                                            <option value="central_okanagan_rural">Central Okanagan Rural</option>
                                            <option value="central_saanich">Central Saanich</option>
                                            <option value="chantham_channel">Chantham Channel</option>
                                            <option value="charella_starlane">Charella Starlane</option>
                                            <option value="charlie_lake">Charlie Lake</option>
                                            <option value="chase">Chase</option>
                                            <option value="chatham_channel">Chatham Channel</option>
                                            <option value="chemainus">Chemainus</option>
                                            <option value="cherryville">Cherryville</option>
                                            <option value="cherryville_bc">Cherryville Bc</option>
                                            <option value="chetwnyd">Chetwnyd</option>
                                            <option value="chetwtynd">Chetwtynd</option>
                                            <option value="chetwynd">Chetwynd</option>
                                            <option value="chetwynd_rural">Chetwynd Rural</option>
                                            <option value="chetywnd">Chetywnd</option>
                                            <option value="chewynd">Chewynd</option>
                                            <option value="chilliwack">Chilliwack</option>
                                            <option value="christina_lake">Christina Lake</option>
                                            <option value="circle_island">Circle Island</option>
                                            <option value="clearwater">Clearwater</option>
                                            <option value="clinton">Clinton</option>
                                            <option value="cloverdale">Cloverdale</option>
                                            <option value="cluculz_lake">Cluculz Lake</option>
                                            <option value="coal_harbour">Coal Harbour</option>
                                            <option value="coalmont">Coalmont</option>
                                            <option value="coalmont_tulameen">Coalmont Tulameen</option>
                                            <option value="cobble_hill">Cobble Hill</option>
                                            <option value="coldsream">Coldsream</option>
                                            <option value="coldstream">Coldstream</option>
                                            <option value="coldstream_bc">Coldstream Bc</option>
                                            <option value="columbia_lake">Columbia Lake</option>
                                            <option value="columbia_valley">Columbia Valley</option>
                                            <option value="colwood">Colwood</option>
                                            <option value="comox">Comox</option>
                                            <option value="coombs">Coombs</option>
                                            <option value="cooper_creek">Cooper Creek</option>
                                            <option value="coquihalla_connector">Coquihalla Connector</option>
                                            <option value="coquitlam">Coquitlam</option>
                                            <option value="cortes_island">Cortes Island</option>
                                            <option value="costa_rica_acres">Costa Rica Acres</option>
                                            <option value="coulter_island">Coulter Island</option>
                                            <option value="courtenay">Courtenay</option>
                                            <option value="courtenay_east">Courtenay East</option>
                                            <option value="cove_pitt_lake">Cove Pitt Lake</option>
                                            <option value="cowichan_bay">Cowichan Bay</option>
                                            <option value="cracroft_island">Cracroft Island</option>
                                            <option value="craigellachie">Craigellachie</option>
                                            <option value="cranbrook">Cranbrook</option>
                                            <option value="cranbrook_north">Cranbrook North</option>
                                            <option value="cranbrook_periphery">Cranbrook Periphery</option>
                                            <option value="cranbrook_south">Cranbrook South</option>
                                            <option value="crawford_bay">Crawford Bay</option>
                                            <option value="crawford_bay_riondel">Crawford Bay Riondel</option>
                                            <option value="crescent_valley">Crescent Valley</option>
                                            <option value="creston">Creston</option>
                                            <option value="creston_flats">Creston Flats</option>
                                            <option value="crofton">Crofton</option>
                                            <option value="cultus_lake">Cultus Lake</option>
                                            <option value="cumberland">Cumberland</option>
                                            <option value="darcy">D?arcy</option>
                                            <option value="dawon_creek">Dawon Creek</option>
                                            <option value="dawsin_creek">Dawsin Creek</option>
                                            <option value="dawson_creek">Dawson Creek</option>
                                            <option value="dawson_creek_bc">Dawson Creek Bc</option>
                                            <option value="dawson_creek_rural">Dawson Creek Rural</option>
                                            <option value="dawson_crek">Dawson Crek</option>
                                            <option value="decourcy_island">Decourcy Island</option>
                                            <option value="deep_bay">Deep Bay</option>
                                            <option value="deer_park">Deer Park</option>
                                            <option value="deka_lake_sulphurous_hathaway_lakes">Deka Lake Sulphurous
                                                Hathaway Lakes
                                            </option>
                                            <option value="delta">Delta</option>
                                            <option value="denman_island">Denman Island</option>
                                            <option value="dent_island">Dent Island</option>
                                            <option value="deroche">Deroche</option>
                                            <option value="destiny_bay">Destiny Bay</option>
                                            <option value="dist_of_lake_country">Dist Of Lake Country</option>
                                            <option value="district_of_west_kelowna">District Of West Kelowna</option>
                                            <option value="dome_creek">Dome Creek</option>
                                            <option value="downtown">Downtown</option>
                                            <option value="drive">Drive</option>
                                            <option value="dry_gulch">Dry Gulch</option>
                                            <option value="dry_gulch_to_radium">Dry Gulch To Radium</option>
                                            <option value="duncan">Duncan</option>
                                            <option value="dutch_creek">Dutch Creek</option>
                                            <option value="eagle_bay">Eagle Bay</option>
                                            <option value="eagle_bay_bc">Eagle Bay Bc</option>
                                            <option value="east_cracroft_island">East Cracroft Island</option>
                                            <option value="east_highway_1">East Highway 1</option>
                                            <option value="east_shore">East Shore</option>
                                            <option value="eastgate">Eastgate</option>
                                            <option value="edgewater">Edgewater</option>
                                            <option value="edgewater_north">Edgewater North</option>
                                            <option value="edgewood">Edgewood</option>
                                            <option value="egmont">Egmont</option>
                                            <option value="elk_valley">Elk Valley</option>
                                            <option value="elk_valley_rural">Elk Valley Rural</option>
                                            <option value="elkford">Elkford</option>
                                            <option value="elko">Elko</option>
                                            <option value="elko_to_montana">Elko To Montana</option>
                                            <option value="ellison">Ellison</option>
                                            <option value="elmwood">Elmwood</option>
                                            <option value="emerald">Emerald</option>
                                            <option value="enderby">Enderby</option>
                                            <option value="enderby_bc">Enderby Bc</option>
                                            <option value="endery">Endery</option>
                                            <option value="erickson">Erickson</option>
                                            <option value="errington">Errington</option>
                                            <option value="erris">Erris</option>
                                            <option value="esler_dog_creek">Esler Dog Creek</option>
                                            <option value="esquimalt">Esquimalt</option>
                                            <option value="fairmont_columbia_lake">Fairmont Columbia Lake</option>
                                            <option value="fairmont_hot_spg">Fairmont Hot Spg</option>
                                            <option value="fairmont_hot_spr">Fairmont Hot Spr</option>
                                            <option value="fairmont_hot_springs">Fairmont Hot Springs</option>
                                            <option value="fairview_to_paulson">Fairview To Paulson</option>
                                            <option value="falkland">Falkland</option>
                                            <option value="falkland_bc">Falkland Bc</option>
                                            <option value="fanny_bay">Fanny Bay</option>
                                            <option value="farmington">Farmington</option>
                                            <option value="faulder">Faulder</option>
                                            <option value="fauquier">Fauquier</option>
                                            <option value="ferguson">Ferguson</option>
                                            <option value="fernie">Fernie</option>
                                            <option value="field">Field</option>
                                            <option value="fintry">Fintry</option>
                                            <option value="fintry_bc">Fintry Bc</option>
                                            <option value="five_coves">Five Coves</option>
                                            <option value="fletcher_creek">Fletcher Creek</option>
                                            <option value="foothills">Foothills</option>
                                            <option value="forest_grove">Forest Grove</option>
                                            <option value="fort_fraser">Fort Fraser</option>
                                            <option value="fort_nelson">Fort Nelson</option>
                                            <option value="fort_nelson_zone_64">Fort Nelson (zone 64)</option>
                                            <option value="fort_nelson_town">Fort Nelson Town</option>
                                            <option value="fort_st_james">Fort St James</option>
                                            <option value="fort_st_james_zone_57">Fort St James (zone 57)</option>
                                            <option value="fort_st_john">Fort St John</option>
                                            <option value="fort_st_john_zone_60">Fort St John (zone 60)</option>
                                            <option value="fort_st_john_city_ne">Fort St John City Ne</option>
                                            <option value="fort_st_john_city_nw">Fort St John City Nw</option>
                                            <option value="fort_st_john_city_se">Fort St John City Se</option>
                                            <option value="fort_st_john_city_sw">Fort St John City Sw</option>
                                            <option value="fort_st_john_rural_w_100th">Fort St John Rural W 100th
                                            </option>
                                            <option value="fort_steele">Fort Steele</option>
                                            <option value="fraser_lake">Fraser Lake</option>
                                            <option value="french_creek">French Creek</option>
                                            <option value="fruitvale">Fruitvale</option>
                                            <option value="fruitvale_rural">Fruitvale Rural</option>
                                            <option value="ft_steele_to_wardner">Ft Steele To Wardner</option>
                                            <option value="furry_creek">Furry Creek</option>
                                            <option value="gabriola">Gabriola</option>
                                            <option value="gabriola_island">Gabriola Island</option>
                                            <option value="galena_bay">Galena Bay</option>
                                            <option value="galiano_island">Galiano Island 9</option>
                                            <option value="gambier_island">Gambier Island</option>
                                            <option value="garden_bay">Garden Bay</option>
                                            <option value="garibaldi_highlands">Garibaldi Highlands</option>
                                            <option value="genelle">Genelle</option>
                                            <option value="gibsons">Gibsons</option>
                                            <option value="gilford_island">Gilford Island</option>
                                            <option value="giscome">Giscome</option>
                                            <option value="glade">Glade</option>
                                            <option value="gold_river">Gold River</option>
                                            <option value="golden">Golden</option>
                                            <option value="grand_forks">Grand Forks</option>
                                            <option value="grand_forks_rural">Grand Forks Rural</option>
                                            <option value="grand_forks_rural_west">Grand Forks Rural West</option>
                                            <option value="granisle">Granisle</option>
                                            <option value="granthams_landing">Granthams Landing</option>
                                            <option value="grasmere">Grasmere</option>
                                            <option value="gray_creek">Gray Creek</option>
                                            <option value="greenwood">Greenwood</option>
                                            <option value="grindrod">Grindrod</option>
                                            <option value="groundbirch">Groundbirch</option>
                                            <option value="gulf_islands">Gulf Islands</option>
                                            <option value="haldi">Haldi</option>
                                            <option value="halfmoon_bay">Halfmoon Bay</option>
                                            <option value="halifax">Halifax</option>
                                            <option value="harrison_hot_springs">Harrison Hot Springs</option>
                                            <option value="harrison_mills">Harrison Mills</option>
                                            <option value="harrogate">Harrogate</option>
                                            <option value="harrop">Harrop</option>
                                            <option value="harrop_procter">Harrop Procter</option>
                                            <option value="hart_highlands">Hart Highlands</option>
                                            <option value="hart_highway">Hart Highway</option>
                                            <option value="harwood">Harwood</option>
                                            <option value="hazelton">Hazelton</option>
                                            <option value="hedley">Hedley</option>
                                            <option value="heffley">Heffley</option>
                                            <option value="heritage">Heritage</option>
                                            <option value="highland_park">Highland Park</option>
                                            <option value="highlands">Highlands</option>
                                            <option value="hilliers">Hilliers</option>
                                            <option value="hills">Hills</option>
                                            <option value="hixon">Hixon</option>
                                            <option value="honeymoon_bay">Honeymoon Bay</option>
                                            <option value="hope">Hope</option>
                                            <option value="hornby_island">Hornby Island</option>
                                            <option value="horse_lake">Horse Lake</option>
                                            <option value="horsefly">Horsefly</option>
                                            <option value="hosmer">Hosmer</option>
                                            <option value="houston">Houston</option>
                                            <option value="houston_zone">Houston (zone 53)</option>
                                            <option value="houston_zone_53">Houston (zone 53)</option>
                                            <option value="houston_town">Houston Town</option>
                                            <option value="howser">Howser</option>
                                            <option value="hudsons_hope">Hudsons Hope</option>
                                            <option value="hyde_creek">Hyde Creek</option>
                                            <option value="idabel_lake">Idabel Lake</option>
                                            <option value="invermere">Invermere</option>
                                            <option value="invermere_rural">Invermere Rural</option>
                                            <option value="isle_pierre">Isle Pierre</option>
                                            <option value="jaffray">Jaffray</option>
                                            <option value="jaffray_and_vicinity">Jaffray And Vicinity</option>
                                            <option value="jenkins_island">Jenkins Island</option>
                                            <option value="jewel_lake">Jewel Lake</option>
                                            <option value="joe_rich">Joe Rich</option>
                                            <option value="joe_riche">Joe Riche</option>
                                            <option value="johnsons_landing">Johnsons Landing</option>
                                            <option value="johnsonslanding">Johnsonslanding</option>
                                            <option value="kaleden">Kaleden</option>
                                            <option value="kaleden_bc">Kaleden Bc</option>
                                            <option value="kaleden_okanagan_falls">Kaleden Okanagan Falls</option>
                                            <option value="kalowna">Kalowna</option>
                                            <option value="kamloops">Kamloops</option>
                                            <option value="kaslo">Kaslo</option>
                                            <option value="kaslo_north_to_gerrard">Kaslo North To Gerrard</option>
                                            <option value="keats_island">Keats Island</option>
                                            <option value="kelowa">Kelowa</option>
                                            <option value="kelowan">Kelowan</option>
                                            <option value="kelown">Kelown</option>
                                            <option value="kelowna">Kelowna</option>
                                            <option value="kelowna_bc">Kelowna Bc</option>
                                            <option value="kelsey_bay">Kelsey Bay</option>
                                            <option value="keowna">Keowna</option>
                                            <option value="keremeos">Keremeos</option>
                                            <option value="keremeos_bc">Keremeos Bc</option>
                                            <option value="keremeos_olalla">Keremeos Olalla</option>
                                            <option value="kimberley">Kimberley</option>
                                            <option value="kimberley_rural">Kimberley Rural</option>
                                            <option value="kingsgate">Kingsgate</option>
                                            <option value="kitchener">Kitchener</option>
                                            <option value="kitimat">Kitimat</option>
                                            <option value="kitimat_zone_89">Kitimat (zone 89)</option>
                                            <option value="kitimat_zone">Kitimat (zone 89)</option>
                                            <option value="kitwanga">Kitwanga</option>
                                            <option value="kokanee_creek_to_balfour">Kokanee Creek To Balfour</option>
                                            <option value="kootenay_bay">Kootenay Bay</option>
                                            <option value="kootenay_boundary_e">Kootenay Boundary E</option>
                                            <option value="krestova">Krestova</option>
                                            <option value="kuskanook">Kuskanook</option>
                                            <option value="lac_la_hache">Lac La Hache</option>
                                            <option value="ladner">Ladner</option>
                                            <option value="ladysmith">Ladysmith</option>
                                            <option value="lafreniere">Lafreniere</option>
                                            <option value="laidlaw">Laidlaw</option>
                                            <option value="lake_country">Lake Country</option>
                                            <option value="lake_country_bc">Lake Country Bc</option>
                                            <option value="lake_cowichan">Lake Cowichan</option>
                                            <option value="lake_koocanusa">Lake Koocanusa</option>
                                            <option value="lakecountry">Lakecountry</option>
                                            <option value="lakelse_lake">Lakelse Lake</option>
                                            <option value="lakes_wasa_lazy_premier">Lakes Wasa Lazy Premier</option>
                                            <option value="lakeside_rural">Lakeside Rural</option>
                                            <option value="lakeview">Lakeview</option>
                                            <option value="lakeview_heights">Lakeview Heights</option>
                                            <option value="lakewood">Lakewood</option>
                                            <option value="langdale">Langdale</option>
                                            <option value="langford">Langford</option>
                                            <option value="langley">Langley</option>
                                            <option value="lantzville">Lantzville</option>
                                            <option value="lanztville">Lanztville</option>
                                            <option value="lardeau">Lardeau</option>
                                            <option value="lasqueti">Lasqueti</option>
                                            <option value="lasqueti_island">Lasqueti Island</option>
                                            <option value="lavington">Lavington</option>
                                            <option value="lee_creek">Lee Creek</option>
                                            <option value="lemon_creek">Lemon Creek</option>
                                            <option value="likely">Likely</option>
                                            <option value="lillooet">Lillooet</option>
                                            <option value="lindell_beach">Lindell Beach</option>
                                            <option value="lions_bay">Lions Bay</option>
                                            <option value="lister">Lister</option>
                                            <option value="little_fort">Little Fort</option>
                                            <option value="logan_lake">Logan Lake</option>
                                            <option value="lone_butte">Lone Butte</option>
                                            <option value="loon_lake">Loon Lake</option>
                                            <option value="lot_12_plan_25491_dlot_2450s">Lot 12 Plan 25491 Dlot 2450s
                                            </option>
                                            <option value="louise_creek">Louise Creek</option>
                                            <option value="lower_college">Lower College</option>
                                            <option value="lower_mud">Lower Mud</option>
                                            <option value="lumby">Lumby</option>
                                            <option value="lumby_bc">Lumby Bc</option>
                                            <option value="lunenburg">Lunenburg</option>
                                            <option value="lytton">Lytton</option>
                                            <option value="mabel_lake">Mabel Lake</option>
                                            <option value="mable_lake">Mable Lake</option>
                                            <option value="macken_rur">Macken Rur</option>
                                            <option value="mackenzie">Mackenzie</option>
                                            <option value="mackenzie_zone_69">Mackenzie (zone 69)</option>
                                            <option value="mackenzie_rural">Mackenzie Rural</option>
                                            <option value="madeira_park">Madeira Park</option>
                                            <option value="magan_bay">Magan Bay</option>
                                            <option value="magna_bay">Magna Bay</option>
                                            <option value="malahat">Malahat</option>
                                            <option value="malahat_area">Malahat &amp; Area</option>
                                            <option value="malakwa">Malakwa</option>
                                            <option value="malcolm_island">Malcolm Island</option>
                                            <option value="malei_island">Malei Island</option>
                                            <option value="manning_park">Manning Park</option>
                                            <option value="maple_ridge">Maple Ridge</option>
                                            <option value="mara">Mara</option>
                                            <option value="mara_lake">Mara Lake</option>
                                            <option value="marina_island">Marina Island</option>
                                            <option value="marysville">Marysville</option>
                                            <option value="masset">Masset</option>
                                            <option value="mayne_island">Mayne Island 6</option>
                                            <option value="mayook">Mayook</option>
                                            <option value="mcbride">Mcbride</option>
                                            <option value="mcleese_lake">Mcleese Lake</option>
                                            <option value="mcleod_lake">Mcleod Lake</option>
                                            <option value="mclure_vinsula">Mclure Vinsula</option>
                                            <option value="meadow_creek">Meadow Creek</option>
                                            <option value="merritt">Merritt</option>
                                            <option value="merville">Merville</option>
                                            <option value="metchosin">Metchosin</option>
                                            <option value="midway">Midway</option>
                                            <option value="mill_bay">Mill Bay</option>
                                            <option value="millar_addition">Millar Addition</option>
                                            <option value="missezula_lake">Missezula Lake</option>
                                            <option value="mission">Mission</option>
                                            <option value="miworth">Miworth</option>
                                            <option value="moberly_lake">Moberly Lake</option>
                                            <option value="mobery_lake">Mobery Lake</option>
                                            <option value="monte_lake_westwold">Monte Lake Westwold</option>
                                            <option value="montrose">Montrose</option>
                                            <option value="mount_alder">Mount Alder</option>
                                            <option value="mount_currie">Mount Currie</option>
                                            <option value="moyie">Moyie</option>
                                            <option value="moyie_lake">Moyie Lake</option>
                                            <option value="mt_waddington">Mt Waddington</option>
                                            <option value="mt_washington">Mt Washington</option>
                                            <option value="mt_woodside">Mt Woodside</option>
                                            <option value="mudge_island">Mudge Island</option>
                                            <option value="n_delta">N Delta</option>
                                            <option value="n27li">N27li</option>
                                            <option value="nakusp">Nakusp</option>
                                            <option value="nakusp_rural">Nakusp Rural</option>
                                            <option value="nanaimo">Nanaimo</option>
                                            <option value="nanoose_bay">Nanoose Bay</option>
                                            <option value="naramata">Naramata</option>
                                            <option value="naramata_bc">Naramata Bc</option>
                                            <option value="naramta">Naramta</option>
                                            <option value="nechako_bench">Nechako Bench</option>
                                            <option value="nechako_ridge">Nechako Ridge</option>
                                            <option value="needles">Needles</option>
                                            <option value="nelson">Nelson</option>
                                            <option value="nelson_bc">Nelson Bc</option>
                                            <option value="nelson_island">Nelson Island</option>
                                            <option value="nelson_rural">Nelson Rural</option>
                                            <option value="nelson_south_salmo_rural">Nelson South Salmo Rural</option>
                                            <option value="nelson_west_south_slocan">Nelson West South Slocan</option>
                                            <option value="nelway">Nelway</option>
                                            <option value="ness_lake">Ness Lake</option>
                                            <option value="new_denver">New Denver</option>
                                            <option value="new_denver_area">New Denver Area</option>
                                            <option value="new_hazelton">New Hazelton</option>
                                            <option value="new_westminster">New Westminster</option>
                                            <option value="newgate">Newgate</option>
                                            <option value="nicholson">Nicholson</option>
                                            <option value="no_city_value">No City Value</option>
                                            <option value="none">None</option>
                                            <option value="nootka_island">Nootka Island</option>
                                            <option value="north_blac">North Blac</option>
                                            <option value="north_blackburn">North Blackburn</option>
                                            <option value="north_castlegar">North Castlegar</option>
                                            <option value="north_kelly">North Kelly</option>
                                            <option value="north_nelson_to_kokanee_creek">North Nelson To Kokanee
                                                Creek
                                            </option>
                                            <option value="north_okanagan">North Okanagan</option>
                                            <option value="north_rendezvous_island">North Rendezvous Island</option>
                                            <option value="north_saanich">North Saanich</option>
                                            <option value="north_surrey">North Surrey</option>
                                            <option value="north_vancouver">North Vancouver</option>
                                            <option value="northwest_cranbrook">Northwest Cranbrook</option>
                                            <option value="nukko_lake">Nukko Lake</option>
                                            <option value="oak_bay">Oak Bay</option>
                                            <option value="ok_falls">Ok Falls</option>
                                            <option value="okanagan_centre">Okanagan Centre</option>
                                            <option value="okanagan_fall">Okanagan Fall</option>
                                            <option value="okanagan_falls">Okanagan Falls</option>
                                            <option value="okanagan_falls_bc">Okanagan Falls Bc</option>
                                            <option value="okanagan_galls">Okanagan Galls</option>
                                            <option value="okanagan_landing">Okanagan Landing</option>
                                            <option value="okangan_falls">Okangan Falls</option>
                                            <option value="ol8iver">Ol8iver</option>
                                            <option value="olalla">Olalla</option>
                                            <option value="old_summit_lake_road">Old Summit Lake Road</option>
                                            <option value="oliber">Oliber</option>
                                            <option value="oliver">Oliver</option>
                                            <option value="oliver_bc">Oliver Bc</option>
                                            <option value="ootischenia">Ootischenia</option>
                                            <option value="ootishcenia">Ootishcenia</option>
                                            <option value="osoyoos">Osoyoos</option>
                                            <option value="osoyoos_bc">Osoyoos Bc</option>
                                            <option value="other_islands">Other Islands</option>
                                            <option value="out_of_area">Out Of Area</option>
                                            <option value="out_of_province_alberta">Out Of Province Alberta</option>
                                            <option value="out_of_town">Out Of Town</option>
                                            <option value="oyama">Oyama</option>
                                            <option value="oyama_bc">Oyama Bc</option>
                                            <option value="panorama">Panorama</option>
                                            <option value="parker_cove">Parker Cove</option>
                                            <option value="parker_cove_vernon">Parker Cove Vernon</option>
                                            <option value="parksville">Parksville</option>
                                            <option value="parksvillle">Parksvillle</option>
                                            <option value="parson">Parson</option>
                                            <option value="pass_creek">Pass Creek</option>
                                            <option value="pass_creek_valley">Pass Creek Valley</option>
                                            <option value="passmore">Passmore</option>
                                            <option value="passmore_winlaw_slocan">Passmore Winlaw Slocan</option>
                                            <option value="peachaland018">Peachaland018</option>
                                            <option value="peachland">Peachland</option>
                                            <option value="peachland_bc">Peachland Bc</option>
                                            <option value="pearse_island">Pearse Island</option>
                                            <option value="peden_hill">Peden Hill</option>
                                            <option value="pemberton">Pemberton</option>
                                            <option value="pender_harbour">Pender Harbour</option>
                                            <option value="pender_island">Pender Island</option>
                                            <option value="penticton">Penticton</option>
                                            <option value="penticton_apex">Penticton (apex)</option>
                                            <option value="penticton_bc">Penticton Bc</option>
                                            <option value="penticton_bc_apex">Penticton Bc Apex</option>
                                            <option value="perry">Perry</option>
                                            <option value="peticton">Peticton</option>
                                            <option value="pg_city_central">Pg City Central</option>
                                            <option value="pg_city_central_zone_72">Pg City Central (zone 72)</option>
                                            <option value="pg_city_north">Pg City North</option>
                                            <option value="pg_city_north_zone_73">Pg City North (zone 73)</option>
                                            <option value="pg_city_south_zone_74">Pg City South (zone 74)</option>
                                            <option value="pg_city_south_east_zone_75">Pg City South East (zone 75)
                                            </option>
                                            <option value="pg_city_west_zone_71">Pg City West (zone 71)</option>
                                            <option value="pg_rural_east_zone_80">Pg Rural East (zone 80)</option>
                                            <option value="pg_rural_north_zone_76">Pg Rural North (zone 76)</option>
                                            <option value="pg_rural_south_zone_78">Pg Rural South (zone 78)</option>
                                            <option value="pg_rural_west_zone_77">Pg Rural West (zone 77)</option>
                                            <option value="phillips_arm">Phillips Arm</option>
                                            <option value="piers_island">Piers Island</option>
                                            <option value="pinaus_lake">Pinaus Lake</option>
                                            <option value="pinecone">Pinecone</option>
                                            <option value="pinewood">Pinewood</option>
                                            <option value="pitt_meadows">Pitt Meadows</option>
                                            <option value="poplar_creek">Poplar Creek</option>
                                            <option value="port_alberni">Port Alberni</option>
                                            <option value="port_alice">Port Alice</option>
                                            <option value="port_clements">Port Clements</option>
                                            <option value="port_coquitlam">Port Coquitlam</option>
                                            <option value="port_edward">Port Edward</option>
                                            <option value="port_hardy">Port Hardy</option>
                                            <option value="port_mcneill">Port Mcneill</option>
                                            <option value="port_moody">Port Moody</option>
                                            <option value="port_neville">Port Neville</option>
                                            <option value="port_renfrew">Port Renfrew</option>
                                            <option value="pouce_coupe">Pouce Coupe</option>
                                            <option value="powell_river">Powell River</option>
                                            <option value="prespatou">Prespatou</option>
                                            <option value="prevost_island">Prevost Island</option>
                                            <option value="prince_george">Prince George</option>
                                            <option value="prince_rupert">Prince Rupert</option>
                                            <option value="prince_rupert_zone_52">Prince Rupert (zone 52)</option>
                                            <option value="prince_rupert_city">Prince Rupert City</option>
                                            <option value="princetn">Princetn</option>
                                            <option value="princeton">Princeton</option>
                                            <option value="princeton_bc">Princeton Bc</option>
                                            <option value="princeton_rural">Princeton Rural</option>
                                            <option value="pritchard">Pritchard</option>
                                            <option value="procter">Procter</option>
                                            <option value="progress">Progress</option>
                                            <option value="protection_island">Protection Island</option>
                                            <option value="purden">Purden</option>
                                            <option value="quadra_island">Quadra Island</option>
                                            <option value="qualicum_beach">Qualicum Beach</option>
                                            <option value="quathiaski_cove">Quathiaski Cove</option>
                                            <option value="quatsino">Quatsino</option>
                                            <option value="queen_charlotte_city">Queen Charlotte City</option>
                                            <option value="queen_charlotte_rural">Queen Charlotte Rural</option>
                                            <option value="queens_cove">Queens Cove</option>
                                            <option value="queest">Queest</option>
                                            <option value="quesnel">Quesnel</option>
                                            <option value="quesnel_zone_28">Quesnel (zone 28)</option>
                                            <option value="quesnel_rural_north">Quesnel Rural North</option>
                                            <option value="quesnel_rural_west">Quesnel Rural West</option>
                                            <option value="quesnel_town">Quesnel Town</option>
                                            <option value="quinson">Quinson</option>
                                            <option value="radium_hot_springs">Radium Hot Springs</option>
                                            <option value="raspberry">Raspberry</option>
                                            <option value="rdos_eastgate">Rdos Eastgate</option>
                                            <option value="rdos_hedley">Rdos Hedley</option>
                                            <option value="rdos_princeton">Rdos Princeton</option>
                                            <option value="read_island">Read Island</option>
                                            <option value="red_bluff_dragon_lake">Red Bluff Dragon Lake</option>
                                            <option value="red_rock_stoner">Red Rock Stoner</option>
                                            <option value="reid_lake">Reid Lake</option>
                                            <option value="remote_areas">Remote Areas</option>
                                            <option value="renata">Renata</option>
                                            <option value="revelstoke">Revelstoke</option>
                                            <option value="revelstoke_3_valley_gap">Revelstoke 3 Valley Gap</option>
                                            <option value="revelstoke_bc">Revelstoke Bc</option>
                                            <option value="richmond">Richmond</option>
                                            <option value="riondel">Riondel</option>
                                            <option value="rivervale">Rivervale</option>
                                            <option value="rivervale_genelle">Rivervale Genelle</option>
                                            <option value="roberts_creek">Roberts Creek</option>
                                            <option value="robson">Robson</option>
                                            <option value="robson_raspberry_brilliant">Robson Raspberry Brilliant
                                            </option>
                                            <option value="robson_valley_zone_81">Robson Valley (zone 81)</option>
                                            <option value="rock_creek">Rock Creek</option>
                                            <option value="rock_creek_bc">Rock Creek Bc</option>
                                            <option value="rock_creek_bridesville">Rock Creek Bridesville</option>
                                            <option value="rolla">Rolla</option>
                                            <option value="rosebery">Rosebery</option>
                                            <option value="rosedale">Rosedale</option>
                                            <option value="ross_spur">Ross Spur</option>
                                            <option value="rossland">Rossland</option>
                                            <option value="rossland_rural">Rossland Rural</option>
                                            <option value="royston">Royston</option>
                                            <option value="ruby_lake">Ruby Lake</option>
                                            <option value="rural_sicamous">Rural Sicamous</option>
                                            <option value="rutland">Rutland</option>
                                            <option value="ruxton_island">Ruxton Island</option>
                                            <option value="ryder_lake">Ryder Lake</option>
                                            <option value="sa">Sa</option>
                                            <option value="saanich">Saanich</option>
                                            <option value="saanich_east">Saanich East</option>
                                            <option value="saanich_west">Saanich West</option>
                                            <option value="saanichton">Saanichton</option>
                                            <option value="saint_ives">Saint Ives</option>
                                            <option value="salmo">Salmo</option>
                                            <option value="salmon_arm">Salmon Arm</option>
                                            <option value="salmon_arm_bc">Salmon Arm Bc</option>
                                            <option value="salmon_valley">Salmon Valley</option>
                                            <option value="salmonarm">Salmonarm</option>
                                            <option value="salt_spring">Salt Spring</option>
                                            <option value="salt_spring_is">Salt Spring Is</option>
                                            <option value="salt_spring_isl">Salt Spring Isl</option>
                                            <option value="salt_spring_island">Salt Spring Island</option>
                                            <option value="saltair">Saltair</option>
                                            <option value="sanca">Sanca</option>
                                            <option value="sandon">Sandon</option>
                                            <option value="sardis">Sardis</option>
                                            <option value="sardis_chwk_river_valley">Sardis Chwk River Valley</option>
                                            <option value="sardis_greendale">Sardis Greendale</option>
                                            <option value="saturna_island">Saturna Island</option>
                                            <option value="saturnina_island">Saturnina Island</option>
                                            <option value="savary_island">Savary Island</option>
                                            <option value="savona">Savona</option>
                                            <option value="sayward">Sayward</option>
                                            <option value="schroeder_creek">Schroeder Creek</option>
                                            <option value="scotch_creek">Scotch Creek</option>
                                            <option value="scotch_creek_bc">Scotch Creek Bc</option>
                                            <option value="scoth_creek">Scoth Creek</option>
                                            <option value="scotxh_creek">Scotxh Creek</option>
                                            <option value="sechelt">Sechelt</option>
                                            <option value="seymour_arm">Seymour Arm</option>
                                            <option value="shannon_lake">Shannon Lake</option>
                                            <option value="shawnigan_lake">Shawnigan Lake</option>
                                            <option value="shelley">Shelley</option>
                                            <option value="shoreacres">Shoreacres</option>
                                            <option value="shuswap_lake">Shuswap Lake</option>
                                            <option value="sicamous">Sicamous</option>
                                            <option value="sicamous_bc">Sicamous Bc</option>
                                            <option value="sicamous_swansea_point">Sicamous Swansea Point</option>
                                            <option value="sicmamous">Sicmamous</option>
                                            <option value="sidney">Sidney</option>
                                            <option value="silver_creek">Silver Creek</option>
                                            <option value="silver_star">Silver Star</option>
                                            <option value="silver_star_mountain">Silver Star Mountain</option>
                                            <option value="silverton">Silverton</option>
                                            <option value="sirdar">Sirdar</option>
                                            <option value="ski_hill_area">Ski Hill Area</option>
                                            <option value="skookumchuck">Skookumchuck</option>
                                            <option value="slocan">Slocan</option>
                                            <option value="slocan_park">Slocan Park</option>
                                            <option value="smithers">Smithers</option>
                                            <option value="smithers_and_area">Smithers And Area</option>
                                            <option value="smithers_and_area_zone">Smithers And Area (zone 54)</option>
                                            <option value="smithers_and_area_zone_54">Smithers And Area (zone 54)
                                            </option>
                                            <option value="smithers_rural">Smithers Rural</option>
                                            <option value="smithers_town">Smithers Town</option>
                                            <option value="soames_point">Soames Point</option>
                                            <option value="sointula">Sointula</option>
                                            <option value="sonora_island">Sonora Island</option>
                                            <option value="sooke">Sooke</option>
                                            <option value="sorrento">Sorrento</option>
                                            <option value="sorrento_bc">Sorrento Bc</option>
                                            <option value="south_blackburn">South Blackburn</option>
                                            <option value="south_castlegar">South Castlegar</option>
                                            <option value="south_fort_george">South Fort George</option>
                                            <option value="south_francois">South Francois</option>
                                            <option value="south_hwy_95">South Hwy 95</option>
                                            <option value="south_of_moyie">South Of Moyie</option>
                                            <option value="south_peace">South Peace</option>
                                            <option value="south_slocan">South Slocan</option>
                                            <option value="south_slocan_to_passmore">South Slocan To Passmore</option>
                                            <option value="south_surrey">South Surrey</option>
                                            <option value="south_surrey_white_rock">South Surrey White Rock</option>
                                            <option value="spallumcheen">Spallumcheen</option>
                                            <option value="sparwood">Sparwood</option>
                                            <option value="spencers_island">Spencers Island</option>
                                            <option value="spences_bridge">Spences Bridge</option>
                                            <option value="spillimacheen">Spillimacheen</option>
                                            <option value="spring_lake">Spring Lake</option>
                                            <option value="spruceland">Spruceland</option>
                                            <option value="spur_valley">Spur Valley</option>
                                            <option value="squamish">Squamish</option>
                                            <option value="st_andrews">St Andrews</option>
                                            <option value="st_ives">St Ives</option>
                                            <option value="st_lawrence_heights">St Lawrence Heights</option>
                                            <option value="stewart">Stewart</option>
                                            <option value="stewart_cassiar">Stewart Cassiar</option>
                                            <option value="street">Street</option>
                                            <option value="stuart_island">Stuart Island</option>
                                            <option value="stump_lake">Stump Lake</option>
                                            <option value="summerland">Summerland</option>
                                            <option value="summerland_bc">Summerland Bc</option>
                                            <option value="summit_lake">Summit Lake</option>
                                            <option value="sun_peaks">Sun Peaks</option>
                                            <option value="sunnybrae">Sunnybrae</option>
                                            <option value="sunnybrae_bc">Sunnybrae Bc</option>
                                            <option value="sunshine_v">Sunshine V</option>
                                            <option value="sunshine_valley">Sunshine Valley</option>
                                            <option value="surrey">Surrey</option>
                                            <option value="swan_lake">Swan Lake</option>
                                            <option value="swansea_point">Swansea Point</option>
                                            <option value="swanson_island">Swanson Island</option>
                                            <option value="syringa_lower_arrow_lake">Syringa Lower Arrow Lake</option>
                                            <option value="ta_ta_creek">Ta Ta Creek</option>
                                            <option value="tabor_lake">Tabor Lake</option>
                                            <option value="taghum">Taghum</option>
                                            <option value="tahsis">Tahsis</option>
                                            <option value="tappen">Tappen</option>
                                            <option value="tappen_bc">Tappen Bc</option>
                                            <option value="tappen_sunnybrae">Tappen Sunnybrae</option>
                                            <option value="tarrys">Tarrys</option>
                                            <option value="taylor">Taylor</option>
                                            <option value="telegraph_cove">Telegraph Cove</option>
                                            <option value="telegrpah_cove">Telegrpah Cove</option>
                                            <option value="telkwa">Telkwa</option>
                                            <option value="terrace">Terrace</option>
                                            <option value="terrace_zone_88">Terrace (zone 88)</option>
                                            <option value="terrace_city">Terrace City</option>
                                            <option value="texada_island">Texada Island</option>
                                            <option value="thetis_island">Thetis Island</option>
                                            <option value="thornhill">Thornhill</option>
                                            <option value="three_valley_gap">Three Valley Gap</option>
                                            <option value="thrums">Thrums</option>
                                            <option value="thrums_tarrys_glade">Thrums Tarrys Glade</option>
                                            <option value="tobiano">Tobiano</option>
                                            <option value="tofino">Tofino</option>
                                            <option value="tomslake">Tomslake</option>
                                            <option value="topley">Topley</option>
                                            <option value="trail">Trail</option>
                                            <option value="trail_rural">Trail Rural</option>
                                            <option value="tribune_channel">Tribune Channel</option>
                                            <option value="trout_lake">Trout Lake</option>
                                            <option value="tsawwassen">Tsawwassen</option>
                                            <option value="tulameen">Tulameen</option>
                                            <option value="tumbler_ridge">Tumbler Ridge</option>
                                            <option value="tumbler_ridge_bc">Tumbler Ridge Bc</option>
                                            <option value="tumbler_rodge">Tumbler Rodge</option>
                                            <option value="tupper">Tupper</option>
                                            <option value="twin_bays">Twin Bays</option>
                                            <option value="ucluelet">Ucluelet</option>
                                            <option value="union_bay">Union Bay</option>
                                            <option value="university_district">University District</option>
                                            <option value="upper_fraser">Upper Fraser</option>
                                            <option value="upper_mud">Upper Mud</option>
                                            <option value="v0c_1j0">V0c 1j0</option>
                                            <option value="v0h_1v0">V0h 1v0</option>
                                            <option value="v1h1m4">V1h1m4</option>
                                            <option value="v2a_2n8">V2a 2n8</option>
                                            <option value="valemount">Valemount</option>
                                            <option value="valemount_rural_west">Valemount Rural West</option>
                                            <option value="valemount_town">Valemount Town</option>
                                            <option value="valleyview">Valleyview</option>
                                            <option value="van_bow">Van Bow</option>
                                            <option value="vancouver">Vancouver</option>
                                            <option value="vanderhoof">Vanderhoof</option>
                                            <option value="vanderhoof_and_area_zone_56">Vanderhoof And Area (zone 56)
                                            </option>
                                            <option value="vanderhoof_town">Vanderhoof Town</option>
                                            <option value="vargas_island">Vargas Island</option>
                                            <option value="venon">Venon</option>
                                            <option value="vernon">Vernon</option>
                                            <option value="vernon_bc">Vernon Bc</option>
                                            <option value="vernon_british_columbia">Vernon British Columbia</option>
                                            <option value="veron">Veron</option>
                                            <option value="verv1h1m4non">Verv1h1m4non</option>
                                            <option value="victoria">Victoria</option>
                                            <option value="victoria_west">Victoria West</option>
                                            <option value="view_royal">View Royal</option>
                                            <option value="village_of_fruitvale">Village Of Fruitvale</option>
                                            <option value="village_of_montrose">Village Of Montrose</option>
                                            <option value="village_of_new_denver">Village Of New Denver</option>
                                            <option value="village_of_salmo">Village Of Salmo</option>
                                            <option value="village_of_silverton">Village Of Silverton</option>
                                            <option value="village_of_slocan">Village Of Slocan</option>
                                            <option value="village_of_warfield">Village Of Warfield</option>
                                            <option value="vla">Vla</option>
                                            <option value="walkem_island">Walkem Island</option>
                                            <option value="wardner">Wardner</option>
                                            <option value="warfield">Warfield</option>
                                            <option value="wasa">Wasa</option>
                                            <option value="wasa_rural">Wasa Rural</option>
                                            <option value="wells">Wells</option>
                                            <option value="wells_barkerville">Wells Barkerville</option>
                                            <option value="wells_gray">Wells Gray</option>
                                            <option value="west_amp_north_highway_1">West &amp; North Highway 1</option>
                                            <option value="west_arrow_park_to_edgewood">West Arrow Park To Edgewood
                                            </option>
                                            <option value="west_austi">West Austi</option>
                                            <option value="west_ballenas_island">West Ballenas Island</option>
                                            <option value="west_bank">West Bank</option>
                                            <option value="west_creston">West Creston</option>
                                            <option value="west_east_of_fernie">West East Of Fernie</option>
                                            <option value="west_fernie">West Fernie</option>
                                            <option value="west_jkelowna">West Jkelowna</option>
                                            <option value="west_kelowna">West Kelowna</option>
                                            <option value="west_kelowna_bc">West Kelowna Bc</option>
                                            <option value="west_kelwona">West Kelwona</option>
                                            <option value="west_shore_estates">West Shore Estates</option>
                                            <option value="west_thurlow_island">West Thurlow Island</option>
                                            <option value="west_vancouver">West Vancouver</option>
                                            <option value="westbank">Westbank</option>
                                            <option value="westbank_bc">Westbank Bc</option>
                                            <option value="westbridge">Westbridge</option>
                                            <option value="westholme">Westholme</option>
                                            <option value="westmount">Westmount</option>
                                            <option value="westv_kelowna">Westv Kelowna</option>
                                            <option value="westwood">Westwood</option>
                                            <option value="whaletown">Whaletown</option>
                                            <option value="whiskey_creek">Whiskey Creek</option>
                                            <option value="whistler">Whistler</option>
                                            <option value="white_lake">White Lake</option>
                                            <option value="white_rock">White Rock</option>
                                            <option value="whitevale">Whitevale</option>
                                            <option value="williams_lake">Williams Lake</option>
                                            <option value="williams_lake_zone">Williams Lake (zone 27)</option>
                                            <option value="williams_lake_zone_27">Williams Lake (zone 27)</option>
                                            <option value="williams_lake_city">Williams Lake City</option>
                                            <option value="williams_lake_rural_east">Williams Lake Rural East</option>
                                            <option value="williams_lake_rural_north">Williams Lake Rural North</option>
                                            <option value="williams_lake_rural_south">Williams Lake Rural South</option>
                                            <option value="williams_lake_rural_west">Williams Lake Rural West</option>
                                            <option value="willow_river">Willow River</option>
                                            <option value="wilmer">Wilmer</option>
                                            <option value="wilson_creek">Wilson Creek</option>
                                            <option value="windemere">Windemere</option>
                                            <option value="windermere">Windermere</option>
                                            <option value="winfield">Winfield</option>
                                            <option value="winlaw">Winlaw</option>
                                            <option value="winter_harbour">Winter Harbour</option>
                                            <option value="wkelowna">Wkelowna</option>
                                            <option value="wolfville">Wolfville</option>
                                            <option value="woss">Woss</option>
                                            <option value="wycliffe">Wycliffe</option>
                                            <option value="wynndel">Wynndel</option>
                                            <option value="wynndel_lakeview">Wynndel Lakeview</option>
                                            <option value="yahk">Yahk</option>
                                            <option value="yale">Yale</option>
                                            <option value="yarrow">Yarrow</option>
                                            <option value="ymir">Ymir</option>
                                            <option value="youbou">Youbou</option>
                                            <option value="zeballos">Zeballos</option>
                                        </optgroup>
                                        <optgroup label="NOT LISTED"></optgroup>
                                        <option value="other">Specify Area</option>
                                    </select>
                                </fieldset>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="quarter_width">
                                    <label>Floor Area (Sq. Ft.)</label>
                                    <input type="Text" name="FloorArea" class="form-control">
                                </fieldset>


                                <div id="solddata" style="display:none">
                                    <div class="clear"></div>
                                    <fieldset class="half_width">
                                        <label class="sale_only" style="">Sold Price</label>
                                        <div class="radio"><input type="Text" name="soldprice" class="form_text"
                                                                  value=""></div>
                                    </fieldset>
                                    <fieldset class="half_width last">
                                        <label class="sale_only" style="">Sold Date</label>
                                        <input type="text" disabled="disabled" name="date_alt" value="">
                                        <div class="radio"><input type="Text" name="solddate" class="form_text"></div>
                                        <a href="#" id="cleardate">clear</a>
                                    </fieldset>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="full_width">
                                    <label>Address</label>
                                    <input type="Text" name="address" class="form-control">
                                </fieldset>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="half_width last">
                                    <div class="sale_only" style="width:100%; margin-right:5px; float:left; ">
                                        <label>Status</label>
                                        <div >
                                            <input type="radio" class="form_radio" name="status" value="I"><span>INACTIVE</span>
                                            <input type="radio" class="form_radio" checked="checked" name="status"
                                                   value="A"><span>ACTIVE</span>
                                            <input type="radio" class="form_radio" name="status"
                                                   value="S"><span>SOLD</span>
                                        </div>
                                    </div>
                                    <div class="rent_only" style="width:100%; margin-right:5px; float:left; display: none">
                                        <label>Rental Status</label>
                                        <div class="radio">
                                            <div >
                                                <input type="radio" class="form_radio" name="RentalStatus"
                                                       value="X"><span>INACTIVE</span>
                                                <input type="radio" class="form_radio" name="RentalStatus"
                                                       value="R"><span>AVAILABLE</span>
                                                <input type="radio" class="form_radio" name="RentalStatus"
                                                       value="T"><span>RENTED</span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="quarter_width">
                                    <div class="sale_only" style="width:100%; margin-right:5px; float:left;">
                                        <label  style="">Price (CAD)*</label>
                                        <input type="Text" name="price" class="form-control">
                                    </div>
                                    <div class="rent_only"  style="width:100%; margin-right:5px; float:left; display: none">
                                        <label >Monthly Rent (CAD)*</label>
                                        <input type="Text" name="MonthlyRent" class="form-control">
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div style="text-align: right">
                            <button type="button" class="btn btn-primary" onclick="next_tab()">next</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="messages">
                        <div class="row">
                            <div class="col-md-6 ">
                                <fieldset class="full_width clear">

                                    <label for="validationTextarea">Descrtiption</label>
                                    <textarea class="form-control " name="longDescription" id="validationTextarea"
                                              required=""></textarea>
                                    <div class="invalid-feedback">
                                        Please enter a message in the textarea.
                                    </div>

                                </fieldset>
                            </div>
                            <div class="col-md-6 ">
                                <fieldset class="full_width clear">

                                    <label for="validationTextarea">Widget Embed Codes</label>
                                    <textarea name="dynamic_embed" class="form-control"></textarea>
                                    <p>Third-party embed codes such as YouTube videos, virtual tours, etc.</p>


                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="full_width clear">
                                    <label>Amenities</label>
                                    <textarea name="Amenities" class="form-control"></textarea>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="quarter_width">
                                        <label>Custom Label 1</label>
                                        <input type="Text" name="Custom_Label_1" class="form-control">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">

                                    <fieldset class="quarter_width">
                                        <label>Custom Value 1</label>
                                        <input type="Text" name="Custom_Value_1" class="form-control">
                                    </fieldset>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="full_width">
                                    <label>Site Influences</label>
                                    <textarea name="siteinfluences" class="form-control"></textarea>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="quarter_width">
                                        <label>Custom Label 2</label>
                                        <input type="Text" name="Custom_Label_2" class="form-control">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="quarter_width last">
                                        <label>Custom Value 2</label>
                                        <input type="Text" name="Custom_Value_2" class="form-control">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="full_width">
                                    <label>Features</label>
                                    <textarea name="Features" class="form-control"></textarea>
                                </fieldset>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="quarter_width">
                                        <label>Bedrooms</label>
                                        <input type="Text" name="Bedrooms" class="form-control">
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="quarter_width">
                                        <label>Bathrooms</label>
                                        <input type="Text" name="Bathrooms" class="form-control">
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div style="text-align: right">
                            <button type="button" class="btn btn-primary" onclick="back_tab()">Back</button>
                            <button type="button" class="btn btn-primary" onclick="next_tab()">next</button>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="level">
                        <div class="row" id="more_details">
                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Term</label>
                                <input type="Text" name="term" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Availability</label>
                                <input type="date" name="availability" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Furnishing</label>
                                <input type="Text" name="furnishing" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 last quarter_width rent_only" style="display: none;">
                                <label>Utilities</label>
                                <input type="Text" name="utilities" class="form-control ">
                            </fieldset>

                        </div>
                        <div class="row">

                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Parking</label>
                                <input type="Text" name="parking" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Smoking</label>
                                <select name="smoking" class="form-control">
                                    <option value="Y">yes</option>
                                    <option value="N">no</option>
                                </select>
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width rent_only" style="display: none;">
                                <label>Security Deposit</label>
                                <input type="Text" name="Security_Deposit" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 last quarter_width rent_only" style="display: none;">
                                <label>Property Manager</label>
                                <input type="Text" name="manager" class="form-control ">
                            </fieldset>
                        </div>

                        <div class="row">
                            <fieldset class="col-md-3 quarter_width">
                                <label>Basement Area</label>
                                <input type="Text" name="basementarea" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width">
                                <label>Zoning</label>
                                <input type="Text" name="zoning" class="form-control ">
                            </fieldset>
                            <fieldset class="col-md-3 last quarter_width">
                                <label>Postal Code</label>
                                <input type="Text" name="postalcode" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width">
                                <label>Lot Size (Sq. Ft.)</label>
                                <input type="Text" name="LotSize" class="form-control">
                            </fieldset>


                        </div>
                        <div class="row">
                            <fieldset class="col-md-3 quarter_width sale_only" style="">
                                <label>Tax Amount</label>
                                <input type="Text" name="TaxAmount" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width sale_only" style="">
                                <label>Tax Year</label>
                                <input type="Text" name="TaxBuilt" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width">
                                <label>Pets</label>
                                <input type="Text" name="pets" class="form-control">

                            </fieldset>
                            <fieldset class="col-md-3 quarter_width">
                                <label>Year Built</label>
                                <input type="Text" name="yearBuilt" class="form-control">
                            </fieldset>

                            <fieldset class="col-md-3 quarter_width">
                                <label>sub_Area</label>
                                <input type="Text" name="SubArea" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 quarter_width last sale_only" style="">
                                <label>Maint. Fee</label>
                                <input type="Text" name="maint_fee" class="form-control">
                            </fieldset>
                            <fieldset class="col-md-3 " style="">
                                <label>TypeofDwelling</label>
                                <select name="TypeofDwelling" class="form-control">
                                    <option value="1">one</option>
                                    <option value="2">two</option>
                                    <option value="3">there</option>
                                </select>

                            </fieldset>
                            <fieldset class="col-md-3 " style="">
                                <label>StyleofHome</label>
                                <select name="StyleofHome" class="form-control">
                                    <option value="1">one</option>
                                    <option value="2">two</option>
                                    <option value="3">there</option>
                                </select>

                            </fieldset>
                        </div>
                        <div style="text-align: right">
                            <button type="button" class="btn btn-primary" onclick="back_tab()">Back</button>
                            <button type="button" class="btn btn-primary" onclick="next_tab()">next</button>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="asas">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationServer05">location</label>
                                <input type="text" class="form-control " name="location" id="validationServer05"
                                       required="">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>images</label>
                                <input type="file" class="form-control " name="img[]" multiple="" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationServer05">street</label>
                                <input type="text" class="form-control " name="street" id="validationServer05"
                                       required="">
                            </div>

                            <div class="col-md-3 mb-3">
                                <label>video</label>
                                <input type="file" class="form-control " name="video" id="validationS444e11rver05"
                                       required="">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset class="full_width clear">
                                        <label>Virtual Tour URL (please include the full URL, including http://)</label>
                                        <input type="Text" name="virtualtour_url" class="form-control">
                                    </fieldset>
                                </div>


                            </div>

                        </div>
                        <div style="text-align: right">
                            <button type="button" class="btn btn-primary" onclick="back_tab()">Back</button>
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </div>

                </div>
            </form>


        </div>


    </div>

    <!-- /.container-fluid -->




    <script>
        function next_tab() {

            $('a.nav-link.active').parent().next().find('a').click()
        }

        function back_tab() {
            $('a.nav-link.active').parent().prev().find('a').click()
        }

        $("document").ready(function () {
            $("[name=type]").change(function () {
                if (this.value == "R") {
                    $("input[type='radio'][name='status'][value='R']").prop('checked', true);
                    $("input[type='radio'][name='status'][value='R']").trigger("change");
                    $(".sale_only").hide();
                    $(".rent_only").show();
                } else {
                    $("input[type='radio'][name='status'][value='A']").prop('checked', true);
                    $("input[type='radio'][name='status'][value='A']").trigger("change");
                    $(".rent_only").hide();
                    $(".sale_only").show();
                }
            });


        });


    </script>



@endsection
