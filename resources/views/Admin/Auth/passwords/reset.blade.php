@extends('Admin.Auth.Layout')
@section('CartTitle' , "Reset Password")
@section('Form')
	<form class="user" action="{{route('admin-auth.password.update')}}" method="post">
		@csrf
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="form-group">
			<input type="email" class="form-control form-control-user @error('email') is-invalid @endError" id="exampleInputEmail"  aria-describedby="emailHelp" placeholder="Enter Email Address..." name="email" value="{{$email}}">
			@error('email')
				<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
			@enderror
		</div>
		<div class="form-group">
			<input type="password" class="form-control form-control-user @error('password') is-invalid @endError"  aria-describedby="passwordHelp" placeholder="Enter New Password..." name="password">
			@error('password')
				<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
			@enderror
		</div>
		<div class="form-group">
			<input type="password" class="form-control form-control-user @error('password_confirmation') is-invalid @endError" id="exampleInputEmail"  aria-describedby="passwordHelp" placeholder="Enter Confirm New Password..." name="password_confirmation">
			@error('email')
				<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
			@enderror
		</div>
		<button type="submit" class="btn btn-primary btn-user btn-block">
			Reset Password
		</button>
	</form>
@endsection