@extends('Admin.Auth.Layout')
@section('CartTitle' , "Forgot Your Password?")
@section('Form')
	<form class="user" action="{{route('admin-auth.password.email')}}" method="post">
		@csrf
		<div class="form-group">
			<input type="email" class="form-control form-control-user @error('email') is-invalid @endError" id="exampleInputEmail"  aria-describedby="emailHelp" placeholder="Enter Email Address..." name="email">
			@error('email')
				<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
			@enderror
		</div>
		<button type="submit" class="btn btn-primary btn-user btn-block">
			Send
		</button>
	</form>
@endsection