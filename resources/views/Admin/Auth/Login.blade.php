@extends('Admin.Auth.Layout')
@section('CartTitle' , 'Welcome Back!')
@section('Form')
	<form method="post" action="{{route('admin-auth.login')}}">
		@csrf()
		<div class="form-group">
			<input type="email" name="email" class="form-control form-control-user @error('email') is-invalid @endError" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
			@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
            </span>
			@enderror
		</div>
		<div class="form-group">
			<input type="password" name="password" class="form-control form-control-user @error('password') is-invalid @endError" id="exampleInputPassword" placeholder="Password">
			@error('password')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
            </span>
			@enderror
		</div>
		<div class="form-group">
			<div class="custom-control custom-checkbox small">
				<input type="checkbox" class="custom-control-input" id="customCheck" name="remember">
				<label class="custom-control-label" for="customCheck">Remember Me</label>
			</div>
		</div>
		<button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
	</form>
	<hr>
	<div class="text-center">
		<a class="small" href="{{route('admin-auth.password.request')}}">Forgot Password?</a>
	</div>
@endsection