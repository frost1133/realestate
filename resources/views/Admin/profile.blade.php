@extends('Admin.Template.layout')
@section('content')
	@include('Admin.Template.notification')
	
<div class="row">
		<div class="col-md-3">
			<div class="card shadow">
				<div class="card-header">Avatar</div>
				<div class="card-body">
					<div class="card" style="width: 18rem;">
						<img src="{{$admin->avatar??'https://images.unsplash.com/source-404?fit=crop&fm=jpg&h=800&q=60&w=1200'}}" class="card-img-top" alt="...">
						<div class="card-body">
							<h5><i class="fas fa-user"></i> {{$admin->name}}</h5><br>
							<h5><i class="fas fa-envelope"></i> {{$admin->email}}</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card shadow">
				<div class="card-header">Change Profile</div>
				<div class="card-body">
					<form action="{{route('admin.profile.change')}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="Name">Name:</label>
							<input type="text" name="name" id="Name" class="form-control" value="{{$admin->name}}">
						</div>
						<div class="form-group">
							<label for="email">Email:</label>
							<input type="text" name="email" id="Email" class="form-control" value="{{$admin->email}}">
						</div>
						<div class="form-group">
							<label for="Avatar">Avatar:</label>
							<input type="file" name="avatar" id="Avatar" class="form-control">
						</div>
						<button type="submit" class="btn btn-success">Send</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="card shadow">
				<div class="card-header">Change Password</div>
				<div class="card-body">
					<form action="{{route('admin.profile.password')}}" method="post">
						@csrf
						<div class="form-group">
							<label for="Password">New Password:</label>
							<input type="password" name="password" id="Password" class="form-control">
						</div>
						<div class="form-group">
							<label for="PasswordConfirmation">Confirm New Password:</label>
							<input type="password" name="password_confirmation" id="PasswordConfirmation" class="form-control">
						</div>
						<button type="submit" class="btn btn-success">Change Password</button>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection