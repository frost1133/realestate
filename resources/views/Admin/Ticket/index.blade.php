@extends('Admin.Template.layout')
@section('content')
    @include('Admin.Template.notification')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ListRequest:</h6>

        </div>
        <div class="card-body">
            <form class="" action="{{request()->fullUrl()}}">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-2 col -sm-12-col-xl-1">
                        <select name="ticketType" id="ticketType" class="form-control">
                            <option value {{request('ticketType')? 'selected' : NULL}}>Select Ticket Type</option>
                            <option value="0" {{request('ticketType') === 0 ? 'selected':NULL}}>But</option>
                            <option value="1" {{request('ticketType') === 1 ? 'selected':NULL}}>Sell</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xl-2">
                        <input type="hidden" name="type" value="{{old('type')}}">
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{request('name')}}">
                    </div>
                    <div class="col-md-3 col-sm-12 col-xl-2">
                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{request('email')}}">
                    </div>
                    <div class="col-md-4 col-sm-12 col-xl-3">
                        <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" value="{{old('phone')}}">
                    </div>
                    <div class="col-sm-2 col-sn-12 col-xl-2">
                        <button type="submit" class="btn btn-outline-primary">Apply</button>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-hover" border="5">
                <thead>
                <tr>
                    <th  style="text-align: center">Id</th>
                    <th  style="text-align: center">Tracing Code</th>
                    <th  style="text-align: center">Date</th>
                    <th  style="text-align: center">Name</th>
                    <th  style="text-align: center">Phone</th>
                    <th  style="text-align: center">TicketType</th>

                    <th style="text-align: center">Settings</th>

                </tr>
                </thead>
                @foreach($tickets as $ticket)
                    <tr>
                        <td style="text-align: center">{{$ticket->id}}</td>
                        <td style="text-align: center">{{$ticket->tracingCode}}</td>
                        <td style="text-align: center">{{$ticket->updated_at}}</td>
                        <td style="text-align: center">{{$ticket->name}}</td>
                        <td style="text-align: center">{{$ticket->phone}}</td>
                        <td style="text-align: center">{{$ticket->ticketType}}</td>
                        <td style="text-align: center">
                            <a href="{{route('admin.ticket.edit',[$ticket])}}"> <button type="button" class="btn btn-warning btn-sm">edit</button></a>
                            <button class="btn btn-primary btn-sm" type="button" onclick="setagent('agent_{{$ticket->id}}')">
                                SetAgent
                            </button>
                        </td>

                    </tr>
                    <tr id="agent_{{$ticket->id}}" style="display: none">
                        <td colspan="7" style="display: none">
                            <div class="row">
                                <form action="{{route('admin.ticket.update',$ticket->id)}}" method="post">
                                    @csrf


                                    <div class="form-group ">
                                        <label for="exampleInputPassword1">state</label>
                                        <select class="form-control" name="agent_id">
                                            @foreach($agents as $agent)
                                                <option value="{{$agent->id}}" {{$agent->agent_id == $agent->id ? 'selected':''}}>{{$agent->name}}</option>

                                            @endforeach
                                        </select>
                                    </div>


                                    <button type="submit" class="btn btn-primary" style="text-align: right">update</button>
                                </form>

                            </div>
                        </td>
                    </tr>

                @endforeach
            </table>
        </div>
    </div>

@endsection
@section('js')
    <script>
        function setagent(id) {
            if ($('#' + id).css('display') == 'none') {

                $('#' + id).show(function () {
                    $($('#' + id).children()[0]).slideDown()
                })
            } else {
                $($('#' + id).children()[0]).slideUp(function () {
                    $('#' + id).hide()
                })
            }

        }
    </script>
@endsection
