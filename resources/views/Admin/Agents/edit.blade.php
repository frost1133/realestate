@extends('Admin.Template.layout')
@section('content')
    <form action="{{route('admin.agent.update',$ticket)}}" method="post">
        @csrf
        <div class="row">
            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">name</label>
                <input type="text" name="name" class="form-control" value="{{$ticket->name}}">

            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">email</label>
                <input type="email" name="email" class="form-control" value="{{$ticket->email}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">phone</label>
                <input type="text" name="phone" class="form-control" value="{{$ticket->phone}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">area</label>
                <input type="text" class="form-control" name="area" value="{{$ticket->area}}">

            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">propertyType</label>
                <input type="text" name="propertyType" class="form-control" value="{{$ticket->propertyType}}">

            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">priceRange</label>
                <input type="text" name="priceRange" class="form-control" value="{{$ticket->priceRange}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">streetAddress</label>
                <input type="text" class="form-control" name="streetAddress" value="{{$ticket->streetAddress}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">city</label>
                <input type="text" class="form-control" name="city" value="{{$ticket->city}}">
            </div>

        </div>
        <div class="row">

            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">bedrooms</label>
                <input type="text" class="form-control" name="bedrooms" value="{{$ticket->bedrooms}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">bathrooms</label>
                <input type="text" class="form-control" name="bathrooms" value="{{$ticket->bathrooms}}">

            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">sqFt</label>
                <input type="text" class="form-control" name="sqFt" value="{{$ticket->sqFt}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">lotSize</label>
                <input type="text" class="form-control" name="lotSize" value="{{$ticket->lotSize}}">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">when</label>
                <input type="text" name="when" class="form-control" value="{{$ticket->when}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputEmail1">message</label>
                <textarea class="form-control" name="message">{{$ticket->message}}</textarea>

            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">tracingCode</label>
                <input type="text" name="tracingCode" class="form-control" value="{{$ticket->tracingCode}}">
            </div>
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">requestType</label>
                <input type="text" name="ticketType" class="form-control" value="{{$ticket->ticketType}}">
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="exampleInputPassword1">state</label>
                <input type="text" name="state" class="form-control" value="{{$ticket->state}}">
            </div>

        </div>

        <button type="submit" class="btn btn-primary">update</button>
    </form>
@endsection
