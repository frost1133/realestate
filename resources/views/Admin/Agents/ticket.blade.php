@extends('Admin.Template.layout')
@section('content')
    @include('Admin.Template.notification')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ListAgent:</h6>

        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover" border="5">
                <thead>
                <tr>
                    <th style="text-align: center">Id</th>
                    <th style="text-align: center">Date</th>
                    <th style="text-align: center">Name</th>
                    <th style="text-align: center">Phone</th>
                    <th style="text-align: center">Email</th>
                    <th style="text-align: center">RequestType</th>

                    <th style="text-align: center">Settings</th>

                </tr>
                </thead>
                @foreach($tickets as $ticket)
                    <tr>
                        <td style="text-align: center">{{$ticket->id}}</td>
                        <td style="text-align: center">{{$ticket->updated_at}}</td>
                        <td style="text-align: center">{{$ticket->name}}</td>
                        <td style="text-align: center">{{$ticket->phone}}</td>
                        <td style="text-align: center">{{$ticket->email}}</td>
                        <td style="text-align: center">{{$ticket->ticketType}}</td>
                        <td style="text-align: center">
                            <a href="{{route('admin.agent.edit',[$ticket])}}">
                                <button type="button" class="btn btn-warning btn-sm">edit</button>
                            </a>
                            <button class="btn btn-primary btn-sm" type="button" onclick="setagent('agent_{{$ticket->id}}')">
                                SetAgent
                            </button>
                        </td>


                    </tr>
                    <tr id="agent_{{$ticket->id}}" style="display: none">
                        <td colspan="7" style="display: none">
                            <div class="row">
                                <form action="{{route('admin.agent.update',$ticket->id)}}" method="post">
                                    @csrf


                                    <div class="form-group ">
                                        <label for="exampleInputPassword1">state</label>

                                    </div>


                                    <button type="submit" class="btn btn-primary" style="text-align: right">update</button>
                                </form>

                            </div>
                        </td>
                    </tr>

                @endforeach
            </table>
        </div>
    </div>



@endsection
@section('js')
    <script>
        function setagent(id) {
            if ($('#' + id).css('display') == 'none') {

                $('#' + id).show(function () {
                    $($('#' + id).children()[0]).slideDown()
                })
            } else {
                $($('#' + id).children()[0]).slideUp(function () {
                    $('#' + id).hide()
                })
            }

        }
    </script>
@endsection



