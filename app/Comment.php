<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int|null $user_id
 * @property int|null $admin_id
 * @property int|null $parentComment
 * @property string|null $confirmComment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Admin|null $admin
 * @property-read \App\Article $article
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $childes
 * @property-read int|null $childes_count
 * @property-read Comment|null $parent
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereConfirmComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereParentComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUserId($value)
 * @mixin \Eloquent
 * @property int $article_id
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereArticleId($value)
 */
class Comment extends Model
{
	protected $guarded = [ 'id' ];
	protected $fillable = ['body','title','article_id','user_id','admin_id','parentComment'];
	protected $casts = [
		'confirmComment' => 'timestamp'
	];
	public function parent ()
	{
		return $this->belongsTo( Comment::class , "parentComment" );
	}

	public function childes ()
	{
		return $this->hasMany( Comment::class , 'parentComment' );
	}

	public function user ()
	{
		return $this->belongsTo( User::class );
	}

	public function admin ()
	{
		return $this->hasOne(Admin::class);
	}
	public function article(){
		return $this->belongsTo(Article::class );
	}
}
