<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\House
 *
 * @property int $id
 * @property int $admin_id
 * @property string $postalcode
 * @property string|null $MLSNumber
 * @property string|null $RentalID
 * @property string $address
 * @property string $type
 * @property int|null $price
 * @property int|null $MonthlyRent
 * @property int|null $maint_fee
 * @property string|null $pets
 * @property string|null $smoking
 * @property string|null $Security_Deposit
 * @property string|null $term
 * @property string|null $manager
 * @property string|null $availability
 * @property string|null $furnishing
 * @property string|null $utilities
 * @property string|null $parking
 * @property int $TaxAmount
 * @property int $LotSize
 * @property string $Zoning
 * @property string $TypeofDwelling
 * @property string $StyleofHome
 * @property string $virtualtour_url
 * @property string $Features
 * @property string $yearBuilt
 * @property string $TaxBuilt
 * @property string $BasementArea
 * @property int $Bathrooms
 * @property int $Bedrooms
 * @property string $Area
 * @property string $SubArea
 * @property string $FloorArea
 * @property string $longDescription
 * @property string $SiteInfluences
 * @property string $Amenities
 * @property string $img
 * @property string|null $video
 * @property string|null $dynamic_embed
 * @property string|null $status
 * @property string|null $RentalStatus
 * @property string $location
 * @property string $street
 * @property string|null $Custom_Label_1
 * @property string|null $Custom_Value_1
 * @property string|null $Custom_Label_2
 * @property string|null $Custom_Value_2
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Admin $admin
 * @method static \Illuminate\Database\Eloquent\Builder|House newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|House newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|House query()
 * @method static \Illuminate\Database\Eloquent\Builder|House whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereAmenities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereAvailability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereBasementArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereBathrooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereBedrooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCustomLabel1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCustomLabel2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCustomValue1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereCustomValue2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereDynamicEmbed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereFeatures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereFloorArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereFurnishing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereLongDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereLotSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMLSNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMaintFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMonthlyRent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereParking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House wherePets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House wherePostalcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereRentalID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereRentalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSecurityDeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSiteInfluences($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSmoking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereStyleofHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereSubArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereTaxBuilt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereTypeofDwelling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereUtilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereVirtualtourUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereYearBuilt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereZoning($value)
 * @mixin \Eloquent
 * @property string|null $metaTitle
 * @property string|null $metaKeywords
 * @property string|null $metaDescription
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|House whereMetaTitle($value)
 */
class House extends Model
{
  protected $guarded=['id'];
  protected $casts = [
  	'img' => 'array'
  ];

  public function  admin()
  {
	  return $this->belongsTo( Admin::class );
  }
}
