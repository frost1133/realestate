<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Article
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property string $slug
 * @property array|null $tag
 * @property string $featuredImage
 * @property int $status
 * @property int|null $admin_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $releaseTime
 * @property string|null $metaTitle
 * @property array|null $metaKeywords
 * @property string|null $metaDescription
 * @property-read \App\Admin|null $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read int|null $comments_count
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereAdminId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereBody( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereFeaturedImage( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereMetaDescription( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereMetaKeywords( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereMetaTitle( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereReleaseTime( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSlug( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereStatus( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTag( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTitle( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Article extends Model
{
	public static $Status = [
		'Publish'   => 1 ,
		'Draft'     => 2 ,
		'UnPublish' => 3 ,
	];
	protected $guarded = [ 'id' ];
	protected $casts = [
		'metaKeywords' => 'array' ,
		'tag'          => 'array' ,
		'releaseTime'  => 'timestamp' ,
	];
	
	public function setSlugAttribute ( $value )
	{
		$slug = \Illuminate\Support\Str::slug( $value );
		$id = $this->id??FALSE;
		$slugs = $this->where( 'slug' , 'like' , $slug . '%' );
		if ($id){
			$slugs = $slugs->whereKeyNot($id);
		}
		$slugs= $slugs->get( 'slug' )->pluck( 'slug' )->toArray();
		if ( ! empty( $slugs ) ) {
			$x = 1;
			do {
				$x ++;
				$newSlug = $slug . '-' . $x;
				$Exist  = FALSE;
				foreach ($slugs as $s){
					if ($s == $newSlug){
						$Exist = TRUE;
					}
				}
			} while ( $Exist );
			$slug = $newSlug;
		}
		$this->attributes['slug'] = $slug;
	}
	
	public function admin ()
	{
		return $this->belongsTo( 'App\Admin' );
	}
	
	public function categories ()
	{
		return $this->belongsToMany( 'App\Category' );
	}
	
	public function comments ()
	{
		return $this->hasMany( 'App\Comment' );
	}
	
	public function getRouteKeyName ()
	{
		return 'slug';
	}
}
