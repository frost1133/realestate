<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Option
 *
 * @property int $id
 * @property string $name
 * @property array $value
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Option newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Option newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Option query()
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereValue($value)
 * @mixin \Eloquent
 */
class Option extends Model
{
	protected $fillable = [ 'name' , 'value' , 'active' ];
	protected $casts = [ 'value' => 'json' ];
	
	public static function SocialMedia ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::SocialMedia() ) {
				return self::SocialMedia()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'SocialMedia' , 'value' => $data ] );
		}
		return self::where( 'name' , 'SocialMedia' )->first();
	}
	public static function AboutPage ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::AboutPage() ) {
				return self::AboutPage()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'AboutPage' , 'value' => $data ] );
		}
		return self::where( 'name' , 'AboutPage' )->first();
	}
	
	public static function Contacts ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::Contacts() ) {
				return self::Contacts()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'Contacts' , 'value' => $data ] );
		}
		return self::where( 'name' , 'Contacts' )->first();
	}
	
	public static function QSearch ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::QSearch() ) {
				return self::QSearch()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'QSearch' , 'value' => $data ] );
		}
		return self::where( 'name' , 'QSearch' )->first();
	}
	
	public static function HomeSlider ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::HomeSlider() ) {
				return self::HomeSlider()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'HomeSlider' , 'value' => $data ] );
		}
		return self::where( 'name' , 'HomeSlider' )->first();
	}
	public static function HomeAbout ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::HomeAbout() ) {
				return self::HomeAbout()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'HomeAbout' , 'value' => $data ] );
		}
		return self::where( 'name' , 'HomeAbout' )->first();
	}
	public static function HomeBuyerSeller ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::HomeBuyerSeller() ) {
				return self::HomeBuyerSeller()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'HomeBuyerSeller' , 'value' => $data ] );
		}
		return self::where( 'name' , 'HomeBuyerSeller' )->first();
	}
	public static function PageSeller ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::PageSeller() ) {
				return self::PageSeller()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'PageSeller' , 'value' => $data ] );
		}
		return self::where( 'name' , 'PageSeller' )->first();
	}
	public static function PageBuyer ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::PageBuyer() ) {
				return self::PageBuyer()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'PageBuyer' , 'value' => $data ] );
		}
		return self::where( 'name' , 'PageBuyer' )->first();
	}
	
	public static function HomeTestimonials ( array $data = NULL )
	{
		if ( $data ) {
			if ( self::HomeTestimonials() ) {
				return self::HomeTestimonials()->update( [ 'value' => $data ] );
			}
			return self::create( [ 'name' => 'HomeTestimonials' , 'value' => $data ] );
		}
		return self::where( 'name' , 'HomeTestimonials' )->first();
	}
}
