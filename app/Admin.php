<?php

namespace App;

use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

/**
 * App\Admin
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[] $Articles
 * @property-read int|null $articles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $Permissions
 * @property-read int|null $permissions_count
 * @property-read mixed $permissions_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\House[] $houses
 * @property-read int|null $houses_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereName( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRememberToken( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt( $value )
 * @mixin \Eloquent
 * @property string|null $avatar
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAvatar($value)
 */
class Admin extends Authenticatable
{
	use Notifiable;
	
	protected $fillable = [
		'name' ,
		'email' ,
		'password',
		'avatar'
	];
	
	public function sendPasswordResetNotification ( $token )
	{
		return $this->notify( new AdminResetPasswordNotification( $token ) );
	}
	
	public function setPasswordAttribute ( $value )
	{
		$this->attributes[ 'password' ] = ( Hash::needsRehash( $value ) ? Hash::make( $value ) : $value );
	}
	
	public function getPermissionsArrayAttribute ()
	{
		return $this->Permissions()->pluck( 'permission' )->toArray();
	}
	
	public function Permissions ()
	{
		
		return $this->hasMany( Permission::class );
		
	}
	
	public function HsePermission ( $premission )
	{
		return $this->Permissions()->firstWhere( 'permission' );
	}
	
	public function DeletePermission ( array $permissions )
	{
		$this->Permissions()->whereIn( 'permission' , $permissions )->delete();
	}
	
	public function houses ()
	{
		return $this->hasMany( House::class );
	}
	
	public function Articles ()
	{
		return $this->hasMany( Article::class );
	}
}
