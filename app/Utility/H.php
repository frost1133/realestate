<?php

namespace App\Utility;
class H
{
	public static function DOld ( $f , $d = NULL )
	{
		return old( $f ) ?? $d;
	}
	
	public static function url ( $field , $value , $ignore = NULL )
	{
		$option = NULL;
		foreach ( request()->all() as $op => $vl ) {
			if ( $ignore != $op ) {
				if ( $vl && $op != $field ) {
					$option ? $option .= '&' : NULL;
					if ( is_array( $vl ) ) {
						foreach ( $vl as $v ) {
							$option ? $option .= '&' : NULL;
							$option .= "{$op}%5B%5D={$v}";
						}
					} else $option .= $op . '=' . $vl;
				}
			}
		}
		$url = "?{$field}={$value}";
		$option != NULL ? $url .= '&' . $option : NULL;
		return ( request()->url() . $url );
	}
}