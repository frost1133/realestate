<?php
	
	
	namespace App\Utility;
	
	
	use App\Admin;
	
	class Permission
	{
		public static function AdminAllPermissions ( Admin $admin )
		{
			return $admin->Permissions()->pluck('Permission')->toArray();
		}
	}