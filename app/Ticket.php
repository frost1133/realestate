<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\SkippedTestSuiteError;

/**
 * App\Ticket
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $area
 * @property string|null $streetAddress
 * @property string|null $city
 * @property string $propertyType
 * @property string|null $priceRange
 * @property int $bedrooms
 * @property int $bathrooms
 * @property int $sqFt
 * @property int|null $lotSize
 * @property string $when
 * @property string $message
 * @property string $tracingCode
 * @property int $ticketType
 * @property int $state
 * @property int|null $admin_id
 * @property int|null $agent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $User
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereBathrooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereBedrooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereLotSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket wherePriceRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket wherePropertyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereSqFt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereStreetAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereTicketType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereTracingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereWhen($value)
 * @mixin \Eloquent
 */
class Ticket extends Model
{

	protected $guarded = ["id"];
	public function User(){
		return $this->belongsTo(User::class);
	}
}
