<?php
	
	namespace App\Providers;
	
	use App\Admin;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Blade;
	use Illuminate\Support\ServiceProvider;
	use Symfony\Component\VarDumper\Dumper\DataDumperInterface;
	
	class CustomBladeDirectives extends ServiceProvider
	{
		/**
		 * Register services.
		 *
		 * @return void
		 */
		public function register ()
		{
			//
		}
		
		/**
		 * Bootstrap services.
		 *
		 * @return void
		 */
		public function boot ()
		{
			Blade::if( 'permission' , function ( ...$permissions ) {
				return Auth::guard( 'admin' )->user()->Permissions()->whereIn( 'permission' , $permissions )->get();
			} );
		}
	}
