<?php
	
	namespace App\Http\Middleware;
	
	use App\Admin;
	use Closure;
	use Illuminate\Support\Facades\Auth;
	
	class PermissionMiddleware
	{
		/**
		 * Handle an incoming request.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param \Closure $next
		 * @param array $permissions
		 * @return mixed
		 */
		public function handle ( $request , Closure $next , ...$permissions )
		{
			$CurrentAdminHasPermission = Auth::guard( 'admin' )->user()->Permissions()->whereIn( 'permission' , $permissions )->get();
			return $CurrentAdminHasPermission->isNotEmpty() ? $next( $request ) : redirect( 'admin/' );
		}
	}
