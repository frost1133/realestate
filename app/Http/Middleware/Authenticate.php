<?php
	
	namespace App\Http\Middleware;
	
	use Illuminate\Auth\Middleware\Authenticate as Middleware;
	use Illuminate\Support\Str;
	
	class Authenticate extends Middleware
	{
		/**
		 * Get the path the user should be redirected to when they are not authenticated.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @return string|null
		 */
		protected function redirectTo ( $request )
		{
			$route = route('App.Home');
			if ( Str::is( '/v-admin*' , $request->route()->getPrefix() ) ) {
				$route = route( 'admin-auth.login' );
			}
			if ( ! $request->expectsJson() ) {
				session()->flash('NeedAuth' , TRUE);
				return $route;
			}
		}
	}
