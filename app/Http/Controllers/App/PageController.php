<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about(){
    	$page=Option::AboutPage()->value;
    	return view('App.about')->with('page',$page);
    }
    
}
