<?php

namespace App\Http\Controllers\App\House;

use App\House;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HouseController extends Controller
{
	public function show ( $MLSNumber , $Address )
	{
		$house = House::where( 'MLSNumber' , '=' , $MLSNumber )->firstOrFail();
		return view( 'App.Houses.show' , compact( 'house' ) );
	}
	
	public function archive ( Request $rq )
	{
		$houses = House::query();
		if ( $rq->input( 'price' ) != NULL ) {
			$houses = $houses->whereBetween( 'price' , [ $rq->input( 'price' )[ 'start' ] , $rq->input( 'price' )[ 'stop' ] ] );
		}
		if ( $rq->input( 'feat' ) != NULL ) {
			$houses = $houses->whereBetween( 'lotSize' , [ $rq->input( 'feat' )[ 'start' ] , $rq->input( 'feat' )[ 'stop' ]] );
		}
		if ( $rq->input( 'yearBuilt' ) != NULL ) {
			$houses = $houses->whereBetween( 'yearBuilt' , [ $rq->input( 'yearBuilt' )[ 'start' ] , $rq->input( 'yearBuilt' )[ 'stop' ] ] );
		}
		if ( $rq->input( 'bedrooms' ) != NULL ) {
			$houses = $houses->where( 'bedrooms' , '>=' , $rq->input( 'bedrooms' ) );
		}
		if ( $rq->input( 'bathrooms' ) != NULL ) {
			$houses = $houses->where( 'bathrooms' , '>=' , $rq->input( 'bathrooms' ) );
		}
		if ( $rq->input( 'area' ) != NULL ) {
			$houses = $houses->where( 'area' , $rq->input( 'area' ) );
		}
		if ( $rq->input( 'propertyType' ) != NULL ) {
			$houses = $houses->where( 'propertyType' , $rq->input( 'propertyType' ) );
		}
		$houses = $houses->paginate( 5 );
		
		$filter = [
			'price' => [
				'min' => House::query()->min('price'),
				'max' => House::query()->max('price'),
			],
			'feat' => [
				'min' => House::query()->min('lotSize'),
				'max' => House::query()->max('lotSize'),
			] ,
			'yearBuilt' => [
				'min' => House::query()->min('yearBuilt'),
				'max' => House::query()->max('yearBuilt'),
			] ,
			'area' => House::query()->groupBy('area')->get('area')->pluck('area')->toArray(),
			'propertyType' => House::query()->groupBy('propertyType')->get('propertyType')->pluck('propertyType')->toArray()
		];
		return view( 'App.Houses.archive' , [ 'houses' => $houses , 'filter' => $filter ] );
	}
}
