<?php

namespace App\Http\Controllers\App\Blog;

use App\Article;
use App\Comment;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function show(Article $article)
    {
        $user = Auth::user();
        $categories = $article->categories;
        $articles = Article::latest()->limit(5)->get();
        $comments = $article->comments()->whereNull('parentComment')->get();
        return view('App.Blog.article', compact('user', 'article', 'categories', 'articles', 'comments'));
    }

    public function archive()
    {
        $latestArticles = Article::latest()->limit(5)->get();
        $articles = Article::where('status' , '=' , Article::$Status['Publish'])->whereDate('releaseTime' , '<=' , Carbon::now()->format('yy-m-d h:i:s'))->latest('releaseTime')->paginate(5);
        return view('App.Blog.archive', compact('latestArticles', 'articles'));
    }

    public function store(Request $request,Article $article)
    {
            $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
            ]);
            $date=$request->all();
            $date['user_id']=Auth::id();
           $article->comments()->create($date);
           session()->flash('success','the comment creat successfuly');
        return redirect(route('App.blog.show').'?id='.$article->id);


    }

}
