<?php

namespace App\Http\Controllers\App\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectPath ()
    {
    	return route('App.Home');
    }
	
	/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function authenticated(\Illuminate\Http\Request $request, $user)
	{
		if ($request->ajax()){
			return response()->json([
				'auth' => auth()->check(),
				'user' => $user,
				'intended' => $this->redirectPath(),
				'csrf-token' => csrf_token(),
				'route' => [
					'profile' => route('App.profile'),
					'logout' => route('app-auth.logout')
				]
			]);
		}
	}
	
	protected function loggedOut ( Request $request )
	{
		
		if ($request->ajax()){
			return response()->json([
				'auth' => auth()->check(),
				'intended' => $this->redirectPath(),
				'csrf-token' => csrf_token(),
			]);
		}
	}
}
