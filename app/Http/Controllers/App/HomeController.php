<?php

namespace App\Http\Controllers\App;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Option;
use Carbon\Carbon;
use League\CommonMark\Inline\Parser\OpenBracketParser;

class HomeController extends Controller
{
	public function Home ()
	{
		$Slider = Option::HomeSlider()->value ?? [ 'slides' => [] ];
		$QSearch = Option::QSearch()->value ?? [ NULL ];
		$About = Option::HomeAbout()->value??NULL;
		$BuyerSeller = Option::HomeBuyerSeller()->value??NULL;
		$resentArticle=Article::where('status' , '=' , Article::$Status['Publish'])->whereDate('releaseTime' , '<=' , Carbon::now()->format('yy-m-d h:i:s'))->latest('releaseTime')->limit(5)->get();
		$Testimonials = Option::HomeTestimonials()->value??[];
		return view( 'App.index' , compact( [ 'Slider' , 'QSearch' , 'BuyerSeller' , 'About' , 'resentArticle'  , 'Testimonials'] ) );
	}
}
