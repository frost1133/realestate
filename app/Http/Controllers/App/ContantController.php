<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class ContantController extends Controller
{
    public function contact(){
    	$data=Option::Contacts()->value;
    	return view('App.contact',compact('data'));
    }
}
