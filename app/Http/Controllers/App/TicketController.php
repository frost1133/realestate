<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Option;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
	public function showBuyingForm ()
	{
		$data=Option::PageBuyer()->value;
		return view( 'App.Ticket.Buy',compact('data') );
	}
	
	public function showSellingForm ()
	{
		$data=Option::PageSeller()->value;
		return view( 'App.Ticket.Sell',compact('data') );
	}
	
	public function Buy ( Request $request )
	{
		$data = $request->validate( [
			"name"         => "required|max:70" ,
			"email"        => "required|max:150" ,
			"phone"        => "numeric" ,
			"propertyType" => "required" ,
			"area"         => "required" ,
			"bathrooms"    => "required" ,
			"bedrooms"     => "required" ,
			"priceRange"   => "required" ,
			"sqFt"         => "required" ,
			"when"         => "required" ,
			"message"      => "required"
		] );
		$data[ 'user_id' ] = ( \auth( 'web' )->check() ? \auth( 'web' )->id() : NULL );
		$data[ 'state' ] = 0;
		$data[ 'ticketType' ] = 1;
		Ticket::create( $data );
		return back()->with( 'requestBuy' , 'Your request has been successfully submitted' );
	}
	
	public function Sell ( Request $request )
	{
		$data = $request->validate( [
			"name"         => "required|max:70" ,
			"email"        => "required|max:150" ,
			"phone"        => "numeric" ,
			"propertyType" => "required" ,
			"bedrooms"     => "required" ,
			"area"         => "required" ,
			"bathrooms"    => "required" ,
			"sqFt"         => "required" ,
			"when"         => "required" ,
			"message"      => "required" ,
		] );
		$data[ 'user_id' ] = ( \auth( 'web' )->check() ? \auth( 'web' )->id() : NULL );
		$data[ 'state' ] = 0;
		$data[ 'ticketType' ] = 0;
		$sell = Ticket::create( $data );
		return back()->with( 'requestSell' , 'Your request has been successfully submitted' );
	}
	
	public function contact ( Request $request )
	{
		Ticket::create( $request->all() );
		return redirect()->route( 'App.contact' );
	}
	
	public function store ( Request $request )
	{
		$this->validate( $request , [
			'name'    => 'required' ,
			'email'   => 'required' ,
			'phone'   => 'required' ,
			'message' => 'required' ,
		] );
		$re = Ticket::create( $request->all() );
		if ( $re ) {
			$arr = [ 'msg' => 'Add Successfully!' , 'status' => TRUE ];
		}
		
		return \Response::json( $arr );
	}
}