<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
	public function profile ()
	{
		$user = auth()->user();
		$requests = $user->tickets;
		return view( 'App.profile' , [ 'user' => $user , 'requests' => $requests ] );
	}
	
	public function update ( Request $request )
	{
		$user = $request->user();
		$valid = validator( $request->all() , [
			'name'  => 'required' ,
			'email' => [ 'required' , 'email' , Rule::unique( 'users' )->ignore( $user->id ) ] ,
			'phone' => 'required|'
		] )->errors();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$user->update($request->all());
		$response = [
			'message'    =>  'Your Profile Has Been Successfully Updated',
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
	}
	
	public function avatar ( Request $request )
	{
		$valid = validator( $request->all() , [ 'avatar' => 'required|mimes:jpeg,jpg,png' ] )->errors();
		$user = $request->user();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$file = $request->file( 'avatar' );
		$file->hashName();
		$file->extension();
		$file = $file->store( 'users/avatar' , [ 'disk' => 'public' ] );
		$user->avatar = Storage::url( $file );
		$user->save();
		$response = [
			'avatar'     => [
				'link' => $user->avatar
			] ,
			'message'    => 'Your Avatar Has Been Successfully Updated' ,
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
	}
	
	public function changePassword ( Request $request )
	{
		$user = $request->user();
		$valid = validator( $request->all() , [
			'current-password' => 'required|password' ,
			'password'         => 'required|confirmed'
		] )->errors();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$user->password = $request->password;
		$user->save();
		$response = [
			'redirect'   => route( 'App.Home' ) ,
			'message'    => 'Your Password Has Been Successfully Changed' ,
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
	}
}
