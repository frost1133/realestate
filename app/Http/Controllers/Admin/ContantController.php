<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Option;
use App\Ticket;
use Illuminate\Http\Request;

class ContantController extends Controller
{
    public function index(){
        $contants=Ticket::all();
        return view('Admin.Contant.index',compact('contants'));
    }
    public function delete(Ticket $contant){
        $contant->delete();
        return redirect()->back();
    }
    public function create(){
	    $data=Option::Contacts()->value;
	    return view('Admin.Contant.create',compact('data'));
    }
	public function store(Request $request){
		$this->validate($request,[
			'body'=>'required',
			'featureImage'=>'mimes:jpeg,bmp,png',
		]);
		$OldData=Option::Contacts()->value;
		$data=$request->only('body','metatitle','discription','metaKeywords');
		if($request->hasFile('headerImage')) {
			$headerImage = $request->file( 'headerImage' );
			$headerImage = $headerImage->store( 'Admin/about' , [ 'disk' => 'public' ] );
			$data[ 'headerImage' ] = \Storage::url( $headerImage );
		}
		else{
			$data['headerImage']=$OldData['headerImage'];
			
		}
		Option::Contacts($data);
		return back()->with('success','create successfully');
		
	}
    
}
