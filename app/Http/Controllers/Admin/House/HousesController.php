<?php

namespace App\Http\Controllers\Admin\House;

use App\House;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HousesController extends Controller
{
	public function index ()
	{
		$s = \request()->all();
		$houses = House::query();
		if ( isset( $s[ 'type' ] ) && $s[ 'type' ] != NULL ) {
			$houses = $houses->where( 'type' , $s[ 'type' ] );
		}
		if ( isset( $s[ 'Area' ] ) && $s[ 'Area' ] != NULL ) {
			$houses = $houses->where( 'Area' , $s[ 'Area' ] );
		}
		$houses = $houses->get();
		return view( 'Admin.Houses.index' , compact( 'houses' ) );
	}
	
	public function create ()
	{
		return view( 'Admin.Houses.edit' )->with( [ 'routeAction' => route( 'admin.houses.store' ) , 'method' => 'post' ] );
	}
	
	public function store ( Request $request )
	{
		$request->validate( [] );
		$data = $request->except( [ 'img' , 'video' ] );
		if ( $request->hasFile( 'img' ) ) {
			$imagesLink = [];
			foreach ( $request->file( 'img' ) as $img ) {
				$img = $img->store( 'houses/images' , [ 'disk' => 'public' ] );
				$imagesLink[] = \Storage::url( $img );
			}
			$data[ 'img' ] = $imagesLink;
		}
		if ( $request->hasFile( 'video' ) ) {
			$video = $request->file( 'video' )->store( 'houses/videos' , [ 'disk' , 'public' ] );
			$data[ 'video' ] = \Storage::url( $video );
		}
		auth('admin')->user()->houses()->create($data);
		return redirect()->route( 'admin.houses.index' )->with( 'success' , 'The house create successfully' );
	}
	
	public function delete ( House $house )
	{
		$house->delete();
		return back()->with( 'delete' , 'The house delete successfully' );
	}
	
	public function edit ( House $house )
	{
		$house = $house;
		return view( 'Admin.Houses.edit' , compact( 'house' ) )->with( [
			'routeAction' => route( 'admin.houses.update' , $house ) ,
			'method'      => 'post'
		] );;
	}
	
	public function update ( Request $request , House $house )
	{
		$request->validate( [] );
		$data = $request->except( [ 'img' , 'video' ] );
		$data['admin_id'] = auth('admin')->user()->id;
		if ( $request->hasFile( 'img' ) ) {
			$imagesLink = [];
			foreach ( $request->file( 'img' ) as $img ) {
				$img = $img->store( 'houses/images' , [ 'disk' => 'public' ] );
				$imagesLink[] = \Storage::url( $img );
			}
			$data[ 'img' ] = $imagesLink;
		}
		if ( $request->hasFile( 'video' ) ) {
			$video = $request->file( 'video' )->store( 'houses/videos' , [ 'disk' , 'public' ] );
			$data[ 'video' ] = \Storage::url( $video );
		}
		$house->update($data);
		return redirect()->route( 'admin.houses.index' )->with( 'update' , 'The house update successfully' );
		
	}
}
