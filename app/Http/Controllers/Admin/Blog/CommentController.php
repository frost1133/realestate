<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Comment;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentController extends Controller
{
	public function index ()
	{
		$comments = Comment::all();
		return view( 'Admin.Blog.Comments.index' )->with( [ "comments" => $comments ] );
	}
	
	public function update ( Request $request )
	{
		$comment = Comment::find( $request->input( 'id' ) );
		$comment->update( [ 'body' => $request->input( 'comment' ) ] );
		return response()->json( [ 'update' => 'Comment Updated' ] );
	}
	
	public function delete ( Comment $comment )
	{
		$comment->delete();
		return back();
	}
	
	public function confirm ( Comment $comment )
	{
		if ( $comment->confirmComment ) {
			$comment->confirmComment = NULL;
		} else {
			$comment->confirmComment = Carbon::now();
		}
		$comment->save();
		return back();
	}
}
