<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	public function index ()
	{
		$categories = Category::whereNull( 'parentCategory' )->get();
		return view( 'Admin.Blog.Categories.index' )->with( [ 'categories' => $categories ] );
	}
	
	public function create ()
	{
		return view( 'Admin.Blog.Categories.create' );
	}
	
	public function store ( Request $request )
	{
		$request->validate( [
			'name'            => 'required' ,
			'slug'            => 'required|unique:categories' ,
			'metaTitle'       => 'required' ,
			'metaKeywords'    => 'required' ,
			'metaDescription' => 'required' ,
		] );
		Category::create( $request->all() );
		return redirect()->route( 'admin.blog.categories.index' );
	}
	
	public function edit ( Category $category )
	{
		return view( 'Admin.Blog.Categories.edit' )->with( [ 'category' => $category ] );
	}
	
	public function update ( Category $category , Request $request )
	{
		$rolls = [ 'name' => 'required' , 'slug' => 'required' ];
		$request->input( 'slug' ) != $category->slug ? $rolls[ 'slug' ] = 'required|unique:categories' : NULL;
		$request->validate( $rolls );
		$category->update( $request->all() );
		return redirect()->route( 'admin.blog.categories.index' );
	}
	
	public function delete ( Category $category )
	{
		try {
			$category->delete();
		} catch ( \Exception $e ) {
			return back()->with('error' , $e->getMessage());
		}
		return redirect()->route( 'admin.blog.categories.index' );
	}
}
