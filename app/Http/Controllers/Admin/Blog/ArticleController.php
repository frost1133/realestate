<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Admin;
use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
	public function index ()
	{
		$now = Carbon::now()->format( 'yy-m-d h:i:s' );
		$rq = request();
		$articles = Article::query();
		if ( $rq[ 'categories' ] ) {
			$articlesCategories = \DB::table( 'article_category' )->whereIn( 'category_id' , $rq[ 'categories' ] )->get( 'article_id' )->pluck( 'article_id' )->toArray();
			$articles = $articles->whereIn( 'id' , $articlesCategories );
		}
		if ( $rq[ 'title' ] ) {
			$articles = $articles->where( 'title' , 'like' , '%' . $rq[ 'title' ] . '%' );
		}
		if ( $rq[ 'tags' ] ) {
			$articles = $articles->where( function( Builder $query ) use ( $rq ) {
				foreach ( $rq[ 'tags' ] as $tag ) {
					$query = $query->orWhere( 'tag' , 'like' , '%' . $tag . '%' );
				}
			} );
		}
		if ( $rq[ 'authors' ] ) {
			$articles = $articles->whereIn( 'admin_id' , $rq[ 'authors' ] );
		}
		if ( $rq[ 'status' ] ) {
			$status = $rq[ 'status' ];
			if ( $status == 'Draft' or $status == 'UnPublish' ) {
				$articles = $articles->where( 'status' , Article::$Status[ $status ] );
			}
			if ( $status == 'Published' ) {
				$articles = $articles->where( 'status' , Article::$Status[ 'Publish' ] )->whereDate( 'releaseTime' , '<=' , $now );
			}
			if ( $status == 'PendingPublish' ) {
				$articles = $articles->where( 'status' , Article::$Status[ 'Publish' ] )->whereDate( 'releaseTime' , '>' , $now );
			}
		}
		$count = [
			'All'            => Article::count() ,
			'Draft'          => Article::where( 'status' , Article::$Status[ 'Draft' ] )->count() ,
			'UnPublish'      => Article::where( 'status' , Article::$Status[ 'UnPublish' ] )->count() ,
			'Published'      => Article::where( 'status' , Article::$Status[ 'Publish' ] )->whereDate( 'releaseTime' , '<=' , $now )->count() ,
			'PendingPublish' => Article::where( 'status' , Article::$Status[ 'Publish' ] )->whereDate( 'releaseTime' , '>' , $now )->count()
		];
		$articles = $articles->latest( 'releaseTime' )->paginate( 10 );
		$categories = Category::whereNull( 'parentCategory' )->get();
		$tags = Article::all( 'tag' )->pluck( 'tag' )->collapse()->unique();
		$authors = Admin::all();
		return view( 'Admin.Blog.Articles.index' , compact( [ 'articles' , 'authors' , 'tags' , 'categories' , 'count' ] ) );
	}
	
	public function create ()
	{
		$article = FALSE;
		return view( 'Admin.Blog.Articles.create' )->with( 'article' , $article );
	}
	
	public function store ( Request $request )
	{
		$this->validate( $request , [
			'title'         => 'required' ,
			'body'          => 'required' ,
			'featuredImage' => 'required|mimes:jpeg,bmp,png' ,
			'category'      => 'required' ,
			'tag'           => 'required' ,
			'publishTime'   => 'required' ,
			'status'        => 'required' ,
		] );
		//Generate Data
		$data = $request->except( 'category' , 'publishTime' );
		$data[ 'releaseTime' ] = Carbon::parse( $request->input( 'publishTime' ) )->format( 'yy-m-d h:i:s' );
		$data[ 'slug' ] = Str::slug( $data[ 'title' ] );
		//Upload image
		$img = $request->file( 'featuredImage' );
		$imageArticle = $img->store( 'articles/featuredImage' , [ 'disk' => 'public' ] );
		$data[ 'featuredImage' ] = \Storage::url( $imageArticle );
		//Create Article
		$article = Auth::guard( 'admin' )->user()->Articles()->create( $data );
		$article->categories()->attach( $request->input( 'Category' ) );
		return redirect()->route( 'admin.blog.articles.index' )->with( 'success' , ' The article create successfully' );
	}
	
	public function delete ( Article $article )
	{
		$article->delete();
		return back()->with( 'delete' , 'The article delete successfully' );
	}
	
	public function edit ( Article $article )
	{
		return view( 'Admin.Blog.Articles.edit' )->with( 'article' , $article );
	}
	
	public function update ( Request $request , Article $article )
	{
		$this->validate( $request , [
			'title'         => 'required' ,
			'body'          => 'required' ,
			'featuredImage' => 'mimes:jpeg,bmp,png' ,
			'category'      => 'required' ,
			'tag'           => 'required' ,
			'publishTime'   => 'required' ,
			'status'        => 'required' ,
		] );
		//Generate Data
		$data = $request->except( [ 'publishTime' , 'category' ] );
		$data[ 'slug' ] = Str::slug( $data[ 'title' ] );
		$data[ 'releaseTime' ] = Carbon::parse( $request->input( 'publishTime' ) )->format( 'yy-m-d h:i:s' );
		//Upload File//
		if ( $request->hasFile( 'featuredImage' ) ) {
			$image = $request->file( 'featuredImage' );
			$image = $image->store( 'articles/featuredImage' , [ 'disk' => 'public' ] );
			$data[ 'featuredImage' ] = Storage::url( $image );
		}
		//Update Article
		$article->update( $data );
		$article->categories()->sync( [ $request->input( 'category' ) ] );
		return redirect()->route( 'admin.blog.articles.index' )->with( 'update' , ' The article update successfully' );
	}
	
	public function uploadImage ( Request $request )
	{
		$request->validate( [
			'image' => 'required|mimetypes:image/jpeg,image/png'
		] );
		$file = $request->file( 'image' );
		$file = $file->store( '/articles' , [ 'disk' => 'public' ] );
		$response = [
			'location'  => Storage::url( $file ) ,
			'success'   => TRUE ,
			'csrfToken' => csrf_token()
		];
		return response()->json( $response );
	}
}
