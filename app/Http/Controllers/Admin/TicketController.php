<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Ticket ;
use Illuminate\Http\Request;


class TicketController extends Controller
{
    public function index()
    {
    	$s = \request()->all();
    	$tickets = Ticket::query();
    	if (isset($s['name']) && $s['name'] != NULL){
    		$tickets = $tickets->where('name' , 'LIKE' , '%'.$s['name'].'%');
	    }
    	if (isset($s['email']) && $s['name'] != NULL){
    		$tickets = $tickets->where('email' , $s['email']);
	    }
    	if (isset($s['phone']) && $s['phone'] != NULL){
    		$tickets = $tickets->where('phone' , 'Like','%'.$s['phone']);
	    }
        $tickets = $tickets->get();
        $agents=Admin::all();
        return view('Admin.Ticket.index', compact('tickets','agents'));
    }


    public function edit(Ticket $ticket)
    {
        $agentPermission = [1, 2];
        $agents = [];
        $Admins = Admin::all();
        foreach ($Admins as $admin) {
            $permission = $admin->PermissionsArray;
            $access=[];
            foreach ($agentPermission as $p) {
                $access[]=(in_array($p, $permission)? 1 : 0) ;
            }
            if (!in_array(0,$access)){
                $agents[]=$admin;
            }
        }

            return view('Admin.Ticket.edit', compact(['ticket','agents']));


    }

    public function update(Request $request,$id)
    {

        $req =Ticket::find($id);

        $req->update($request->all());
        return redirect()->route('admin.ticket.index')->with('update','The ticket update successfully');
    }
}
