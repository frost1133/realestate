<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(User $user){
            $p=\request()->all();
            $users=User::query();
        if ( isset( $p[ 'name' ] ) && $p[ 'name' ] != NULL ) {
            $users = $users->where( 'name' , 'LIKE' , '%' . $p[ 'name' ] . '%' );
        }
        if ( isset( $p[ 'email' ] ) && $p[ 'email' ] != NULL ) {
            $users = $users->where( 'email' , $p[ 'email' ] );
        }
        $users=$users->get();
        return view('Admin.Users.index',compact('users'));
    }
    public function delete(User $user){
        $user->delete();
        return back()->with('delete','The user delete successfully');
    }
    public function show(User $user){

        return view('Admin.Users.show',compact('user'));
    }
}
