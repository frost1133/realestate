<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
	protected $permissions = [ 1 => 'admin1' , 2 => 'admin2' , 3 => 'admin3' , ];
	
	public function dashboard ()
	{
		return view( 'Admin.Dashboard' );
	}
	
	public function create ()
	{
		return view( 'Admin.Admins.create' , [ 'permissions' => $this->permissions ] );
	}
	
	public function edit ( Admin $admin )
	{
		return view( 'Admin.Admins.edit' , [ 'admin' => $admin , 'permissions' => $this->permissions ] );
	}
	
	public function index ( Admin $admin )
	{
		$s = \request()->all();
		$admins = Admin::query();
		if ( isset( $s[ 'type' ] )  && $s[ 'type' ] != NULL) {
			$admins = $admins->where( 'type' , $s[ 'type' ] );
		}
		if ( isset( $s[ 'name' ] ) && $s[ 'name' ] != NULL ) {
			$admins = $admins->where( 'name' , 'LIKE' , '%' . $s[ 'name' ] . '%' );
		}
		if ( isset( $s[ 'email' ] ) && $s[ 'email' ] != NULL ) {
			$admins = $admins->where( 'email' , $s[ 'email' ] );
		}
		if ( isset( $s[ 'permissions' ] ) && count( $s[ 'permissions' ] ) >0 ) {
			$admins = $admins->whereHas( 'Permissions' , function( Builder $builder ) {
				$builder->whereIn( 'permission' , \request()->input( 'permissions' ) );
			} );
		}
		$admins = $admins->get();
		return view( 'Admin.Admins.index' , [ 'admins' => $admins ] );
	}
	
	public function delete ( Admin $admin )
	{
		$admin->delete();
		return back();
	}
	
	public function update ( Request $request , Admin $admin )
	{
		$data = $request->validate( [ 'name' => 'required|min:5' , 'email' => "required|email" . ( $admin->email != $request->input( 'email' ) ? '|unique:admins' : '' ) , 'password' => 'required|min:5' , 'permissions' => 'required|array' ] );
		$admin->update( $data );
		$DeletedPermissions = array_diff( $admin->PermissionsArray , $request->input( 'permissions' ) );
		$admin->DeletePermission( $DeletedPermissions );
		$NewPermissions = [];
		foreach ( array_diff( $request->input( 'permissions' ) , $admin->PermissionsArray ) as $permission ) {
			$NewPermissions[] = [ 'permission' => $permission ];
		}
		$admin->Permissions()->createMany( $NewPermissions );
		return redirect()->route( 'admin.account.index' );
	}
	
	public function store ( Request $request )
	{
		$data = $request->validate( [ 'name' => 'required|min:5' , 'email' => 'required|email|unique:admins' , 'password' => 'required|min:5' , 'permissions' => 'required|array' ] );
		$admin = Admin::create( $data );
		$permissions = [];
		foreach ( $data[ 'permissions' ] as $permission ) {
			$permissions[] = new Permission( [ 'permission' => $permission ] );
		}
		$admin->Permissions()->saveMany( $permissions );
		return redirect()->route( 'admin.account.index' );
	}
	
	public function show ( Admin $admin )
	{
		
		return view( 'Admin.Admins.show' , compact( 'admin' ) );
	}
	public function profile(){
		$admin=\Auth::guard('admin')->user();
		return view('Admin.profile',compact('admin'));
	}
	public function change(Request $request){
		$this->validate($request,[
			'name'=>'required',
			'email'=>'required|email',
			'avatar'=>'mimetypes:image/jpg,image/png'
		]);
		$data=$request->only('name','email');
		if ($request->hasFile('avatar')){
			$avatar=$request->file('avatar');
			$avatar=$avatar->store('Admin/avatar',['disk'=>'public']);
			$data['avatar']=\Storage::url($avatar);
		}
	
		Auth::guard('admin')->user()->update($data);
		return back()->with('success','The profile update successfully');
	}
	public function  password(Request $request){
		$this->validate($request,[
			'password'=>'required|min:6|confirmed',
		]);
		$admin=Auth::guard('admin')->user();
		$admin->password=$request->input('password');
		$admin->save();
		return back()->with('success','The password update successfully');
	}
}
