<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentController extends Controller
{
    public function index(){
        $tickets=Ticket::where('agent_id',Auth::guard('admin')->id())->get();
        return view('Admin.Agents.ticket',compact('tickets'));
    }

    public function edit($id){
      $ticket=Ticket::find($id);
        if ($ticket->agent_id != Auth::guard('admin')->id()){
            return redirect()->route('admin.agent.index')->with('error','You cannot Edit this Ticket.');
        }
        return view('Admin.Agents.edit',compact('ticket'));
    }
    public function  update(Request $request,$id){

        $ticket=Ticket::find($id);
        if ($ticket->agent_id != Auth::guard('admin')->id()){
            return redirect()->route('admin.agent.index')->with('error','You cannot Update this Ticket.');
        }
        $ticket->update($request->all());
            return redirect()->route('admin.agent.index')->with('update','The agent update successfully');

    }
}
