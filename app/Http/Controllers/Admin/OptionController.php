<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class OptionController extends Controller
{
	protected $DirectoryUploadImage = [
		'HomeAbout'       => 'home/about' ,
		'HomeBuyerSeller' => 'home/buyerseller' ,
		'PageSeller' => 'seller' ,
		'PageBuyer' => 'buyer' ,
		'PageContact' => 'contact' ,
		'PageAbout' => 'about' ,
		'HomeTestimonial' => 'home/testimonials' ,
	];
	
	public function Home ()
	{
		$slider = Option::HomeSlider()->value ?? [ 'slides' => [] ];
		$QSearch = Option::QSearch()->value ?? [ NULL ];
		$About = Option::HomeAbout()->value ?? NULL;
		$BuyerSeller = Option::HomeBuyerSeller()->value ?? NULL;
		$Testimonials = Option::HomeTestimonials()->value??[];
		return view( 'Admin.Options.Home' , compact( [ 'slider' , 'QSearch' , 'About' , 'BuyerSeller' , 'Testimonials' ] ) );
	}
	
	public function UpdateHome ( Request $request )
	{
		$request->validate( [
			'SliderTitle'    => 'required' ,
			'SlideImages'    => 'required|array|min:1' ,
			'SlideImagesAlt' => 'required|array|min:1' ,
		] );
		$this->HomeSlide( $request->only( [ 'SliderTitle' , 'SlideImages' , 'SlideImagesAlt' ] ) );
		$this->QSearch( $request->only( [
			'QSearchRow1Image' ,
			'QSearchRow1Alt' ,
			'QSearchRow1Link' ,
			'QSearchRow2Image' ,
			'QSearchRow2Alt' ,
			'QSearchRow2Link' ,
			'QSearchTitle'
		] ) );
		$this->HomeAbout( $request->only( [ 'AboutTitle' , 'AboutBody' , 'AboutImage' ] ) );
		$this->HomeBuyerSeller( $request->only( [ 'BuyerSellerTitle' , 'BuyerSellerBody' , 'BuyerSellerImage' ] ) );
		$this->HomeTestimonials( $request->only( [ 'TestimonialTitle' , 'TestimonialName' , 'TestimonialBody' , 'TestimonialImage' ] ) );
		return redirect()->back();
		
	}
	
	protected function HomeSlide ( array $data )
	{
		$slider = [
			'Title'  => $data[ 'SliderTitle' ] ,
			'Slides' => [] ,
		];
		foreach ( $data[ 'SlideImages' ] as $index => $image ) {
			$slider[ 'Slides' ][ $index ][ 'Image' ] = $image;
			$slider[ 'Slides' ][ $index ][ 'Alt' ] = $data[ 'SlideImagesAlt' ][ $index ];
		}
		try {
			$HomeSlider = Option::HomeSlider( $slider );
			
		} catch ( Exception $exception ) {
			ddd( $exception );
		}
		$this->delete( $data[ 'SlideImages' ] , 'home/slider' );
	}
	
	public function delete ( $files , string $directory , string $drive = 'public' )
	{
		$f = [];
		foreach ( $files as $file ) {
			$re = '/(?<FileName>\w+\.\w+$)/m';
			preg_match_all( $re , $file , $matches , PREG_SET_ORDER , 0 );
			$f[] = $directory . '/' . $matches[ 0 ][ 'FileName' ];
		}
		$allFileInDirectory = Storage::drive( $drive )->files( $directory );
		$deleteFiles = array_diff( $allFileInDirectory , $f );
		if ( ! empty( $deleteFiles ) ) {
			return Storage::drive( $drive )->delete( $deleteFiles );
		}
		return 'Does not exist File To Delete';
	}
	
	protected function QSearch ( array $data )
	{
		$QSearch = [];
		foreach ( $data[ 'QSearchRow1Image' ] as $index => $image ) {
			$QSearch[ 'QSearchRow1' ][ $index ][ 'Image' ] = $data[ 'QSearchRow1Image' ][ $index ];
			$QSearch[ 'QSearchRow1' ][ $index ][ 'Alt' ] = $data[ 'QSearchRow1Alt' ][ $index ];
			$QSearch[ 'QSearchRow1' ][ $index ][ 'Link' ] = $data[ 'QSearchRow1Link' ][ $index ];
		}
		foreach ( $data[ 'QSearchRow2Image' ] as $index => $image ) {
			$QSearch[ 'QSearchRow2' ][ $index ][ 'Image' ] = $data[ 'QSearchRow2Image' ][ $index ];
			$QSearch[ 'QSearchRow2' ][ $index ][ 'Alt' ] = $data[ 'QSearchRow2Alt' ][ $index ];
			$QSearch[ 'QSearchRow2' ][ $index ][ 'Link' ] = $data[ 'QSearchRow2Link' ][ $index ];
		}
		$QSearch[ 'Title' ] = $data[ 'QSearchTitle' ];
		Option::QSearch( $QSearch );
		$images = array_merge( $data[ 'QSearchRow1Image' ] , $data[ 'QSearchRow2Image' ] );
		$this->delete( $images , 'home/qsearch' );
		
	}
	
	protected function HomeAbout ( array $data )
	{
		$about = [
			'Title' => $data[ 'AboutTitle' ] ,
			'Body'  => $data[ 'AboutBody' ] ,
			'Image' => $data[ 'AboutImage' ]
		];
		Option::HomeAbout( $about );
	}
	
	protected function HomeBuyerSeller ( $data )
	{
		$BuyerSeller = [
			'Title' => $data[ 'BuyerSellerTitle' ] ,
			'Body'  => $data[ 'BuyerSellerBody' ] ,
			'Image' => $data[ 'BuyerSellerImage' ] ,
		];
		Option::HomeBuyerSeller( $BuyerSeller );
	}
	
	public function General ()
	{
		$Contacts = Option::Contacts()->value??[];
		$SocialMedia = Option::SocialMedia()->value ?? [];
		if ( ! empty( old() ) ) {
			$data = [
				'SocialMediaIcon' => old( 'SocialMediaIcon' ) ,
				'SocialMediaName' => old( 'SocialMediaName' ) ,
				'SocialMediaLink' => old( 'SocialMediaLink' ),
				'ContactsIcon' => old( 'ContactsIcon' ),
				'ContactsValue' => old( 'ContactsValue' ),
				'ContactsName' => old( 'ContactsName' ),
			];
			$Social = [];
			foreach ( $data[ 'SocialMediaIcon' ] as $index => $icon ) {
				$Social[ $index ] = [
					'Icon' => $data[ 'SocialMediaIcon' ][ $index ] ,
					'Name' => $data[ 'SocialMediaName' ][ $index ] ,
					'Link' => $data[ 'SocialMediaLink' ][ $index ] ,
					
				];
			}
			$Contact=[];
			foreach ( $data[ 'Contacts' ] as $index => $icon ) {
				$Contact[ $index ] = [
					
					'Icon' => $data[ 'ContactsIcon' ][ $index ] ,
					'Value' => $data[ 'ContactsValue' ][ $index ] ,
					'Name' => $data[ 'ContactsName' ][ $index ] ,
				];
			}
			$oldData = [
				'SocialMedia' => $Social,
				'Contacts'=>$Contacts,
			];
		}
		return view( 'Admin.Options.General' , compact( [ 'Contacts' , 'SocialMedia' ] ) )->with('oldData',$oldData??[]);
	}
	
	public function UpdateGeneral ( Request $request )
	{
		$request->validate( [
			'ContactsIcon'             => 'required|array' ,
			'ContactsValue'             => 'required|array' ,
			'ContactsName'           => 'required|array' ,
			'SocialMediaIcon'   => 'required|array' ,
			'SocialMediaName'   => 'required|array' ,
			'SocialMediaLink'   => 'required|array' ,
			'SocialMediaLink.*' => 'required|url' ,
		] );
		$this->Contacts( $request->only( [ 'ContactsIcon' , 'ContactsValue' , 'ContactsName' ] ) );
		$this->SocialMedia( $request->only( [ 'SocialMediaIcon' , 'SocialMediaName' , 'SocialMediaLink' ] ) );
		return back();
	}
	
	protected function SocialMedia ( array $data )
	{
		$Social = [];
		foreach ( $data[ 'SocialMediaIcon' ] as $index => $icon ) {
			$Social[ $index ] = [
				'Icon' => $data[ 'SocialMediaIcon' ][ $index ] ,
				'Name' => $data[ 'SocialMediaName' ][ $index ] ,
				'Link' => $data[ 'SocialMediaLink' ][ $index ] ,
			];
		}
		
		Option::SocialMedia( $Social );
	}
	
	protected function Contacts(array $data){
		$Contact = [];
		foreach ( $data[ 'ContactsIcon' ] as $index => $icon ) {
			$Contact[ $index ] = [
				'Icon'  => $data[ 'ContactsIcon' ][ $index ] ,
				'Name'  => $data[ 'ContactsName' ][ $index ] ,
				'Value' => $data[ 'ContactsValue' ][ $index ] ,
			];
		}
		Option::Contacts( $Contact );
		
	}
	
	public function imageSlider ( Request $request )
	{
		$valid = Validator( $request->all() , [
			'slide' => 'Required|mimetypes:image/jpeg,image/png'
		] )->errors();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$file = $this->upload( $request->file( 'slide' ) , 'home/slider' );
		$response = [
			'slide'      => Storage::url( $file ) ,
			'message'    => 'Uploaded' ,
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
	}
	
	public function upload ( UploadedFile $file , $directory )
	{
		$file->hashName();
		$file = $file->store( $directory , [ 'disk' => 'public' ] );
		return $file;
	}
	
	public function UploadImage ( Request $request )
	{
		$valid = validator( $request->all() , [
			'image'     => 'required|file|mimetypes:image/jpeg,image/png' ,
			'directory' => 'required'
		] )->errors();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$file = $this->upload( $request->file( 'image' ) , $this->DirectoryUploadImage[ $request->input( 'directory' ) ] );
		$response = [
			'image'      => Storage::url( $file ) ,
			'message'    => 'Uploaded' ,
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
		
	}
	
	public function ImageQSearch ( Request $request )
	{
		$valid = Validator( $request->all() , [
			'QSearch' => 'Required|mimetypes:image/jpeg,image/png'
		] )->errors();
		if ( $valid->any() ) {
			$response = [
				'message'    => $valid->first() ,
				'success'    => FALSE ,
				'csrf-token' => csrf_token()
			];
			return response()->json( $response );
		}
		$file = $this->upload( $request->file( 'QSearch' ) , 'home/qsearch' );
		$response = [
			'QSearch'    => Storage::url( $file ) ,
			'message'    => 'Uploaded' ,
			'success'    => TRUE ,
			'csrf-token' => csrf_token()
		];
		return response()->json( $response );
	}
	
	protected function HomeTestimonials ( array $data )
	{
		$Testimonials = [];
		foreach ( $data[ 'TestimonialName' ] as $index => $Testimonial ) {
			$Testimonials[ $index ][ 'Name' ] = $data[ 'TestimonialName' ][ $index ];
			$Testimonials[ $index ][ 'Title' ] = $data[ 'TestimonialTitle' ][ $index ];
			$Testimonials[ $index ][ 'Body' ] = $data[ 'TestimonialBody' ][ $index ];
			$Testimonials[ $index ][ 'Image' ] = $data[ 'TestimonialImage' ][ $index ];
		}
		Option::HomeTestimonials($Testimonials);
	}
	
	public function about(){
		$data=Option::AboutPage()->value;
		
		return view('Admin.Options.About',compact('data'));
	}
	public function saveAbout(Request $request){
		$this->validate($request,[
			'title'=>'required',
			'body'=>'required',
			'featureImage'=>'mimes:jpeg,bmp,png',
			'headerImage'=>'mimes:jpeg,bmp,png',
			
		]);
		$OldData=Option::AboutPage()->value;
		$data=$request->only('title','body','metatitle','discription','metaKeywords');
		if($request->hasFile('featureImage')){
			$featureImage = $request->file( 'featureImage' );
			$featureImage = $featureImage->store( 'Admin/about' , [ 'disk' => 'public' ] );
			$data[ 'featureImage' ] = \Storage::url( $featureImage );
		}
		else{
			$data['featureImage']=$OldData['featureImage'];
		}
		if($request->hasFile('headerImage')) {
			$headerImage = $request->file( 'headerImage' );
			$headerImage = $headerImage->store( 'Admin/about' , [ 'disk' => 'public' ] );
			$data[ 'headerImage' ] = \Storage::url( $headerImage );
		}
		else{
			$data['headerImage']=$OldData['headerImage'];

		}
		Option::AboutPage($data);
		return back()->with('success','create successfully');
		
	}
	
	public function buyer(){
		$data=Option::PageBuyer()->value;
		
		return view('Admin.Options.Buyer',compact('data'));
	}
	public function  saveBuyer(Request $request){
		$this->validate($request,[
			'body'=>'required',
			'headerImage'=>'mimes:jpeg,bmp,png',
		
		]);
		$OldData=Option::PageBuyer()->value;
		$data=$request->only('body','metatitle','metaDescription','metaKeywords');
	
		if($request->hasFile('headerImage')) {
			$headerImage = $request->file( 'headerImage' );
			$headerImage = $headerImage->store( 'buyer' , [ 'disk' => 'public' ] );
			$data[ 'headerImage' ] = \Storage::url( $headerImage );
		}
		else{
			$data['headerImage']=$OldData['headerImage'];
			
		}
		Option::PageBuyer($data);
		return back()->with('success','create successfully');
	}
	public function seller(){
		$data=Option::PageSeller()->value;
		
		return view('Admin.Options.Seller',compact('data'));
	}
	public function saveSeller(Request $request){
		$this->validate($request,[
			'body'=>'required',
			'headerImage'=>'mimes:jpeg,bmp,png',
		
		]);
		$OldData=Option::PageSeller()->value;
		$data=$request->only('body','metatitle','metaDescription','metaKeywords');
		
		if($request->hasFile('headerImage')) {
			$headerImage = $request->file( 'headerImage' );
			$headerImage = $headerImage->store( 'seller' , [ 'disk' => 'public' ] );
			$data[ 'headerImage' ] = \Storage::url( $headerImage );
		}
		else{
			$data['headerImage']=$OldData['headerImage'];
			
		}
		Option::PageSeller($data);
		return back()->with('success','create successfully');
	}
}
