<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ListingSearchController extends Controller
{
	
	protected function search ( array $data )
	{
		$q = DB::table( 'houses' );
		foreach ( $data as $key => $value ) {
			switch ( $key ) {
				case 'bathrooms':
					$q = $q->where( 'bathrooms' , '>=' , $value );
					break;
				case 'bedrooms':
					$q = $q->where( 'bedrooms' , ">=" , $value );
					break;
				case 'propertyType':
					$q = $q->where( 'propertyType' , $value );
					break;
				case 'price' :
					$q = $q->whereBetween( 'price' , $value );
					break;
				case 'squareFeet' :
					$q = $q->whereBetween( 'squareFeet' , $value );
					break;
				case 'yearBuilt':
					$q = $q->whereBetween( 'yearBuilt' , $value );
					break;
				case 'city' :
					$q = $q->where( 'city' , $value );
					break;
			}
		}
		return $q->get();
	}
}
